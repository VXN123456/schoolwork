package com.example.vien_pt.schoolwork.asyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.MeetingModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Vien-PT on 04-Dec-17.
 */

public class MeetingsAsyncTask extends AsyncTask<String, Void, List<MeetingModel>> {
    Context context;
    ProgressDialog progressDialog;

    public MeetingsAsyncTask(Context context) {
        this.context = context;
        this.progressDialog = new ProgressDialog(context, R.style.DialogThemMain);
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setMessage("Processing...");
        progressDialog.show();
    }

    @Override
    protected List<MeetingModel> doInBackground(String... params) {
        String jsonFinal = null;
        try {
            jsonFinal = Utils.makeRequest(Constants.GET_METHOD, params[0], null, Constants.CONTENT_TYPE_JSON, "");
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONArray jsonArrayParent = null;
        try {
            jsonArrayParent = new JSONArray(jsonFinal);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        List<MeetingModel> meetingModels = new ArrayList<>();
        if (jsonArrayParent != null) for (int i = 0; i < jsonArrayParent.length(); i++) {
            JSONObject jsonObject = null;
            try {
                jsonObject = jsonArrayParent.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                MeetingModel meetingModel = new MeetingModel();
                if (jsonObject != null) {
                    meetingModel.setId(jsonObject.getInt("id"));
                    meetingModel.setName(jsonObject.getString("name"));
                    meetingModel.setTime(jsonObject.getString("time"));
                    meetingModel.setDate(jsonObject.getString("date"));
                    meetingModel.setLocation(jsonObject.getString("location"));
                    meetingModel.setContent(jsonObject.getString("content"));
                    meetingModel.setProjectId(jsonObject.getInt("projectId"));
                    meetingModel.setCreatedBy(jsonObject.getInt("createdBy"));
                    meetingModel.setCreatedAt(jsonObject.getString("createdAt"));
                    meetingModel.setUpdatedAt(jsonObject.getString("updatedAt"));
                    meetingModel.setParticipants(jsonObject.getInt("participants"));
                    meetingModel.setMembers(jsonObject.getInt("members"));

                    meetingModels.add(meetingModel);
                }
            } catch (JSONException ignored) {
            }
        }
        return meetingModels;
    }

    @Override
    protected void onPostExecute(List<MeetingModel> meetingModels) {
        super.onPostExecute(meetingModels);
        LoginActivity.meetings = meetingModels;
        progressDialog.dismiss();
    }
}
