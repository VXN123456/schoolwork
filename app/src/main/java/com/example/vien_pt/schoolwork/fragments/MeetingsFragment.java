package com.example.vien_pt.schoolwork.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.CreateMeetingActivity;
import com.example.vien_pt.schoolwork.adapters.MeetingAdapter;
import com.example.vien_pt.schoolwork.asyncTasks.ProfileMeetingsAsyncTask;
import com.example.vien_pt.schoolwork.common.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vien-PT on 08-Oct-17.
 */

public class MeetingsFragment extends Fragment {

    ImageButton imbAddMeeting;
    ViewPager viewPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_meetings, container, false);

        // Setting ViewPager for each Tabs
        viewPager = (ViewPager) view.findViewById(R.id.vpMeeting);
        setupViewPager(viewPager);
        // Set Tabs inside Toolbar
        TabLayout tabsMeeting = (TabLayout) view.findViewById(R.id.tabsMeeting);
        tabsMeeting.setTabGravity(TabLayout.GRAVITY_FILL);

        tabsMeeting.setupWithViewPager(viewPager);

        return view;

    }

    // Add Fragments to Tabs
    public void setupViewPager(final ViewPager viewPager) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                MeetingTabAdapter meetingTabAdapter = new MeetingTabAdapter(getChildFragmentManager());
                meetingTabAdapter.addFragment(new MyMeetingsFragment(), "My Meetings");
                meetingTabAdapter.addFragment(new MeetingsHistoryFragment(), "Meetings History");
                meetingTabAdapter.addFragment(new NextMeetingsFragment(), "Next Meetings");
                viewPager.setAdapter(meetingTabAdapter);
            }
        }, 500);

    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("CutPasteId")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imbAddMeeting = (ImageButton)getView().findViewById(R.id.imbAddMeeting);
        imbAddMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCreateMeeting = new Intent(getActivity(), CreateMeetingActivity.class);
                startActivity(intentCreateMeeting);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        //get records bảng mối quan hệ giữa profile và meeting
        new ProfileMeetingsAsyncTask(getActivity()).execute(Constants.URL_PROFILEMEETINGS_BY_PROFILE_API);
    }

    static class MeetingTabAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public MeetingTabAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
