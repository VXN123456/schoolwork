package com.example.vien_pt.schoolwork.activities;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.adapters.SpinnerAssigneeAdapter;
import com.example.vien_pt.schoolwork.adapters.SpinnerProjectAdapter;
import com.example.vien_pt.schoolwork.asyncTasks.CreateOrUpdateMeetingAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.CreateOrUpdateTaskAsyncTask;
import com.example.vien_pt.schoolwork.base.BaseActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.ProfileModel;
import com.example.vien_pt.schoolwork.models.ProjectModel;
import com.example.vien_pt.schoolwork.models.TaskModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class EditTaskActivity extends BaseActivity {
    List<ProfileModel> members;
    TextView tvDate;
    DatePickerDialog datePickerDialog;
    Spinner spnProject;
    Spinner spnAssignee;
    SpinnerAssigneeAdapter assigneeAdapter;
    SpinnerProjectAdapter spinnerProjectAdapter;
    ProjectModel projectModel;
    List<ProjectModel> projectModels;
    AutoCompleteTextView edtTaskName;
    EditText edtTaskDes;
    Button btnSaveTask;
    ProfileModel assigneeModelSelected;
    ProjectModel projectModelSelected;
    TaskModel taskModelEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_task);
        // init view
        initView();
        //get task model need to edit
        getTaskModelEdit();

        //check create task in or out
        if (projectModel != null) {
            projectModels = new ArrayList<>();
            projectModels.add(projectModel);
        } else {
            projectModels = LoginActivity.profileProjects;
        }
        spinnerProjectAdapter = new SpinnerProjectAdapter(EditTaskActivity.this, R.layout.item_project, projectModels);
        spnProject.setAdapter(spinnerProjectAdapter);

        if (members != null && members.size() > 0) {
            assigneeAdapter = new SpinnerAssigneeAdapter(EditTaskActivity.this, R.layout.item_contact, members);
            spnAssignee.setAdapter(assigneeAdapter);
        }

        spnProject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                projectModelSelected = (ProjectModel) parent.getItemAtPosition(position);
                Integer[] membersId = projectModelSelected.getMembers();
                members = new ArrayList<>();
                for (int idProfile = 0; idProfile < membersId.length; idProfile++) {
                    for (ProfileModel profileModel : LoginActivity.profiles) {
                        if (profileModel.getId() == membersId[idProfile]) {
                            members.add(profileModel);
                            break;
                        }
                    }
                }
                assigneeAdapter = new SpinnerAssigneeAdapter(EditTaskActivity.this, R.layout.item_contact, members);
                spnAssignee.setAdapter(assigneeAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // perform click event on edit text
        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(EditTaskActivity.this,android.R.style.Theme_DeviceDefault_Light_Dialog,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                tvDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        //choose assignee
        spnAssignee.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                assigneeModelSelected = (ProfileModel) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        // set current task model
        setDataEdit();

        //click button save
        btnSaveTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isConnectingToInternet(getApplicationContext())) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(EditTaskActivity.this, R.style.DialogThemMain);
                    builder.setMessage("No internet connection, please try again!")
                            .setPositiveButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            }).show();
                } else if (edtTaskName.getText().toString().equals(Constants.EMPTY)) {
                    Toast.makeText(getApplicationContext(), "Please enter task name!", Toast.LENGTH_SHORT).show();
                } else {
                    JSONObject taskObject = new JSONObject();

                    try {
                        taskObject.put("id", taskModelEdit.getId());
                        taskObject.put("name", edtTaskName.getText().toString());
                        taskObject.put("status", taskModelEdit.getStatus());
                        taskObject.put("description", edtTaskDes.getText().toString());
                        taskObject.put("createdBy", taskModelEdit.getCreatedBy());
                        taskObject.put("meetingId", 0);
                        taskObject.put("assignee", assigneeModelSelected.getId());
                        taskObject.put("deadline", tvDate.getText().toString());
                        taskObject.put("createdAt", taskModelEdit.getCreatedAt());
                        taskObject.put("updatedAt", Utils.getCurrentTimeStamp());
                        taskObject.put("projectId", projectModelSelected.getId());
                        taskObject.put("note", "");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    CreateOrUpdateTaskAsyncTask createOrUpdateTaskAsyncTask = new CreateOrUpdateTaskAsyncTask(EditTaskActivity.this, taskObject.toString());
                    createOrUpdateTaskAsyncTask.execute(Constants.URL_ADD_OR_UPDATE_TASK_API);
                }
            }
        });

    }

    //set data current task
    private void setDataEdit() {
        edtTaskName.setText(taskModelEdit.getName());
        edtTaskDes.setText(taskModelEdit.getDescription());
        tvDate.setText(taskModelEdit.getDeadline());
        //get project model need to edit
        ProjectModel projectContainTask = new ProjectModel();
        if (projectModels != null && projectModels.size() > 0) {
            for (ProjectModel projectModel : projectModels) {
                if (projectModel.getId() == taskModelEdit.getProjectId()) {
                    projectContainTask = projectModel;
                }
            }
        }
        spnProject.setSelection(spinnerProjectAdapter.getPosition(projectContainTask));

        ProfileModel currentAssignee = new ProfileModel();
        if (members != null && members.size() > 0) {
            for (ProfileModel profileModel : members) {
                if (profileModel.getId() == taskModelEdit.getAssignee()) {
                    currentAssignee = profileModel;
                }
            }
            spnAssignee.setSelection(assigneeAdapter.getPosition(currentAssignee));
        }
    }

    //get task model edit
    private void getTaskModelEdit() {
        Intent intent = getIntent();
        taskModelEdit = new TaskModel();
        taskModelEdit = (TaskModel) intent.getSerializableExtra("task");
    }

    public void initView() {
        this.setToolBar("Edit task");
        //set color status bar
        setColorStatusBar(this);

        edtTaskName = (AutoCompleteTextView) findViewById(R.id.edtTaskName);
        edtTaskDes = (EditText) findViewById(R.id.edtTaskDes);
        //dropdown projects
        spnProject = (Spinner) findViewById(R.id.spnProjectName);
        spnAssignee = (Spinner) findViewById(R.id.spnAssignee);
        // initiate the date picker and a button
        tvDate = (TextView) findViewById(R.id.tvDate);
        //save to create a new task
        btnSaveTask = (Button) findViewById(R.id.btnSaveTask);
    }

}
