package com.example.vien_pt.schoolwork.activities;

import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.example.vien_pt.schoolwork.asyncTasks.LoginAsyncTask;
import com.example.vien_pt.schoolwork.base.BaseActivity;
import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.helpers.DBHelper;

public class SplashActivity extends BaseActivity {

    /**
     * Duration of wait
     **/
    private final int SPLASH_DISPLAY_LENGTH = 3000;

    /**
     * Called when the activity is first created.
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_splash);

        setColorStatusBar(this);

        ImageView rotate_image = (ImageView) findViewById(R.id.imvSplash);
        RotateAnimation rotate = new RotateAnimation(30, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(3000);
        rotate_image.startAnimation(rotate);

        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                DBHelper db = new DBHelper(getApplication());
                Cursor cursor = db.getData();
                String email;
                String password;
                String role;
                if (cursor.getCount()!=0){
                    cursor.moveToFirst();
                    email = cursor.getString(0);
                    password = cursor.getString(1);
                    role = cursor.getString(2);
                    new LoginAsyncTask(SplashActivity.this).execute(Constants.URL_ALL_PROFILE_API, email, password, "true");
                }else{
                    /* Create an Intent that will start the Login-Activity. */
                    Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                    SplashActivity.this.startActivity(mainIntent);
                    SplashActivity.this.finish();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
