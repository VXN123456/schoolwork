package com.example.vien_pt.schoolwork.asyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.MeetingModel;
import com.example.vien_pt.schoolwork.models.ProfileMeetingModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Vien-PT on 04-Dec-17.
 */

public class ProfileMeetingsAsyncTask extends AsyncTask<String, Void, List<ProfileMeetingModel>> {
    Context context;
    ProgressDialog progressDialog;

    public ProfileMeetingsAsyncTask(Context context) {
        this.context = context;
        this.progressDialog = new ProgressDialog(context, R.style.DialogThemMain);
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setMessage("Processing...");
        progressDialog.show();
    }

    @Override
    protected List<ProfileMeetingModel> doInBackground(String... params) {
        String jsonFinal = null;
        try {
            jsonFinal = Utils.makeRequest(Constants.GET_METHOD, params[0], null, Constants.CONTENT_TYPE_JSON, "");
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONArray jsonArrayParent = null;
        try {
            jsonArrayParent = new JSONArray(jsonFinal);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        List<ProfileMeetingModel> profileMeetingModels = new ArrayList<>();
        if (jsonArrayParent != null) for (int i = 0; i < jsonArrayParent.length(); i++) {
            JSONObject jsonObject = null;
            try {
                jsonObject = jsonArrayParent.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                ProfileMeetingModel profileMeetingModel = new ProfileMeetingModel();
                if (jsonObject != null) {
                    profileMeetingModel.setId(jsonObject.getInt("id"));
                    profileMeetingModel.setProfileId(jsonObject.getInt("profileId"));
                    profileMeetingModel.setMeetingId(jsonObject.getInt("meetingId"));
                    profileMeetingModel.setFlagRead(jsonObject.getInt("flagRead"));
                    profileMeetingModel.setFlagJoin(jsonObject.getInt("flagJoin"));

                    profileMeetingModels.add(profileMeetingModel);
                }
            } catch (JSONException ignored) {
            }
        }
        return profileMeetingModels;
    }

    @Override
    protected void onPostExecute(List<ProfileMeetingModel> profileMeetingModels) {
        super.onPostExecute(profileMeetingModels);
        LoginActivity.profileProfileMeetings = profileMeetingModels;
        progressDialog.dismiss();
    }
}
