package com.example.vien_pt.schoolwork.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.vien_pt.schoolwork.models.CriterionModel;
import com.example.vien_pt.schoolwork.models.MeetingModel;
import com.example.vien_pt.schoolwork.models.ProfileMeetingModel;
import com.example.vien_pt.schoolwork.models.ProfileModel;
import com.example.vien_pt.schoolwork.models.ProjectModel;
import com.example.vien_pt.schoolwork.asyncTasks.LoginAsyncTask;
import com.example.vien_pt.schoolwork.base.BaseActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.models.TaskModel;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends BaseActivity {

    public static List<ProjectModel> profileProjects;
    public static int accountId = 0;
    public static List<TaskModel> profileTasks;
    public static List<TaskModel> tasks;
    public static List<ProfileModel> profiles;
    public static List<ProjectModel> projects;
    public static List<MeetingModel> profileMeetings;
    public static List<MeetingModel> meetings;
    public static List<ProfileMeetingModel> listProfileMeetings;
    public static List<ProfileMeetingModel> profileProfileMeetings;
    public static List<CriterionModel> criteria;

    public static boolean isLoadedProjects = false;
    public static boolean isLoadedProfileProjects = false;
    public static boolean isLoadedContacts = false;
    public static boolean isLoadedTasks = false;
    public static boolean isLoadedMeetings = false;
    public static boolean isLoadedCriteria = false;

    private String email;
    private String password;
    private boolean isCheckedRemember;
    private String role;

    AutoCompleteTextView edtEmail;
    AutoCompleteTextView edtPassword;
    CheckBox chbRememberMe;
    private TextView goSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //set color status bar
        setColorStatusBar(this);



        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        edtEmail = (AutoCompleteTextView) findViewById(R.id.edtEmail);
        edtPassword = (AutoCompleteTextView) findViewById(R.id.edtPassword);
        chbRememberMe = (CheckBox) findViewById(R.id.chbRememberMe);
        goSignUp = (TextView) findViewById(R.id.tvGoSignUp);

        goSignUp.setText(R.string.sign_up_now);


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.context = LoginActivity.this;
                if (!Utils.isConnectingToInternet(getApplicationContext())) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this, R.style.DialogThemMain);
                    builder.setMessage("No internet connection, please try again!")
                            .setPositiveButton(getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    onResume();
                                }
                            }).show();
                } else {
                    setEmail(edtEmail.getText().toString());
                    setPassword(edtPassword.getText().toString());
                    setCheckedRemember(chbRememberMe.isChecked());
                    new LoginAsyncTask(LoginActivity.this).execute(Constants.URL_ALL_PROFILE_API, getEmail(), getPassword(), String.valueOf(isCheckedRemember()));
                }
            }
        });

    }

    public void signUp(View view) {
        Intent intentSignUp = new Intent(LoginActivity.this, SignUpActivity.class);
        startActivity(intentSignUp);
        this.finish();
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this, R.style.DialogThemMain)
                .setMessage("Do you want to close app?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        moveTaskToBack(true);
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    //getter and setter

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isCheckedRemember() {
        return isCheckedRemember;
    }

    public void setCheckedRemember(boolean checkedRemember) {
        isCheckedRemember = checkedRemember;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    protected void onResume() {
        super.onResume();
        //init project list
        profileProjects = new ArrayList<>();
        profileTasks = new ArrayList<>();
        tasks = new ArrayList<>();
        profiles = new ArrayList<>();
        projects = new ArrayList<>();
        profileMeetings = new ArrayList<>();
        meetings = new ArrayList<>();
        profileProfileMeetings = new ArrayList<>();
        criteria = new ArrayList<>();
    }
}
