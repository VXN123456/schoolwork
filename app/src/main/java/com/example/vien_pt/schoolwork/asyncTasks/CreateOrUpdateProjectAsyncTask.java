package com.example.vien_pt.schoolwork.asyncTasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.CreateProjectActivity;
import com.example.vien_pt.schoolwork.activities.EditProjectActivity;
import com.example.vien_pt.schoolwork.activities.ProjectDetailActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.ProjectModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by Vien-PT on 03-Nov-17.
 */

public class CreateOrUpdateProjectAsyncTask extends AsyncTask<String, Void, String> {
    Context context;
    String jsonRequestEntity;
    String result;
    ProjectModel projectModel;
    ProgressDialog progressDialog;

    public CreateOrUpdateProjectAsyncTask(Context context, String jsonString, ProjectModel projectModel) {
        this.context = context;
        this.jsonRequestEntity = jsonString;
        this.projectModel = projectModel;
        this.progressDialog = new ProgressDialog(context, R.style.DialogThemMain);

    }

    @Override
    protected void onPreExecute() {
        progressDialog.setMessage("Processing...");
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            result = Utils.makeRequest(Constants.POST_METHOD, params[0], null, Constants.CONTENT_TYPE_JSON, this.jsonRequestEntity);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result.equals("Created")) {
            Toast.makeText(context, "Create successful!", Toast.LENGTH_SHORT).show();
            ((Activity) context).finish();
        } else if (result.equals("Updated")) {
            Toast.makeText(context, "Update successful!", Toast.LENGTH_SHORT).show();
            ((Activity)context).setResult(Activity.RESULT_OK,new Intent().putExtra("project",projectModel));
            ((Activity) context).finish();
        } else if (result.equals("Duplicated project")) {
            Toast.makeText(context, "Project name has existed", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Cannot connect to server!", Toast.LENGTH_SHORT).show();
        }
    }
}
