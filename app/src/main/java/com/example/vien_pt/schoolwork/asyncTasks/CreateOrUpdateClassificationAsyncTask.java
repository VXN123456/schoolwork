package com.example.vien_pt.schoolwork.asyncTasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.ClassificationModel;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Vien-PT on 02-Dec-17.
 */

public class CreateOrUpdateClassificationAsyncTask extends AsyncTask<String, Void, String> {
    String result = null;
    Context context;
    ProgressDialog progressDialog;
    private String jsonRequestEntity;
    List<ClassificationModel> classifications;

    public CreateOrUpdateClassificationAsyncTask(Context context, String jsonString, List<ClassificationModel> classifications) {
        this.context = context;
        this.progressDialog = new ProgressDialog(context, R.style.DialogThemMain);
        this.jsonRequestEntity = jsonString;
        this.classifications = classifications;
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setMessage("Processing...");
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            result = Utils.makeRequest(Constants.POST_METHOD, params[0], null, Constants.CONTENT_TYPE_JSON, this.jsonRequestEntity);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result.equals("Updated")){
            Toast.makeText(context, "Updated successfully", Toast.LENGTH_SHORT).show();
            ((Activity)context).setResult(Activity.RESULT_OK,new Intent().putExtra("classifications", (Serializable) classifications));
            ((Activity)context).finish();
            progressDialog.dismiss();
        }else if(result.equals("Removed all")){
            Toast.makeText(context, "You have deleted all the classifications", Toast.LENGTH_SHORT).show();
            ((Activity)context).setResult(Activity.RESULT_OK,new Intent().putExtra("classifications", (Serializable) classifications));
            ((Activity)context).finish();
            progressDialog.dismiss();
        }else{
            Toast.makeText(context, "Processing failed", Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();
        }
    }
}
