package com.example.vien_pt.schoolwork.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.adapters.SpinnerProjectAdapter;
import com.example.vien_pt.schoolwork.asyncTasks.CreateOrUpdateMeetingAsyncTask;
import com.example.vien_pt.schoolwork.base.BaseActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.MeetingModel;
import com.example.vien_pt.schoolwork.models.ProjectModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

public class EditMeetingActivity extends BaseActivity {
    AutoCompleteTextView inputMeetingName;
    EditText edtMeetingContent;
    Spinner spnProject;
    TextView tvMeetingDate;
    TextView tvMeetingTime;
    EditText edtLocation;
    Button btnSaveMeeting;
    DatePickerDialog datePickerDialog;
    TimePickerDialog timePickerDialog;
    SpinnerProjectAdapter spinnerProjectAdapter;
    List<ProjectModel> projectModels;
    MeetingModel meetingModel;
    ProjectModel projectModelSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_meeting);

        //init view
        this.initView();
        //get meeting model from meeting list
        this.getMeetingModelFromMeetingList();
        projectModels = LoginActivity.profileProjects;
        spinnerProjectAdapter = new SpinnerProjectAdapter(EditMeetingActivity.this, R.layout.item_project, projectModels);
        spnProject.setAdapter(spinnerProjectAdapter);
        this.setDataEdit();
        spnProject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                projectModelSelected = new ProjectModel();
                projectModelSelected = (ProjectModel) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // perform click event on edit text
        tvMeetingDate.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR); // current year
                int month = c.get(Calendar.MONTH); // current month
                int day = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(EditMeetingActivity.this,android.R.style.Theme_DeviceDefault_Light_Dialog,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                tvMeetingDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, year, month, day);
                datePickerDialog.show();
            }
        });
        tvMeetingTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get Current Time
                final Calendar c = Calendar.getInstance();
                int hour = c.get(Calendar.HOUR_OF_DAY);
                int minute = c.get(Calendar.MINUTE);

                // Launch Time Picker Dialog
                timePickerDialog = new TimePickerDialog(EditMeetingActivity.this,android.R.style.Theme_DeviceDefault_Light_Dialog,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                tvMeetingTime.setText(String.format("%02d:%02d", hourOfDay, minute));
                            }
                        }, hour, minute, false);
                timePickerDialog.show();
            }
        });

        //click button save meeting
        btnSaveMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isConnectingToInternet(getApplicationContext())) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(EditMeetingActivity.this, R.style.DialogThemMain);
                    builder.setMessage("No internet connection, please try again!")
                            .setPositiveButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            }).show();
                } else if (inputMeetingName.getText().toString().equals(Constants.EMPTY)) {
                    Toast.makeText(getApplicationContext(), "Please enter meeting name!", Toast.LENGTH_SHORT).show();
                } else {
                    JSONObject meetingObject = new JSONObject();

                    try {
                        meetingObject.put("id", meetingModel.getId());
                        meetingObject.put("name", inputMeetingName.getText().toString());
                        meetingObject.put("content", edtMeetingContent.getText().toString());
                        meetingObject.put("date", tvMeetingDate.getText().toString());
                        meetingObject.put("time", tvMeetingTime.getText().toString());
                        meetingObject.put("location", edtLocation.getText().toString());
                        meetingObject.put("projectId", projectModelSelected.getId());
                        meetingObject.put("createdBy", MainActivity.currentAccount.getId());
                        meetingObject.put("createdAt", meetingModel.getCreatedAt());
                        meetingObject.put("updatedAt", Utils.getCurrentTimeStamp());
                        meetingObject.put("participants", meetingModel.getParticipants());
                        meetingObject.put("members", projectModelSelected.getMembers().length);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    CreateOrUpdateMeetingAsyncTask createOrUpdateMeetingAsyncTask = new CreateOrUpdateMeetingAsyncTask(EditMeetingActivity.this, meetingObject.toString());
                    createOrUpdateMeetingAsyncTask.execute(Constants.URL_ADD_OR_UPDATE_MEETING_API);
                }
            }
        });

    }

    // get meeting model from meeting list
    private void getMeetingModelFromMeetingList() {
        Intent intentMeeting = getIntent();
        meetingModel = (MeetingModel) intentMeeting.getSerializableExtra("meeting");

    }

    //set data to edit screen
    private void setDataEdit() {
        if (meetingModel != null) {
            inputMeetingName.setText(meetingModel.getName());
            edtMeetingContent.setText(meetingModel.getContent());
            //get project model need to edit
            ProjectModel projectContainMeeting = new ProjectModel();
            if (projectModels != null && projectModels.size() > 0) {
                for (ProjectModel projectModel : projectModels) {
                    if (projectModel.getId() == meetingModel.getProjectId()) {
                        projectContainMeeting = projectModel;
                    }
                }
            }
            spnProject.setSelection(spinnerProjectAdapter.getPosition(projectContainMeeting));
            tvMeetingDate.setText(meetingModel.getDate());
            tvMeetingTime.setText(meetingModel.getTime());
            edtLocation.setText(meetingModel.getLocation());
        }
    }

    public void initView() {
        // Set a toolbar to replace the action bar.
        setToolBar("Edit appointment");
        //set color status bar
        setColorStatusBar(this);
        inputMeetingName = (AutoCompleteTextView) findViewById(R.id.edtMeetingName);
        edtMeetingContent = (EditText) findViewById(R.id.edtMeetingContent);
        spnProject = (Spinner) findViewById(R.id.spnProjectName);
        tvMeetingDate = (TextView) findViewById(R.id.tvMeetingDate);
        tvMeetingTime = (TextView) findViewById(R.id.tvMeetingTime);
        edtLocation = (EditText) findViewById(R.id.edtLocation);
        btnSaveMeeting = (Button) findViewById(R.id.btnSaveMeeting);
    }
}
