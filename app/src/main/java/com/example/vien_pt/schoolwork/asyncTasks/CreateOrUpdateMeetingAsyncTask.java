package com.example.vien_pt.schoolwork.asyncTasks;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.activities.CreateMeetingActivity;
import com.example.vien_pt.schoolwork.activities.CreateTaskActivity;
import com.example.vien_pt.schoolwork.activities.EditMeetingActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;

import java.io.IOException;

/**
 * Created by Vien-PT on 03-Nov-17.
 */

public class CreateOrUpdateMeetingAsyncTask extends AsyncTask<String, Void, String> {
    private Context context;
    private String jsonRequestEntity;
    private String result;

    public CreateOrUpdateMeetingAsyncTask(Context context, String jsonString) {
        this.context = context;
        this.jsonRequestEntity = jsonString;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            result = Utils.makeRequest(Constants.POST_METHOD,params[0],null,Constants.CONTENT_TYPE_JSON,this.jsonRequestEntity);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (result.equals("Created")){
            Toast.makeText(context,"Create successful!",Toast.LENGTH_SHORT).show();
            ((CreateMeetingActivity)context).finish();
        }else if (result.equals("Updated")){
            Toast.makeText(context,"Update successful!",Toast.LENGTH_SHORT).show();
            ((EditMeetingActivity)context).finish();
        }else if(result.equals("")){
            Toast.makeText(context,"Failed",Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(context,"Cannot connect to server!",Toast.LENGTH_SHORT).show();
        }
    }
}
