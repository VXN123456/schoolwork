package com.example.vien_pt.schoolwork.common;

import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.activities.MainActivity;
import com.example.vien_pt.schoolwork.models.ClassificationModel;
import com.example.vien_pt.schoolwork.models.CriterionModel;
import com.example.vien_pt.schoolwork.models.MeetingModel;
import com.example.vien_pt.schoolwork.models.ProfileMeetingModel;
import com.example.vien_pt.schoolwork.models.ProfileModel;
import com.example.vien_pt.schoolwork.models.ProjectModel;
import com.example.vien_pt.schoolwork.models.TaskModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vien-PT on 18-Nov-17.
 */

public class DataProcessingManager {

    //gắng tên project, tên người tạo task và tên người được giao task
    public static void setRelationFieldsForTask(List<TaskModel> taskModels, List<ProjectModel> projectModels, List<ProfileModel> profileModels) {
        if (taskModels != null && taskModels.size() > 0) {
            for (TaskModel taskModel : taskModels) {
                //set project name for task
                for (ProjectModel projectModel : projectModels) {
                    if (taskModel.getProjectId() == projectModel.getId()) {
                        taskModel.setProjectName(projectModel.getName());
                        break;
                    }
                }
                //set creator and assignee for task
                for (ProfileModel profileModel : profileModels) {
                    if (taskModel.getCreatedBy() == profileModel.getId()) {
                        taskModel.setCreator(profileModel.getFullName());
                    }
                    if (taskModel.getAssignee() == profileModel.getId()) {
                        taskModel.setAssigneeName(profileModel.getFullName());
                    }
                }
            }
        }
    }

    // gắng tên project và tên người tạo cuộc hẹn
    public static void setRelationFieldsForMeeting(List<MeetingModel> meetingModels, List<ProjectModel> projectModels, List<ProfileModel> profileModels) {
        if (meetingModels != null && meetingModels.size() > 0) {
            for (MeetingModel meetingModel : meetingModels) {
                //set project name
                for (ProjectModel projectModel : projectModels) {
                    if (meetingModel.getProjectId() == projectModel.getId()) {
                        meetingModel.setProjectName(projectModel.getName());
                        break;
                    }
                }
                //set creator
                for (ProfileModel profileModel : profileModels) {
                    if (meetingModel.getCreatedBy() == profileModel.getId()) {
                        meetingModel.setCreator(profileModel.getFullName());
                        break;
                    }
                }
            }
        }
    }

    //get Criteria belong to profile
    public static List<CriterionModel> getProfileCriteria(List<CriterionModel> criteria, int profileId) {
        List<CriterionModel> profileCriteria = new ArrayList<>();
        if (criteria != null && criteria.size() > 0) {
            for (CriterionModel criterionModel : criteria) {
                if (criterionModel.getProfileId() == profileId) {
                    profileCriteria.add(criterionModel);
                }
            }
        }
        return profileCriteria;
    }

    //get Criteria belong to profile common for all project
    public static List<CriterionModel> getProfileCriteriaCommon(List<CriterionModel> criteria, int profileId) {
        List<CriterionModel> profileCriteriaCommon = new ArrayList<>();
        if (criteria != null && criteria.size() > 0) {
            for (CriterionModel criterionModel : criteria) {
                if (criterionModel.getProfileId() == profileId && criterionModel.getProjectId() == 0) {
                    profileCriteriaCommon.add(criterionModel);
                }
            }
        }

        return profileCriteriaCommon;
    }

    //get Criteria belong to project
    public static List<CriterionModel> getProjectCriteria(List<CriterionModel> profileCriteria, int projectId) {
        List<CriterionModel> projectCriteria = new ArrayList<>();
        if (profileCriteria != null && profileCriteria.size() > 0) {
            for (CriterionModel criterionModel : profileCriteria) {
                if (criterionModel.getProjectId() == projectId) {
                    projectCriteria.add(criterionModel);
                }
            }
        }

        return projectCriteria;
    }

    //filter classifications for profile
    public static List<ClassificationModel> getProfileClassifications(List<ClassificationModel> classifications, int profileId) {
        List<ClassificationModel> profileClassifications = new ArrayList<>();
        if (classifications != null && classifications.size() > 0) {
            for (ClassificationModel classificationModel : classifications) {
                if (classificationModel.getProfileId() == profileId) {
                    profileClassifications.add(classificationModel);
                }
            }
        }
        return profileClassifications;
    }

    //get project name by project id
    public static String getProjectNameByProjectId(List<ProjectModel> projects, int projectId){
        String projectName = null;
        if (projects!=null && projects.size()>0){
            for (ProjectModel projectModel : projects){
                if (projectModel.getId() == projectId){
                    projectName = projectModel.getName();
                }
            }
        }
        return projectName;
    }

    //get project name by project id
    public static String getProfileNameByProfileId(List<ProfileModel> profiles, int profileId){
        String profileName = null;
        if (profiles!=null && profiles.size()>0){
            for (ProfileModel profileModel : profiles){
                if (profileModel.getId() == profileId){
                    profileName = profileModel.getFullName();
                }
            }
        }
        return profileName;
    }
    //get members project
    public List<ProfileModel> getMembers(Integer[] membersId){
        List<ProfileModel> members = new ArrayList<>();
        for (int idProfile = 0; idProfile < membersId.length; idProfile++) {
            for (ProfileModel profileModel : MainActivity.contactList) {
                if (profileModel.getId() == membersId[idProfile]) {
                    members.add(profileModel);
                    break;
                }
            }
        }
        return members;
    }
    //lấy các ProfileMeeting của user
    public static List<ProfileMeetingModel> getProfileProfileMeetingsByProfileId(List<ProfileMeetingModel> profileMeetings, int profileId){
        List<ProfileMeetingModel> profileProfileMeetings = new ArrayList<>();
        if (profileMeetings!=null && profileMeetings.size()>0){
            for (ProfileMeetingModel profileMeetingModel : profileMeetings){
                if (profileMeetingModel.getProfileId() == profileId){
                    profileProfileMeetings.add(profileMeetingModel);
                }
            }
        }

        return profileProfileMeetings;
    }
    //lấy các ProfileMeeting theo meetingId
    public static ProfileMeetingModel getProfileProfileMeetingsByMeetingId(List<ProfileMeetingModel> profileProfileMeetings, int meetingId){
        ProfileMeetingModel profileProfileMeeting = new ProfileMeetingModel();
        if (profileProfileMeetings!=null && profileProfileMeetings.size()>0){
            for (ProfileMeetingModel profileMeetingModel : profileProfileMeetings){
                if (profileMeetingModel.getMeetingId() == meetingId){
                    profileProfileMeeting = profileMeetingModel;
                }
            }
        }
        return  profileProfileMeeting;
    }

    //get next meeting in my meeting
    public static List<MeetingModel> getNextMeetings(List<MeetingModel> profileMeetings){
        List<MeetingModel> nextMeetings = new ArrayList<>();
        if (profileMeetings!=null && profileMeetings.size()>0){
            long currentTime = Utils.convertTimeStampToLongMillisecond(Utils.getCurrentTimeStamp());
            for(MeetingModel meetingModel : profileMeetings){
                long meetingTime = Utils.convertStringDateToLongMillisecond(meetingModel.getDate())
                        +Utils.convertStringTimeToLongMillisecond(meetingModel.getTime()+":00");
                if (!Utils.isPassCurrentTime(currentTime,meetingTime)){
                    nextMeetings.add(meetingModel);
                }
            }
        }
        return nextMeetings;
    }

    //get open tasks
    public static List<TaskModel> getOpenTaks(List<TaskModel> profileTask){
        List<TaskModel> openTasks = new ArrayList<>();
        if (profileTask !=null && profileTask.size()>0){
            for (TaskModel taskModel : LoginActivity.profileTasks){
                if (taskModel.getStatus().equals("Open")){
                    openTasks.add(taskModel);
                }
            }
        }
        return openTasks;
    }
    //get completed tasks
    public static List<TaskModel> getCompletedTasks(List<TaskModel> profileTask){
        List<TaskModel> completedTasks = new ArrayList<>();
        if (profileTask !=null && profileTask.size()>0){
            for (TaskModel taskModel : LoginActivity.profileTasks){
                if (taskModel.getStatus().equals("Completed")){
                    completedTasks.add(taskModel);
                }
            }
        }
        return completedTasks;
    }
    //get completed tasks
    public static List<TaskModel> getRequestedTasks(List<TaskModel> profileTask, int profileId){
        List<TaskModel> requestedTasks = new ArrayList<>();
        if (profileTask !=null && profileTask.size()>0){
            for (TaskModel taskModel : LoginActivity.profileTasks){
                if (taskModel.getCreatedBy()== profileId){
                    requestedTasks.add(taskModel);
                }
            }
        }
        return requestedTasks;
    }
}
