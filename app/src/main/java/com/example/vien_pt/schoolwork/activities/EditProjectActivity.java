package com.example.vien_pt.schoolwork.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.adapters.ContactAdapter;
import com.example.vien_pt.schoolwork.asyncTasks.CreateOrUpdateProjectAsyncTask;
import com.example.vien_pt.schoolwork.base.BaseActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.ProfileModel;
import com.example.vien_pt.schoolwork.models.ProjectModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EditProjectActivity extends BaseActivity {
    AutoCompleteTextView edtProjectCode;
    AutoCompleteTextView edtProjectName;
    TextView tvDescription;
    TextView tvRequirement;
    TextView tvKeyword;
    TextView tvReference;
    TextView tvMainGuide;
    EditText edtStartDate;
    EditText edtEndDate;
    ListView lvContact;
    Button btnUpdateProject;
    ProjectModel projectModel;
    DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_project);
        //init view
        initView();
        setDataEdit();

        setActionEdit();

        MainActivity.isContactToAdd = true;
        if (MainActivity.contactList != null && MainActivity.contactList.size() > 0) {
            ContactAdapter contactAdapter = new ContactAdapter(EditProjectActivity.this, R.layout.item_contact, MainActivity.contactList, false);
            lvContact.setAdapter(contactAdapter);
        }

        lvContact.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        btnUpdateProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isConnectingToInternet(getApplicationContext())) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(EditProjectActivity.this, R.style.DialogThemMain);
                    builder.setMessage("No internet connection, please try again!")
                            .setPositiveButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            }).show();
                } else if (edtProjectName.getText().toString().equals(Constants.EMPTY)) {
                    Toast.makeText(getApplicationContext(), "Please enter project name!", Toast.LENGTH_SHORT).show();
                } else {
                    JSONObject projectObject = new JSONObject();
                    projectModel.setMembers(ProjectDetailActivity.membersEditToProject
                            .toArray(new Integer[ProjectDetailActivity.membersEditToProject.size()]));
                    ProjectDetailActivity.membersEditToProject.add(LoginActivity.accountId);
                    JSONArray arrayMembers = new JSONArray(ProjectDetailActivity.membersEditToProject);

                    try {
                        projectObject.put("id", projectModel.getId());
                        projectObject.put("name", edtProjectName.getText().toString());
                        projectModel.setName(edtProjectName.getText().toString());
                        projectObject.put("code", edtProjectCode.getText().toString());
                        projectModel.setCode(edtProjectCode.getText().toString());
                        projectObject.put("createdBy", MainActivity.currentAccount.getId());
                        projectObject.put("description", tvDescription.getText().toString());
                        projectModel.setDescription(tvDescription.getText().toString());
                        projectObject.put("requirement", tvRequirement.getText().toString());
                        projectModel.setRequirement(tvRequirement.getText().toString());
                        projectObject.put("keyword", tvKeyword.getText().toString());
                        projectModel.setKeyword(tvKeyword.getText().toString());
                        projectObject.put("reference", tvReference.getText().toString());
                        projectModel.setReference(tvReference.getText().toString());
                        projectObject.put("createdAt", projectModel.getCreatedAt());
                        projectObject.put("updatedAt", Utils.getCurrentTimeStamp());
                        projectObject.put("timeStart", edtStartDate.getText().toString());
                        projectModel.setTimeStart(edtStartDate.getText().toString());
                        projectObject.put("timeEnd", edtEndDate.getText().toString());
                        projectModel.setTimeEnd(edtEndDate.getText().toString());
                        projectObject.put("mainGuide", projectModel.getMainGuide());

                        projectObject.put("members", arrayMembers);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    CreateOrUpdateProjectAsyncTask createOrUpdateProjectAsyncTask = new CreateOrUpdateProjectAsyncTask(EditProjectActivity.this, projectObject.toString(), projectModel);
                    createOrUpdateProjectAsyncTask.execute(Constants.URL_ADD_OR_UPDATE_PROJECT_API);
                }
            }
        });
    }

    private void setActionEdit() {
        final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        //enter description
        tvDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialogEdit = new Dialog(EditProjectActivity.this);
                dialogEdit.setContentView(R.layout.dialog_edit_common);
                TextView tvTitle = (TextView) dialogEdit.findViewById(R.id.tvTitle);
                final EditText edtCommon = (EditText) dialogEdit.findViewById(R.id.edtCommon);
                Button btnSave = (Button) dialogEdit.findViewById(R.id.btnSave);
                tvTitle.setText("Descriptions");
                if (!tvDescription.getText().toString().equals(Constants.NULL_STRING)) {
                    edtCommon.setText(tvDescription.getText().toString());
                    edtCommon.setSelection(edtCommon.getText().toString().length());
                }
                edtCommon.requestFocus();
                inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!edtCommon.getText().toString().trim().equals(Constants.NULL_STRING) && !edtCommon.getText().toString().trim().equals(Constants.EMPTY)) {
                            tvDescription.setText(edtCommon.getText().toString().trim());
                        }
                        tvDescription.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                int lines = tvDescription.getLayout().getLineCount();
                                if (lines > 1) {
                                    tvDescription.setLines(1);
                                    tvDescription.setEllipsize(TextUtils.TruncateAt.END);
                                }
                                tvDescription.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            }
                        });
                        dialogEdit.dismiss();
                    }
                });
                dialogEdit.show();
            }
        });
        //Enter requirement
        tvRequirement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialogEdit = new Dialog(EditProjectActivity.this);
                dialogEdit.setContentView(R.layout.dialog_edit_common);
                TextView tvTitle = (TextView) dialogEdit.findViewById(R.id.tvTitle);
                final EditText edtCommon = (EditText) dialogEdit.findViewById(R.id.edtCommon);
                Button btnSave = (Button) dialogEdit.findViewById(R.id.btnSave);
                tvTitle.setText("Requirements");
                if (!tvRequirement.getText().toString().equals(Constants.NULL_STRING)) {
                    edtCommon.setText(tvRequirement.getText().toString());
                    edtCommon.setSelection(edtCommon.getText().toString().length());
                }
                edtCommon.requestFocus();
                inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!edtCommon.getText().toString().trim().equals(Constants.NULL_STRING) && !edtCommon.getText().toString().trim().equals(Constants.EMPTY)) {
                            tvRequirement.setText(edtCommon.getText().toString().trim());
                        }
                        tvRequirement.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                int lines = tvRequirement.getLayout().getLineCount();
                                if (lines > 1) {
                                    tvRequirement.setLines(1);
                                    tvRequirement.setEllipsize(TextUtils.TruncateAt.END);
                                }
                                tvRequirement.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            }
                        });
                        dialogEdit.dismiss();
                    }
                });
                dialogEdit.show();
            }
        });
        //Enter keywords
        tvKeyword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialogEdit = new Dialog(EditProjectActivity.this);
                dialogEdit.setContentView(R.layout.dialog_edit_common);
                TextView tvTitle = (TextView) dialogEdit.findViewById(R.id.tvTitle);
                final EditText edtCommon = (EditText) dialogEdit.findViewById(R.id.edtCommon);
                Button btnSave = (Button) dialogEdit.findViewById(R.id.btnSave);
                tvTitle.setText("Keywords");
                if (!tvKeyword.getText().toString().equals(Constants.NULL_STRING)) {
                    edtCommon.setText(tvKeyword.getText().toString());
                    edtCommon.setSelection(edtCommon.getText().toString().length());
                }
                edtCommon.requestFocus();
                inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!edtCommon.getText().toString().trim().equals(Constants.NULL_STRING) && !edtCommon.getText().toString().trim().equals(Constants.EMPTY)) {
                            tvKeyword.setText(edtCommon.getText().toString().trim());
                        }
                        tvKeyword.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                int lines = tvKeyword.getLayout().getLineCount();
                                if (lines > 1) {
                                    tvKeyword.setLines(1);
                                    tvKeyword.setEllipsize(TextUtils.TruncateAt.END);
                                }
                                tvKeyword.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            }
                        });
                        dialogEdit.dismiss();
                    }
                });
                dialogEdit.show();
            }
        });
        //Enter references
        tvReference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialogEdit = new Dialog(EditProjectActivity.this);
                dialogEdit.setContentView(R.layout.dialog_edit_common);
                TextView tvTitle = (TextView) dialogEdit.findViewById(R.id.tvTitle);
                final EditText edtCommon = (EditText) dialogEdit.findViewById(R.id.edtCommon);
                Button btnSave = (Button) dialogEdit.findViewById(R.id.btnSave);
                tvTitle.setText("References");
                if (!tvReference.getText().toString().equals(Constants.NULL_STRING)) {
                    edtCommon.setText(tvReference.getText().toString());
                    edtCommon.setSelection(edtCommon.getText().toString().length());
                }
                edtCommon.requestFocus();
                inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!edtCommon.getText().toString().trim().equals(Constants.NULL_STRING) && !edtCommon.getText().toString().trim().equals(Constants.EMPTY)) {
                            tvReference.setText(edtCommon.getText().toString().trim());
                        }
                        tvReference.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                int lines = tvReference.getLayout().getLineCount();
                                if (lines > 1) {
                                    tvReference.setLines(1);
                                    tvReference.setEllipsize(TextUtils.TruncateAt.END);
                                }
                                tvReference.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            }
                        });
                        dialogEdit.dismiss();
                    }
                });
                dialogEdit.show();
            }
        });

        tvMainGuide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialogEditMainGuide = new Dialog(EditProjectActivity.this);
                dialogEditMainGuide.setContentView(R.layout.dialog_edit_main_guide);
                ListView lvTeacher = (ListView) dialogEditMainGuide.findViewById(R.id.lvTeacher);
                final List<ProfileModel> teacherModels = new ArrayList<>();
                for (ProfileModel member : ProjectDetailActivity.members) {
                    if (member.getRole().equals("Teacher")){
                        teacherModels.add(member);
                    }
                }
                ArrayAdapter<ProfileModel> arrayAdapterTeacher = new ArrayAdapter<ProfileModel>(EditProjectActivity.this, R.layout.item_teacher,teacherModels){
                    int selectedPosition = 0;

                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View v = convertView;
                        ProfileModel profileModel = getItem(position);

                        if (v == null) {
                            LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            v = vi.inflate(R.layout.item_teacher, null);
                        }
                        TextView tvTeacherName = (TextView)v.findViewById(R.id.tvProfileName);
                        TextView tvRole = (TextView)v.findViewById(R.id.tvRole);
                        if (profileModel != null) {
                            tvTeacherName.setText(profileModel.getFullName());
                            tvRole.setText(profileModel.getRole());
                        }
                        RadioButton rbSelectMainGuide = (RadioButton)v.findViewById(R.id.rbSelectMainGuide);
                        if (profileModel.getId()==projectModel.getMainGuide()){
                            selectedPosition = position;
                        }
                        rbSelectMainGuide.setChecked(position == selectedPosition);
                        rbSelectMainGuide.setTag(position);
                        rbSelectMainGuide.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                selectedPosition = (Integer)view.getTag();
                                projectModel.setMainGuide(teacherModels.get(selectedPosition).getId());
                                notifyDataSetChanged();
                            }
                        });
                        return v;
                    }

                };
                lvTeacher.setAdapter(arrayAdapterTeacher);
                lvTeacher.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                        Toast.makeText(EditProjectActivity.this, String.valueOf(position), Toast.LENGTH_SHORT).show();
                        tvMainGuide.setText(teacherModels.get(position).getFullName());
                        projectModel.setMainGuide(teacherModels.get(position).getId());
                        dialogEditMainGuide.dismiss();
                    }
                });
                dialogEditMainGuide.setCancelable(false);
                dialogEditMainGuide.show();
            }
        });

    }

    private void setDataEdit() {
        Intent intent = getIntent();
        projectModel = (ProjectModel) intent.getSerializableExtra("project");
        if (projectModel != null) {
            if (!projectModel.getName().equals(Constants.NULL_STRING)) {
                edtProjectName.setText(projectModel.getName());

            }
            if (!projectModel.getDescription().equals(Constants.NULL_STRING)) {
                tvDescription.setText(projectModel.getDescription());
                tvDescription.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        int lines = tvDescription.getLayout().getLineCount();
                        if (lines>1){
                            tvDescription.setLines(1);
                            tvDescription.setEllipsize(TextUtils.TruncateAt.END);
                        }
                        tvDescription.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
            }
            if (!projectModel.getRequirement().equals(Constants.NULL_STRING)) {
                tvRequirement.setText(projectModel.getRequirement());
                tvRequirement.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        int lines = tvRequirement.getLayout().getLineCount();
                        if (lines>1){
                            tvRequirement.setLines(1);
                            tvRequirement.setEllipsize(TextUtils.TruncateAt.END);
                        }
                        tvRequirement.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
            }
            if (!projectModel.getKeyword().equals(Constants.NULL_STRING)) {
                tvKeyword.setText(projectModel.getKeyword());
                tvKeyword.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        int lines = tvKeyword.getLayout().getLineCount();
                        if (lines>1){
                            tvKeyword.setLines(1);
                            tvKeyword.setEllipsize(TextUtils.TruncateAt.END);
                        }
                        tvKeyword.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
            }
            if (!projectModel.getReference().equals(Constants.NULL_STRING)) {
                tvReference.setText(projectModel.getReference());
                tvReference.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        int lines = tvReference.getLayout().getLineCount();
                        if (lines>1){
                            tvReference.setLines(1);
                            tvReference.setEllipsize(TextUtils.TruncateAt.END);
                        }
                        tvReference.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
            }
            if (!projectModel.getCode().equals(Constants.NULL_STRING)) {
                edtProjectCode.setText(projectModel.getCode());
            }
            if (!projectModel.getTimeStart().equals(Constants.NULL_STRING)) {
                edtStartDate.setText(projectModel.getTimeStart());
            }
            if (!projectModel.getTimeEnd().equals(Constants.NULL_STRING)) {
                edtEndDate.setText(projectModel.getTimeEnd());
            }
            if (projectModel.getMainGuide() != 0) {
                for (ProfileModel profileModel : LoginActivity.profiles) {
                    if (profileModel.getId() == projectModel.getMainGuide()) {
                        tvMainGuide.setText(profileModel.getFullName());
                    }
                }
            }
        }

        edtStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR); // current year
                int month = c.get(Calendar.MONTH); // current month
                int day = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(EditProjectActivity.this, android.R.style.Theme_DeviceDefault_Light_Dialog,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                edtStartDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, year, month, day);
                datePickerDialog.show();
            }
        });
        edtEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR); // current year
                int month = c.get(Calendar.MONTH); // current month
                int day = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(EditProjectActivity.this, android.R.style.Theme_DeviceDefault_Light_Dialog,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                edtEndDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, year, month, day);
                datePickerDialog.show();
            }
        });
    }

    public void initView() {
        setColorStatusBar(this);
        setToolBar("Edit Project");
        edtProjectName = (AutoCompleteTextView) findViewById(R.id.edtProjectName);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
        tvRequirement = (TextView) findViewById(R.id.tvRequirement);
        tvKeyword = (TextView) findViewById(R.id.tvKeyword);
        tvReference = (TextView) findViewById(R.id.tvReference);
        tvMainGuide = (TextView) findViewById(R.id.tvMainGuide);
        edtProjectCode = (AutoCompleteTextView) findViewById(R.id.edtProjectCode);
        edtStartDate = (AutoCompleteTextView) findViewById(R.id.edtStartDate);
        edtEndDate = (AutoCompleteTextView) findViewById(R.id.edtEndDate);
        lvContact = (ListView) findViewById(R.id.lvContact);
        btnUpdateProject = (Button) findViewById(R.id.btnUpdateProject);
    }
}
