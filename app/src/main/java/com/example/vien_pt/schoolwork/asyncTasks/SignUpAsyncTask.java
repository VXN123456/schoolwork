package com.example.vien_pt.schoolwork.asyncTasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.activities.SignUpActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.ProfileModel;
import com.example.vien_pt.schoolwork.models.ProjectModel;

import java.io.IOException;

/**
 * Created by Vien-PT on 05-Nov-17.
 */

public class SignUpAsyncTask extends AsyncTask<String, Void, String> {
    Context context;
    String strObjectSignUp;
    private ProgressDialog progressDialog;
    String messageResponse = null;
    String resultRegistration = null;
    private ProfileModel profileModel;
    public SignUpAsyncTask(Context context, String strObject , ProfileModel profileModel) {
        this.strObjectSignUp = strObject;
        this.profileModel = profileModel;
        this.progressDialog = new ProgressDialog(context, R.style.DialogThemMain);
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setMessage("Registering, please wait...");
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            messageResponse = Utils.makeRequest(Constants.POST_METHOD,params[0],null,Constants.CONTENT_TYPE_JSON,this.strObjectSignUp);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return messageResponse;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        progressDialog.dismiss();
        if (messageResponse.equals("Duplicated Email")){
            resultRegistration = "Existed Email";
            Toast.makeText(context,resultRegistration,Toast.LENGTH_SHORT).show();
        }else if(messageResponse.equals("Created")){
            resultRegistration = "Registration successful!";
            Toast.makeText(context,resultRegistration,Toast.LENGTH_SHORT).show();
            context.startActivity(new Intent(context, LoginActivity.class));
            ((Activity)context).finish();
        }else if(messageResponse.equals("Updated")){
            Toast.makeText(context,"Updated!",Toast.LENGTH_SHORT).show();
            ((Activity)context).setResult(Activity.RESULT_OK,new Intent().putExtra("profile",profileModel));
            ((Activity)context).finish();
        }else {
            Toast.makeText(context,"Registration failed!",Toast.LENGTH_SHORT).show();
        }
    }
}
