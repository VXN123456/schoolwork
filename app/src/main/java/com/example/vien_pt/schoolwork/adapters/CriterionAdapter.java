package com.example.vien_pt.schoolwork.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.MainActivity;
import com.example.vien_pt.schoolwork.activities.ProjectEvaluationActivity;
import com.example.vien_pt.schoolwork.activities.SettingCriteriaActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.models.CriterionModel;

import java.util.List;

/**
 * Created by Vien-PT on 24-Nov-17.
 */

public class CriterionAdapter extends ArrayAdapter<CriterionModel> {
    Context context;
    List<CriterionModel> projectCriteria;
    boolean isSetting;

    public CriterionAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<CriterionModel> objects, boolean isSetting) {
        super(context, resource, objects);
        this.context = context;
        this.isSetting = isSetting;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable final View convertView, @NonNull ViewGroup parent) {

        View view = convertView;
        final CriterionModel criterionModel = getItem(position);
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_criterion, null);
        }
        final InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);

        final TextView tvCriterionName = (TextView) view.findViewById(R.id.tvCriterionName);
        final TextView tvWeight = (TextView) view.findViewById(R.id.tvWeight);
        final TextView tvScore = (TextView) view.findViewById(R.id.tvScore);
        final TextView tvNote = (TextView) view.findViewById(R.id.tvNote);
        final TextView tvSettingNote = (TextView) view.findViewById(R.id.tvSettingNote);
        ImageView imvEditCriterion = (ImageView) view.findViewById(R.id.imvEditCriterion);
        ImageView imvDeleteCriterion = (ImageView) view.findViewById(R.id.imvDeleteCriterion);
        LinearLayout lnlScore = (LinearLayout) view.findViewById(R.id.lnlScore);
        LinearLayout lnlNote = (LinearLayout) view.findViewById(R.id.lnlNote);
        RelativeLayout rlCriterionControl = (RelativeLayout) view.findViewById(R.id.rlCriterionControl);

        if (criterionModel != null) {
            tvCriterionName.setText(criterionModel.getName());
            tvWeight.setText(String.valueOf(criterionModel.getWeight()));
            tvScore.setText(String.valueOf(criterionModel.getScore()));
            if (!criterionModel.getNote().equals(Constants.NULL_STRING)){
                tvNote.setText(criterionModel.getNote());
            }
        }
        //edit note
        tvNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialogEditNote = new Dialog(context);
                dialogEditNote.setContentView(R.layout.dialog_edit_criterion_note);
                final EditText edtNote = (EditText) dialogEditNote.findViewById(R.id.edtNote);
                if (!criterionModel.getNote().equals(Constants.NULL_STRING)){
                    edtNote.setText(criterionModel.getNote());
                }
                edtNote.requestFocus();
                inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                Button btnSaveNote = (Button) dialogEditNote.findViewById(R.id.btnSaveNote);
                btnSaveNote.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!edtNote.getText().toString().equals("")) {
                            criterionModel.setNote(edtNote.getText().toString());
                            tvNote.setText(edtNote.getText().toString());
                        } else {
                            criterionModel.setNote(" ");
                            tvNote.setText(" ");
                        }
                        ((ProjectEvaluationActivity)context).setChangedEvaluate(true);
                        dialogEditNote.dismiss();
                    }
                });
                dialogEditNote.show();
            }
        });
        //edit score
        tvScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialogEditNote = new Dialog(context);
                dialogEditNote.setContentView(R.layout.dialog_edit_criterion_score);
                final EditText edtScore = (EditText) dialogEditNote.findViewById(R.id.edtScore);
                if (criterionModel != null) {
                    edtScore.setText(String.valueOf(criterionModel.getScore()));
                    edtScore.setSelection(edtScore.getText().length());
                    edtScore.requestFocus();
                    inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }
                Button btnSaveScore = (Button) dialogEditNote.findViewById(R.id.btnSaveScore);
                btnSaveScore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            double value = Double.parseDouble(edtScore.getText().toString());
                            criterionModel.setScore(Double.parseDouble(edtScore.getText().toString()));
                            tvScore.setText(String.valueOf(value));
                            dialogEditNote.dismiss();
                            ((ProjectEvaluationActivity) context).setTotalScore();
                            ((ProjectEvaluationActivity)context).setClassification();
                            ((ProjectEvaluationActivity)context).setChangedEvaluate(true);
                        } catch (Exception e) {
                            Toast.makeText(context, "Score invalid! Please enter valid score!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                dialogEditNote.show();
            }
        });


        //action in setting criteria
        if (isSetting) {
            lnlNote.setVisibility(View.GONE);
            lnlScore.setVisibility(View.GONE);
            rlCriterionControl.setVisibility(View.VISIBLE);
            if (criterionModel != null) {
                if (!criterionModel.getNote().equals(Constants.NULL_STRING)){
                    tvSettingNote.setText(criterionModel.getNote());
                }
            }
            imvEditCriterion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (MainActivity.currentAccount.getRole().equals("Teacher")) {// giao vien moi co quyen chinh sua
                        final Dialog dialogEditCriterion = new Dialog(context);
                        dialogEditCriterion.setContentView(R.layout.dialog_edit_criterion);
                        final EditText edtCriterionName = (EditText) dialogEditCriterion.findViewById(R.id.edtCriterionName);
                        final EditText edtCriterionWeight = (EditText) dialogEditCriterion.findViewById(R.id.edtCriterionWeight);
                        final EditText edtCriterionNote = (EditText) dialogEditCriterion.findViewById(R.id.edtCriterionNote);
                        Button btnUpdateCriterion = (Button) dialogEditCriterion.findViewById(R.id.btnUpdateCriterion);
                        if (criterionModel != null) {
                            edtCriterionName.setText(criterionModel.getName());
                            edtCriterionWeight.setText(String.valueOf(criterionModel.getWeight()));
                            if (!criterionModel.getNote().equals(Constants.NULL_STRING)){
                                edtCriterionNote.setText(criterionModel.getNote());
                            }
                        }
                        // save after edit criterion
                        btnUpdateCriterion.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (edtCriterionName.getText().toString().trim().equals("")) {
                                    Toast.makeText(context, "Please enter criterion name", Toast.LENGTH_SHORT).show();
                                } else {
                                    try {
                                        tvCriterionName.setText(edtCriterionName.getText().toString());
                                        int value = Integer.parseInt(edtCriterionWeight.getText().toString());
                                        tvWeight.setText(String.valueOf(value));
                                        if (edtCriterionNote.getText().toString().equals(" ")) {
                                            tvSettingNote.setText(" ");
                                        } else {
                                            tvSettingNote.setText(edtCriterionNote.getText().toString());
                                        }
                                        criterionModel.setName(edtCriterionName.getText().toString());
                                        criterionModel.setWeight(value);
                                        criterionModel.setNote(edtCriterionNote.getText().toString());
                                        ((SettingCriteriaActivity)context).setChangedCriteriaSetting(true);
                                        dialogEditCriterion.dismiss();
                                    } catch (Exception e) {
                                        Toast.makeText(context, "Weight is an integer value!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        });
                        dialogEditCriterion.show();
                    } else {
                        Toast.makeText(context, "This function for Teacher role!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            // perform delete criterion
            imvDeleteCriterion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(context, "Deleting", Toast.LENGTH_SHORT).show();
                    new android.support.v7.app.AlertDialog.Builder(context, R.style.DialogThemMain)
                            .setMessage("Do you really want to delete this criterion?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (MainActivity.currentAccount.getRole().equals("Teacher") && MainActivity.currentAccount.getId() == criterionModel.getProfileId()) {
                                        remove(criterionModel);
                                        notifyDataSetChanged();
                                        ((SettingCriteriaActivity)context).setChangedCriteriaSetting(true);
                                    } else {
                                        Toast.makeText(context, "Permission Denied", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            })
                            .setNegativeButton("No", null)
                            .show();
                }
            });
        }

        return view;
    }
}
