package com.example.vien_pt.schoolwork.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.activities.MainActivity;
import com.example.vien_pt.schoolwork.adapters.MeetingAdapter;
import com.example.vien_pt.schoolwork.asyncTasks.MeetingsProfileAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.ProfileMeetingsAsyncTask;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.DataProcessingManager;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.MeetingModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Vien-PT on 01-Dec-17.
 */

public class MyMeetingsFragment extends Fragment {

    MeetingAdapter meetingAdapter;
    ListView lvMeetings;
    SwipeRefreshLayout swipeRefreshMeetingList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_meetings, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeRefreshMeetingList = (SwipeRefreshLayout) getView().findViewById(R.id.swipeRefreshMeetingList);
        lvMeetings = (ListView) getView().findViewById(R.id.lvMeetings);

        DataProcessingManager.setRelationFieldsForMeeting(LoginActivity.profileMeetings, LoginActivity.projects, LoginActivity.profiles);

        meetingAdapter = new MeetingAdapter(getActivity(), R.layout.item_meeting, LoginActivity.profileMeetings);
        lvMeetings.setAdapter(meetingAdapter);
        lvMeetings.setScrollingCacheEnabled(false);

        swipeRefreshMeetingList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshMeetingList.setRefreshing(false);
                onResume();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        //reload projectMeeting for user
        new ProfileMeetingsAsyncTask(getActivity()).execute(Constants.URL_PROFILEMEETINGS_BY_PROFILE_API + LoginActivity.accountId);
        //get projects
        new MeetingsProfileAsyncTask(getActivity()).execute(Constants.URL_PROFILE_MEETINGS_API + String.valueOf(LoginActivity.accountId));


        DataProcessingManager.setRelationFieldsForMeeting(LoginActivity.profileMeetings, LoginActivity.projects, LoginActivity.profiles);
        meetingAdapter = new MeetingAdapter(getActivity(), R.layout.item_meeting, LoginActivity.profileMeetings);
        lvMeetings.setAdapter(meetingAdapter);
        lvMeetings.setScrollingCacheEnabled(false);
        meetingAdapter.notifyDataSetChanged();

        ((MainActivity)getActivity()).setNumberNotice();
    }

}
