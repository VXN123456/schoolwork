package com.example.vien_pt.schoolwork.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.adapters.CriterionAdapter;
import com.example.vien_pt.schoolwork.asyncTasks.CreateOrUpdateCriterionAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.CreateOrUpdateProjectAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.CriteriaAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.ProjectResetCriteriaAsyncTask;
import com.example.vien_pt.schoolwork.base.BaseActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.DataProcessingManager;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.ClassificationModel;
import com.example.vien_pt.schoolwork.models.CriterionModel;
import com.example.vien_pt.schoolwork.models.ProjectModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProjectEvaluationActivity extends BaseActivity {
    private boolean isChangedEvaluate = false;

    public boolean isChangedEvaluate() {
        return isChangedEvaluate;
    }

    public void setChangedEvaluate(boolean changedEvaluate) {
        isChangedEvaluate = changedEvaluate;
    }

    ListView lvCriteria;
    //    EditText edtGeneralEvaluation;
    TextView tvClassification;
    TextView tvTotalScore;
    ProjectModel projectModel;
    public Button btnSaveEvaluate;
    CriterionAdapter criterionAdapter;
    SwipeRefreshLayout swipeRefreshCriteriaList;
    List<CriterionModel> projectCriteria;
    List<ClassificationModel> profileClassifications;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_evaluation);
        initView();
        getProjectModeTransfer();

        projectCriteria = new ArrayList<>();
        profileClassifications = new ArrayList<>();
        //lấy danh sách tiêu chí đánh giá của project
        projectCriteria = DataProcessingManager.getProjectCriteria(DataProcessingManager.getProfileCriteria(
                LoginActivity.criteria, projectModel.getMainGuide()), projectModel.getId());
        //lấy các setting xếp loại của giáo viên hướng dẫn chính của project
        profileClassifications = DataProcessingManager.getProfileClassifications(MainActivity.classifications, projectModel.getMainGuide());
        //kiem tra project da duoc danh gia hay chua neu chua se lay danh sach cac tieu chi chung cua giao vien huong dan chinh
        //neu da co thi se hien thi thong tin danh gia truoc
        if (projectCriteria == null || projectCriteria.size() == 0) {
            //lay cac tieu chi cua giao vien huong dan chinh
            projectCriteria = DataProcessingManager.getProfileCriteriaCommon(LoginActivity.criteria, projectModel.getMainGuide());
        }
        criterionAdapter = new CriterionAdapter(ProjectEvaluationActivity.this, R.layout.item_criterion, projectCriteria, false);
        lvCriteria.setAdapter(criterionAdapter);

        //set đánh giá chung
        if (profileClassifications != null && profileClassifications.size() == 0) {

        }

        //refresh list criteria by pull down
        swipeRefreshCriteriaList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onResume();
            }
        });

        btnSaveEvaluate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //chỉ có giáo viên là hướng dẫn chính của project mới được đánh giá cũng như chỉnh sửa đánh giá cho project đó
                if (MainActivity.currentAccount.getRole().equals("Teacher") && projectModel.getMainGuide() == LoginActivity.accountId) {
                    if (projectCriteria != null && projectCriteria.size() > 0) {
                        JSONArray arrayCriteria = new JSONArray();
                        for (CriterionModel criterionModel : projectCriteria) {
                            JSONObject objectCriterion = new JSONObject();
                            try {
                                objectCriterion.put("id", criterionModel.getId());
                                objectCriterion.put("profileId", LoginActivity.accountId);
                                objectCriterion.put("projectId", projectModel.getId());
                                objectCriterion.put("name", criterionModel.getName());
                                objectCriterion.put("weight", criterionModel.getWeight());
                                objectCriterion.put("score", criterionModel.getScore());
                                objectCriterion.put("note", criterionModel.getNote());
                                arrayCriteria.put(objectCriterion);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        new CreateOrUpdateCriterionAsyncTask(ProjectEvaluationActivity.this
                                , arrayCriteria.toString()).execute(Constants.URL_ADD_OR_UPDATE_CRITERION_API);

                        //save
                        JSONObject projectObject = new JSONObject();
                        try {
                            if (projectModel != null) {
                                double score;
                                if (tvTotalScore.getText().toString().equals("") || tvTotalScore.getText().toString().length() == 0) {
                                    score = 0;
                                } else {
                                    score = Double.valueOf(tvTotalScore.getText().toString());
                                }
                                projectObject.put("id", projectModel.getId());
                                projectObject.put("name", projectModel.getName());
                                projectModel.setRating(tvClassification.getText().toString());
                                projectObject.put("rating", tvClassification.getText().toString());
                                projectObject.put("score", score);
                                projectObject.put("code", projectModel.getCode());
                                projectObject.put("createdBy", projectModel.getCreatedBy());
                                projectObject.put("description", projectModel.getDescription());
                                projectObject.put("createdAt", projectModel.getCreatedAt());
                                projectObject.put("updatedAt", Utils.getCurrentTimeStamp());
                                projectObject.put("timeStart", projectModel.getTimeStart());
                                projectObject.put("timeEnd", projectModel.getTimeEnd());
                                projectObject.put("mainGuide", projectModel.getMainGuide());
                                projectObject.put("members", new JSONArray(projectModel.getMembers()));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        CreateOrUpdateProjectAsyncTask createOrUpdateProjectAsyncTask = new CreateOrUpdateProjectAsyncTask(ProjectEvaluationActivity.this, projectObject.toString(), null);
                        createOrUpdateProjectAsyncTask.execute(Constants.URL_ADD_OR_UPDATE_PROJECT_API);

                        //get criteria
                        new CriteriaAsyncTask().execute(Constants.URL_GET_CRITERIA_API);
                    }
                } else {
                    Toast.makeText(ProjectEvaluationActivity.this, "This function just for main guide Teacher!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void getProjectModeTransfer() {
        Intent intent = getIntent();
        projectModel = (ProjectModel) intent.getSerializableExtra("project");
    }

    private void initView() {
        setToolBar("Project Evaluation");
        setColorStatusBar(this);
        lvCriteria = (ListView) findViewById(R.id.lvCriteria);
        tvClassification = (TextView) findViewById(R.id.tvClassification);
        tvTotalScore = (TextView) findViewById(R.id.tvTotalScore);
        btnSaveEvaluate = (Button) findViewById(R.id.btnSaveEvaluate);
        swipeRefreshCriteriaList = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshCriteriaList);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.evaluate_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settingCriteria:
                if (LoginActivity.accountId == projectModel.getMainGuide() || MainActivity.currentAccount.getRole().equals("Teacher")) {
                    startActivity(new Intent(ProjectEvaluationActivity.this, SettingCriteriaActivity.class));
                } else {
                    Toast.makeText(this, "This function only for Teacher!", Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.settingClassification:
                if (LoginActivity.accountId == projectModel.getMainGuide() || MainActivity.currentAccount.getRole().equals("Teacher")) {
                    startActivityForResult(new Intent(ProjectEvaluationActivity.this, SettingClassificationsActivity.class)
                            .putExtra("classifications", (Serializable) profileClassifications), 3);
                } else {
                    Toast.makeText(this, "This function only for Teacher!", Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.resetEvaluate:
                if (Utils.isConnectingToInternet(getApplicationContext())) {
                    if (projectModel.getMainGuide() == LoginActivity.accountId) {
                        new ProjectResetCriteriaAsyncTask(ProjectEvaluationActivity.this)
                                .execute(Constants.URL_PROJECT_RESET_CRITERIA_API + projectModel.getMainGuide() + "/" + projectModel.getId());
                        onResume();
                    } else {
                        Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "Can not connect internet!", Toast.LENGTH_SHORT).show();
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 3 && resultCode == RESULT_OK) {
            profileClassifications = (List<ClassificationModel>) data.getSerializableExtra("classifications");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //get criteria
        new CriteriaAsyncTask().execute(Constants.URL_GET_CRITERIA_API);
        swipeRefreshCriteriaList.setRefreshing(false);
        //lấy danh sách tiêu chí đánh giá của project
        projectCriteria = DataProcessingManager.getProjectCriteria(DataProcessingManager.getProfileCriteria(LoginActivity.criteria, projectModel.getMainGuide()), projectModel.getId());
        //kiem tra project da duoc danh gia hay chua neu chua se lay danh sach cac tieu chi chung cua giao vien huong dan chinh
        //neu da co thi se hien thi thong tin danh gia truoc
        if (projectCriteria == null || projectCriteria.size() == 0) {
            //lay cac tieu chi cua giao vien huong dan chinh
            projectCriteria = DataProcessingManager.getProfileCriteriaCommon(LoginActivity.criteria, projectModel.getMainGuide());
        }
        criterionAdapter = new CriterionAdapter(ProjectEvaluationActivity.this, R.layout.item_criterion, projectCriteria, false);
        lvCriteria.setAdapter(criterionAdapter);

        tvTotalScore.setText(String.valueOf(this.calculateTotalScore(projectCriteria)));
        tvClassification.setText(this.getClassificationName(
                DataProcessingManager.getProfileClassifications(profileClassifications, projectModel.getMainGuide())
                , Double.parseDouble(tvTotalScore.getText().toString())
        ));
//        projectModel.setScore(this.calculateTotalScore(projectCriteria));
//        projectModel.setRating(tvClassification.getText().toString());
    }

    //calculate total score
    public double calculateTotalScore(List<CriterionModel> projectCriteria) {
        double scoreTemp = 0;
        int totalWeight = 0;
        if (projectCriteria != null && projectCriteria.size() > 0) {
            for (CriterionModel criterionModel : projectCriteria) {
                if (criterionModel.getScore() != 0) {
                    scoreTemp += criterionModel.getWeight() * criterionModel.getScore();
                }
                totalWeight += criterionModel.getWeight();
            }
        } else {
            return 0;
        }
        return scoreTemp / totalWeight;
    }

    public void setTotalScore() {
        tvTotalScore.setText(String.valueOf(calculateTotalScore(projectCriteria)));
    }

    public void setClassification() {
        tvClassification.setText(getClassificationName(profileClassifications, Double.parseDouble(tvTotalScore.getText().toString())));
    }

    public String getClassificationName(List<ClassificationModel> profileClassifications, double totalScore) {
        String classificationName = null;
        if (profileClassifications != null && profileClassifications.size() > 0) {
            for (ClassificationModel classificationModel : profileClassifications) {
                if (classificationModel.getFromScore() <= totalScore && classificationModel.getToScore() > totalScore) {
                    classificationName = classificationModel.getName();
                }
            }
        }
        return classificationName;
    }

    @Override
    public void onBackPressed() {
        if (this.isChangedEvaluate) {
            new AlertDialog.Builder(this, R.style.DialogThemMain)
                    .setMessage("Your changes will not be saved. Continue?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        } else {
            finish();
        }
    }
}
