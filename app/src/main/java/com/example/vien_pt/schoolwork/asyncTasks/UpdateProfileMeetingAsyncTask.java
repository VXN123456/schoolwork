package com.example.vien_pt.schoolwork.asyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;

import java.io.IOException;

/**
 * Created by Vien-PT on 04-Dec-17.
 */

public class UpdateProfileMeetingAsyncTask extends AsyncTask<String, Void, String> {
    String result = null;
    Context context;
    String jsonEntityRequest = null;
    ProgressDialog progressDialog;


    public UpdateProfileMeetingAsyncTask(Context context, String jsonEntityRequest) {
        this.context = context;
        this.jsonEntityRequest = jsonEntityRequest;
        this.progressDialog = new ProgressDialog(context, R.style.DialogThemMain);
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setMessage("Processing...");
        progressDialog.show();
    }


    @Override
    protected String doInBackground(String... params) {
        try {
            result = Utils.makeRequest(Constants.POST_METHOD, params[0], null, Constants.CONTENT_TYPE_JSON, this.jsonEntityRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (result.equals("Updated")) {
            Toast.makeText(context, "Succeed", Toast.LENGTH_SHORT).show();
//            new ProfileMeetingsAsyncTask(context).execute(Constants.URL_PROFILEMEETINGS_BY_PROFILE_API + LoginActivity.accountId);
        }else{
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
        }
        progressDialog.dismiss();
    }
}
