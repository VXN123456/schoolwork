package com.example.vien_pt.schoolwork.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.activities.MainActivity;
import com.example.vien_pt.schoolwork.adapters.MeetingAdapter;
import com.example.vien_pt.schoolwork.adapters.TaskAdapter;
import com.example.vien_pt.schoolwork.asyncTasks.MeetingsProfileAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.ProfileMeetingsAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.TaskProfileAsyncTask;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.DataProcessingManager;
import com.example.vien_pt.schoolwork.models.TaskModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vien-PT on 01-Dec-17.
 */

public class CompletedTasksFragment extends Fragment {

    TaskAdapter taskAdapter;
    ListView lvCompletedTasks;
    SwipeRefreshLayout swipeRefreshTaskList;
    List<TaskModel> completedTasks;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_completed_tasks, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeRefreshTaskList = (SwipeRefreshLayout) getView().findViewById(R.id.swipeRefreshTaskList);
        lvCompletedTasks = (ListView) getView().findViewById(R.id.lvCompletedTasks);
        //get open task
        completedTasks = new ArrayList<>();
        completedTasks = DataProcessingManager.getCompletedTasks(LoginActivity.profileTasks);

        taskAdapter = new TaskAdapter(getActivity(), R.layout.item_task, completedTasks, null, false);
        lvCompletedTasks.setAdapter(taskAdapter);
        lvCompletedTasks.setScrollingCacheEnabled(false);

        swipeRefreshTaskList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshTaskList.setRefreshing(false);
                onResume();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        new TaskProfileAsyncTask(getActivity()).execute(Constants.URL_PROFILE_TASKS_API + LoginActivity.accountId);
        completedTasks = DataProcessingManager.getCompletedTasks(LoginActivity.profileTasks);

        taskAdapter = new TaskAdapter(getActivity(), R.layout.item_task, completedTasks, null, false);
        lvCompletedTasks.setAdapter(taskAdapter);

        ((MainActivity)getActivity()).setNumberNotice();
    }

}
