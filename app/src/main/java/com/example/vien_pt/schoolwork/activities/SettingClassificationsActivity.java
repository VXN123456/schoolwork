package com.example.vien_pt.schoolwork.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.adapters.ClassificationAdapter;
import com.example.vien_pt.schoolwork.asyncTasks.ClassificationAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.CreateOrUpdateClassificationAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.CreateOrUpdateCriterionAsyncTask;
import com.example.vien_pt.schoolwork.base.BaseActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.ClassificationModel;
import com.example.vien_pt.schoolwork.models.CriterionModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SettingClassificationsActivity extends BaseActivity {
    private boolean isChangedClassificationSetting = false;

    public boolean isChangedClassificationSetting() {
        return isChangedClassificationSetting;
    }

    public void setChangedClassificationSetting(boolean changedClassificationSetting) {
        isChangedClassificationSetting = changedClassificationSetting;
    }

    List<ClassificationModel> profileClassifications;
    Button btnAddNewClassification;
    ClassificationAdapter classificationAdapter;
    ListView lvClassifications;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_classifications);
        //init view
        initView();

        profileClassifications = new ArrayList<>();
        getDataTransfer();

        classificationAdapter = new ClassificationAdapter(this, R.layout.item_classification, profileClassifications);
        lvClassifications.setAdapter(classificationAdapter);
        //add new classification
        btnAddNewClassification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialogAddClassification = new Dialog(SettingClassificationsActivity.this);
                dialogAddClassification.setContentView(R.layout.dialog_edit_classification);
                TextView tvTitle = (TextView) dialogAddClassification.findViewById(R.id.tvTitle);
                final EditText edtClassificationName = (EditText) dialogAddClassification.findViewById(R.id.edtClassificationName);
                final EditText edtFromScore = (EditText) dialogAddClassification.findViewById(R.id.edtFromScore);
                final EditText edtToScore = (EditText) dialogAddClassification.findViewById(R.id.edtToScore);
                Button btnCreate = (Button) dialogAddClassification.findViewById(R.id.btnSave);

                tvTitle.setText("Add New Classification");
                btnCreate.setText("Create");

                btnCreate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (MainActivity.currentAccount.getRole().equals("Teacher")) {
                            ClassificationModel classificationModel = new ClassificationModel();
                            if (edtClassificationName.getText().toString().trim().equals("")) {
                                Toast.makeText(SettingClassificationsActivity.this, "Please enter classification name", Toast.LENGTH_SHORT).show();
                            } else {
                                try {
                                    double fromScore = Double.parseDouble(edtFromScore.getText().toString());
                                    double toScore = Double.parseDouble(edtToScore.getText().toString());
                                    if (fromScore >= toScore) {
                                        Toast.makeText(SettingClassificationsActivity.this, "FROM must less than TO", Toast.LENGTH_SHORT).show();
                                    } else {
                                        classificationModel.setId(0);
                                        classificationModel.setProfileId(LoginActivity.accountId);
                                        classificationModel.setName(edtClassificationName.getText().toString().trim());
                                        classificationModel.setFromScore(fromScore);
                                        classificationModel.setToScore(toScore);
                                        profileClassifications.add(classificationModel);
                                        setChangedClassificationSetting(true);
                                        dialogAddClassification.dismiss();
                                        classificationAdapter.notifyDataSetChanged();
                                    }
                                } catch (Exception e) {
                                    Toast.makeText(SettingClassificationsActivity.this, "Input value invalid!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else {
                            Toast.makeText(SettingClassificationsActivity.this, "This function for Teacher role!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                dialogAddClassification.show();
            }
        });
    }

    private void getDataTransfer() {
        Intent intent = getIntent();
        profileClassifications = (List<ClassificationModel>) intent.getSerializableExtra("classifications");
    }

    private void initView() {
        setToolBar("Setting Classification");
        setColorStatusBar(this);
        btnAddNewClassification = (Button) findViewById(R.id.btnAddNewClassification);
        lvClassifications = (ListView) findViewById(R.id.lvClassifications);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.classification_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.saveSettingClassifications:
                if (Utils.isConnectingToInternet(getApplicationContext())) {
                    if (profileClassifications != null && profileClassifications.size() > 0) {
                        JSONArray arrayCriteria = new JSONArray();
                        for (ClassificationModel classificationModel : profileClassifications) {
                            JSONObject objectCriterion = new JSONObject();
                            try {
                                objectCriterion.put("id", 0);
                                objectCriterion.put("profileId", classificationModel.getProfileId());
                                objectCriterion.put("name", classificationModel.getName());
                                objectCriterion.put("fromScore", classificationModel.getFromScore());
                                objectCriterion.put("toScore", classificationModel.getToScore());
                                arrayCriteria.put(objectCriterion);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        new CreateOrUpdateClassificationAsyncTask(SettingClassificationsActivity.this, arrayCriteria.toString(),profileClassifications)
                                .execute(Constants.URL_ADD_OR_UPDATE_CLASSIFICATION_API + String.valueOf(LoginActivity.accountId));
                    }
                } else {
                    Toast.makeText(this, "Can not connect to internet!", Toast.LENGTH_SHORT).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (this.isChangedClassificationSetting()) {
            new AlertDialog.Builder(this, R.style.DialogThemMain)
                    .setMessage("Your changes not be saved. Continue?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        } else {
            finish();
        }
    }
}
