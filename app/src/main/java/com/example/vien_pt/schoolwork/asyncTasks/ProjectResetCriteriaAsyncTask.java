package com.example.vien_pt.schoolwork.asyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;

import java.io.IOException;

/**
 * Created by Vien-PT on 19-Nov-17.
 */

public class ProjectResetCriteriaAsyncTask extends AsyncTask<String, Void, String> {
    Context context;
    private String result = "";
    ProgressDialog progressDialog;

    public ProjectResetCriteriaAsyncTask(Context context) {
        this.context = context;
        this.progressDialog = new ProgressDialog(context, R.style.DialogThemMain);
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            result = Utils.makeRequest(Constants.POST_METHOD, params[0], null, "", "");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result.equals("Removed")) {
            Toast.makeText(context, "Reset done", Toast.LENGTH_SHORT).show();
        } else if (result.equals("Not existed")) {
            Toast.makeText(context, "Not yet evaluated! Reset done", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Reset failed", Toast.LENGTH_SHORT).show();
        }
        progressDialog.dismiss();
    }
}
