package com.example.vien_pt.schoolwork.common;

/**
 * Created by Vien-PT on 31-Oct-17.
 */

public class Constants {
    //profile
    public static String URL_ALL_PROFILE_API = "http://192.168.43.232:8080/SchoolWork/profiles";
    public static String URL_ADD_OR_UPDATE_PROFILE_API = "http://192.168.43.232:8080/SchoolWork/profile/update";
    public static String URL_REGISTRATION_API = "http://192.168.43.232:8080/SchoolWork/registration";
    //contact
    public static String URL_GET_CONTACTS_API = "http://192.168.43.232:8080/SchoolWork/contacts/";
    public static String URL_CREATE_CONTACTS_API = "http://192.168.43.232:8080/SchoolWork/contact/add/";
    public static String URL_DELETE_CONTACT_API = "http://192.168.43.232:8080/SchoolWork/contact/remove/";
    public static String URL_UPDATE_CONTACT_API = "http://192.168.43.232:8080/SchoolWork/contact/update";
    //project
    public static String URL_GET_PROFILE_PROJECTS_API = "http://192.168.43.232:8080/SchoolWork/projects/";
    public static String URL_GET_PROJECTS_API = "http://192.168.43.232:8080/SchoolWork/projects";
    public static String URL_ADD_OR_UPDATE_PROJECT_API = "http://192.168.43.232:8080/SchoolWork/project/add-or-update";
    public static String URL_DELETE_PROJECT_API = "http://192.168.43.232:8080/SchoolWork/project/remove/";
    //task
    public static String URL_PROJECT_TASKS_API = "http://192.168.43.232:8080/SchoolWork/project/tasks/";
    public static String URL_PROFILE_TASKS_API = "http://192.168.43.232:8080/SchoolWork/profile/tasks/";
    public static String URL_ADD_OR_UPDATE_TASK_API = "http://192.168.43.232:8080/SchoolWork/project/task/add-or-update";
    public static String URL_UPDATE_TASK_STATUS_API = "http://192.168.43.232:8080/SchoolWork/project/task/";
    public static String URL_DELETE_TASK_API = "http://192.168.43.232:8080/SchoolWork/project/task/remove/";

    //meeting
    public static String URL_MEETINGS_API = "http://192.168.43.232:8080/SchoolWork/meetings";
    public static String URL_PROFILE_MEETINGS_API = "http://192.168.43.232:8080/SchoolWork/profile/meetings/";
    public static String URL_ADD_OR_UPDATE_MEETING_API = "http://192.168.43.232:8080/SchoolWork/project/meeting/add-or-update";
    public static String URL_DELETE_MEETING_API = "http://192.168.43.232:8080/SchoolWork/project/meeting/remove/";

    public static String URL_PROFILEMEETINGS_API = "http://192.168.43.232:8080/SchoolWork/profile-meetings";
    public static String URL_PROFILEMEETINGS_BY_PROFILE_API = "http://192.168.43.232:8080/SchoolWork/profile-meetings/";
    public static String URL_UDPATE_PROFILEMEETING_API = "http://192.168.43.232:8080/SchoolWork/meeting/profile-meeting/update";
    public static String URL_UPDATE_PROFILEMEETINGS_API = "http://192.168.43.232:8080/SchoolWork/meeting/profile-meetings/update";

    //criteria
    public static String URL_GET_CRITERIA_API = "http://192.168.43.232:8080/SchoolWork/criteria";
    public static String URL_ADD_OR_UPDATE_CRITERION_API = "http://192.168.43.232:8080/SchoolWork/criterion/add-or-update";
    public static String URL_PROJECT_RESET_CRITERIA_API = "http://192.168.43.232:8080/SchoolWork/project/criteria/reset/";

    //classification
    public static String URL_GET_CLASSIFICATIONS_API = "http://192.168.43.232:8080/SchoolWork/classifications";
    public static String URL_ADD_OR_UPDATE_CLASSIFICATION_API = "http://192.168.43.232:8080/SchoolWork/classifications/add-or-update/";
    public static String URL_DELETE_CLASSIFICATION_API = "http://192.168.43.232:8080/SchoolWork/classifications/remove/";

    //notification
    public static String URL_GET_PROFILE_NOTIFICATIONS_API = "http://192.168.43.232:8080/SchoolWork/notifications/";
    public static String URL_UPDATE_PROFILE_NOTIFICATIONS_API = "http://192.168.43.232:8080/SchoolWork/notifications/update";

//    //using data mobile
//    public static String URL_ADD_OR_UPDATE_MEETING_API = "http://192.43.232:8080/SchoolWork/project/meetings/add-or-update";


    public static String GET_METHOD = "GET";
    public static String POST_METHOD = "POST";

    public static String CONTENT_TYPE_JSON = "application/json";
    public static String EMPTY = "";
    public static String NULL_STRING = "null";
    //activity life circle
    public static String ON_CREATE_ACTIVITY = "onCreate";
    public static String ON_START_ACTIVITY = "onStart";
    public static String ON_RESUME_ACTIVITY = "onResume";
    public static String ON_PAUSE_ACTIVITY = "onPause";
    public static String ON_STOP_ACTIVITY = "onStop";
    public static String ON_RESTART_ACTIVITY = "onRestart";
    public static String ON_DESTROY_ACTIVITY = "onDestroy";


    //fragment life circle
    public static String ON_ATTACH_FRAGMENT = "onAttach";
    public static String ON_CREATE_FRAGMENT = "onCreate";
    public static String ON_CREATE_VIEW_FRAGMENT = "onCreateView";
    public static String ON_VIEW_CREATED_FRAGMENT = "onViewCreated";
    public static String ON_ACTIVITY_CREATED_FRAGMENT = "onActivityCreated";
    public static String ON_START_FRAGMENT = "onStart";
    public static String ON_RESUME_FRAGMENT = "onResume";
    public static String ON_PAUSE_FRAGMENT = "onPause";
    public static String ON_STOP_FRAGMENT = "onStop";
    public static String ON_DESTROY_VIEW_FRAGMENT = "onDestroyView";
    public static String ON_DESTROY_FRAGMENT = "onDestroy";
    public static String ON_DETACH_FRAGMENT = "onDetach";


}