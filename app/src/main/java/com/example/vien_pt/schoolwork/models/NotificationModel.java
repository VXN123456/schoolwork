package com.example.vien_pt.schoolwork.models;

import java.sql.Timestamp;

/**
 * Created by Vien-PT on 03-Dec-17.
 */

public class NotificationModel {
    private int id;
    private int noticeFrom;
    private int noticeTo;
    private int projectId;
    private int meetingId;
    private int taskId;
    private String content;
    private long createdAt;
    private int flagRead;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNoticeFrom() {
        return noticeFrom;
    }

    public void setNoticeFrom(int noticeFrom) {
        this.noticeFrom = noticeFrom;
    }

    public int getNoticeTo() {
        return noticeTo;
    }

    public void setNoticeTo(int noticeTo) {
        this.noticeTo = noticeTo;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(int meetingId) {
        this.meetingId = meetingId;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public int getFlagRead() {
        return flagRead;
    }

    public void setFlagRead(int flagRead) {
        this.flagRead = flagRead;
    }
}
