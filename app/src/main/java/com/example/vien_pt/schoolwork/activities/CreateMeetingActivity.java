package com.example.vien_pt.schoolwork.activities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.adapters.SpinnerProjectAdapter;
import com.example.vien_pt.schoolwork.asyncTasks.CreateOrUpdateMeetingAsyncTask;
import com.example.vien_pt.schoolwork.base.BaseActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.ProjectModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CreateMeetingActivity extends BaseActivity {
    AutoCompleteTextView inputMeetingName;
    EditText edtMeetingContent;
    Spinner spnProject;
    TextView tvMeetingDate;
    TextView tvMeetingTime;
    EditText edtLocation;
    Button btnSaveMeeting;
    DatePickerDialog datePickerDialog;
    TimePickerDialog timePickerDialog;
    SpinnerProjectAdapter spinnerProjectAdapter;
    List<ProjectModel> projectModels;
    ProjectModel projectModelSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_meeting);
        //init view
        initView();

        projectModels = LoginActivity.profileProjects;

        spinnerProjectAdapter = new SpinnerProjectAdapter(CreateMeetingActivity.this, R.layout.item_project, projectModels);
        spnProject.setAdapter(spinnerProjectAdapter);
        spnProject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                projectModelSelected = (ProjectModel) parent.getItemAtPosition(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // perform click event on edit text
        tvMeetingDate.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR); // current year
                int month = c.get(Calendar.MONTH); // current month
                int day = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(CreateMeetingActivity.this,android.R.style.Theme_DeviceDefault_Light_Dialog,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                tvMeetingDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, year, month, day);
                datePickerDialog.show();
            }
        });
        tvMeetingTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get Current Time
                final Calendar c = Calendar.getInstance();
                int hour = c.get(Calendar.HOUR_OF_DAY);
                int minute = c.get(Calendar.MINUTE);

                // Launch Time Picker Dialog
                timePickerDialog = new TimePickerDialog(CreateMeetingActivity.this,android.R.style.Theme_DeviceDefault_Light_Dialog,
                        new TimePickerDialog.OnTimeSetListener() {
                            @SuppressLint("DefaultLocale")
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                tvMeetingTime.setText(String.format("%02d:%02d",hourOfDay, minute));
                            }
                        }, hour, minute, false);
                timePickerDialog.show();
            }
        });
        //save meeting
        btnSaveMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isConnectingToInternet(getApplicationContext())){
                    AlertDialog.Builder builder = new AlertDialog.Builder(CreateMeetingActivity.this,R.style.DialogThemMain);
                    builder.setMessage("No internet connection, please try again!")
                            .setPositiveButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            }).show();
                }else if (inputMeetingName.getText().toString().equals(Constants.EMPTY)) {
                    Toast.makeText(getApplicationContext(), "Please enter meeting name!", Toast.LENGTH_SHORT).show();
                } else {
                    JSONObject meetingObject = new JSONObject();

                    try {
                        meetingObject.put("id", 0);
                        meetingObject.put("name", inputMeetingName.getText().toString().trim());
                        meetingObject.put("time", tvMeetingTime.getText().toString().trim());
                        meetingObject.put("date", tvMeetingDate.getText().toString());
                        meetingObject.put("location", edtLocation.getText().toString());
                        meetingObject.put("content", edtMeetingContent.getText().toString());
                        meetingObject.put("projectId", projectModelSelected.getId());
                        meetingObject.put("createdBy", MainActivity.currentAccount.getId());
                        meetingObject.put("createdAt", Utils.getCurrentTimeStamp());
                        meetingObject.put("updatedAt", Utils.getCurrentTimeStamp());
                        meetingObject.put("participants", 1);
                        meetingObject.put("members", projectModelSelected.getMembers().length);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    CreateOrUpdateMeetingAsyncTask createOrUpdateMeetingAsyncTask = new CreateOrUpdateMeetingAsyncTask(CreateMeetingActivity.this, meetingObject.toString());
                    createOrUpdateMeetingAsyncTask.execute(Constants.URL_ADD_OR_UPDATE_MEETING_API);
                }
            }
        });

    }

    public void initView() {
// Set a toolbar to replace the action bar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Create an appointment");
        
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //set color status bar
        setColorStatusBar(this);
        inputMeetingName = (AutoCompleteTextView) findViewById(R.id.edtMeetingName);
        edtMeetingContent = (EditText) findViewById(R.id.edtMeetingContent);
        spnProject = (Spinner) findViewById(R.id.spnProjectName);
        tvMeetingDate = (TextView) findViewById(R.id.tvMeetingDate);
        tvMeetingTime = (TextView) findViewById(R.id.tvMeetingTime);
        edtLocation = (EditText) findViewById(R.id.edtLocation);
        btnSaveMeeting = (Button) findViewById(R.id.btnSaveMeeting);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
