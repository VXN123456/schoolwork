package com.example.vien_pt.schoolwork.models;

import java.io.Serializable;

/**
 * Created by Vien-PT on 04-Dec-17.
 */

public class ProfileMeetingModel implements Serializable {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

    public int getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(int meetingId) {
        this.meetingId = meetingId;
    }

    public int getFlagRead() {
        return flagRead;
    }

    public void setFlagRead(int flagRead) {
        this.flagRead = flagRead;
    }

    public int getFlagJoin() {
        return flagJoin;
    }

    public void setFlagJoin(int flagJoin) {
        this.flagJoin = flagJoin;
    }

    private int id;
    private int profileId;
    private int meetingId;
    private int flagRead;
    private int flagJoin;
}
