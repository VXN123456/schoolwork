package com.example.vien_pt.schoolwork.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.asyncTasks.ClassificationAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.MeetingsAsyncTask;
import com.example.vien_pt.schoolwork.base.BaseActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.DataProcessingManager;
import com.example.vien_pt.schoolwork.fragments.AccountFragment;
import com.example.vien_pt.schoolwork.fragments.ContactsFragment;
import com.example.vien_pt.schoolwork.fragments.MeetingsFragment;
import com.example.vien_pt.schoolwork.fragments.ProjectsFragment;
import com.example.vien_pt.schoolwork.fragments.TasksFragment;
import com.example.vien_pt.schoolwork.models.ClassificationModel;
import com.example.vien_pt.schoolwork.models.NotificationModel;
import com.example.vien_pt.schoolwork.models.ProfileModel;

import org.w3c.dom.Text;

import java.util.List;

import static java.security.AccessController.getContext;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    public static List<ClassificationModel> classifications;
    public static List<NotificationModel> profileNotifications;

    public static boolean isContactToAdd = false;
    public static int positionCurrentTab = 1;
    public static ProfileModel currentAccount;
    public static List<ProfileModel> contactList;

    LinearLayout lnlProject;
    LinearLayout lnlMeeting;
    LinearLayout lnlTask;
    LinearLayout lnlContact;
    LinearLayout lnlAccount;
    FrameLayout frameLayoutMain;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    TextView tvTabTitleProject;
    TextView tvTabTitleMeeting;
    TextView tvTabTitleTask;
    TextView tvTabTitleContact;
    TextView tvTabTitleAccount;
    LinearLayout lnlNumberNoticeProject;
    TextView tvNumberNoticeProject;

    LinearLayout lnlNumberNoticeMeeting;
    TextView tvNumberNoticeMeeting;

    LinearLayout lnlNumberNoticeTask;
    TextView tvNumberNoticeTask;
    ImageView imvProject;
    ImageView imvMeeting;
    ImageView imvTask;
    ImageView imvContact;
    ImageView imvAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //set color status bar
        setColorStatusBar(this);

        //set project name,from,to for task
        DataProcessingManager.setRelationFieldsForTask(LoginActivity.profileTasks, LoginActivity.profileProjects, LoginActivity.profiles);
        //init view
        initView();
        setView();
        lnlProject.setOnClickListener(this);
        lnlMeeting.setOnClickListener(this);
        lnlTask.setOnClickListener(this);
        lnlContact.setOnClickListener(this);
        lnlAccount.setOnClickListener(this);
        //load classification
        new ClassificationAsyncTask(this).execute(Constants.URL_GET_CLASSIFICATIONS_API);
    }

    private void setView() {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.frameLayoutMain, new ProjectsFragment(), "project_fragment");
        positionCurrentTab = 1;
        tvTabTitleProject.setTextColor(this.getResources().getColor(R.color.colorAllWhite));
        imvProject.setColorFilter(ContextCompat.getColor(this, R.color.colorAllWhite));
        fragmentTransaction.commit();
        setNumberNotice();
    }

    public void setNumberNotice() {
        if (DataProcessingManager.getNextMeetings(LoginActivity.profileMeetings).size() > 0) {
            lnlNumberNoticeMeeting.setVisibility(View.VISIBLE);
            tvNumberNoticeMeeting.setText(String.valueOf(DataProcessingManager.getNextMeetings(LoginActivity.profileMeetings).size()));
        } else {
            lnlNumberNoticeMeeting.setVisibility(View.GONE);
        }
        //
        if (DataProcessingManager.getOpenTaks(LoginActivity.profileTasks).size() > 0) {
            lnlNumberNoticeTask.setVisibility(View.VISIBLE);
            tvNumberNoticeTask.setText(String.valueOf(DataProcessingManager.getOpenTaks(LoginActivity.profileTasks).size()));
        } else {
            lnlNumberNoticeTask.setVisibility(View.GONE);
        }
    }

    private void initView() {

        lnlProject = (LinearLayout) findViewById(R.id.lnlProject);
        lnlMeeting = (LinearLayout) findViewById(R.id.lnlMeeting);
        lnlTask = (LinearLayout) findViewById(R.id.lnlTask);
        lnlContact = (LinearLayout) findViewById(R.id.lnlContact);
        lnlAccount = (LinearLayout) findViewById(R.id.lnlAccount);
        frameLayoutMain = (FrameLayout) findViewById(R.id.frameLayoutMain);
        tvTabTitleProject = (TextView) findViewById(R.id.tvTabTitleProject);
        tvTabTitleMeeting = (TextView) findViewById(R.id.tvTabTitleMeeting);
        tvTabTitleTask = (TextView) findViewById(R.id.tvTabTitleTask);
        tvTabTitleContact = (TextView) findViewById(R.id.tvTabTitleContact);
        tvTabTitleAccount = (TextView) findViewById(R.id.tvTabTitleAccount);
        lnlNumberNoticeProject = (LinearLayout) findViewById(R.id.lnlNumberNoticeProject);
        tvNumberNoticeProject = (TextView) findViewById(R.id.tvNumberNoticeProject);
        lnlNumberNoticeMeeting = (LinearLayout) findViewById(R.id.lnlNumberNoticeMeeting);
        tvNumberNoticeMeeting = (TextView) findViewById(R.id.tvNumberNoticeMeeting);
        lnlNumberNoticeTask = (LinearLayout) findViewById(R.id.lnlNumberNoticeTask);
        tvNumberNoticeTask = (TextView) findViewById(R.id.tvNumberNoticeTask);
        imvProject = (ImageView) findViewById(R.id.imvProject);
        imvMeeting = (ImageView) findViewById(R.id.imvMeeting);
        imvTask = (ImageView) findViewById(R.id.imvTask);
        imvContact = (ImageView) findViewById(R.id.imvContact);
        imvAccount = (ImageView) findViewById(R.id.imvAccount);

    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this, R.style.DialogThemMain)
                .setMessage("Do you want to close app?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        moveTaskToBack(true);
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //load danh sách các cuộc hẹn
        new MeetingsAsyncTask(this).execute(Constants.URL_MEETINGS_API);

        //set thong bao moi
        if (getNumberUnreadNotifications(MainActivity.profileNotifications) == 0) {
            lnlNumberNoticeProject.setVisibility(View.GONE);
        } else {
            lnlNumberNoticeProject.setVisibility(View.VISIBLE);
            tvNumberNoticeProject.setText(String.valueOf(getNumberUnreadNotifications(MainActivity.profileNotifications)));
        }
//        //set thong bao moi
//        if (getNumberUnreadNotifications(MainActivity.profileNotifications) == 0) {
//            lnlNumberNoticeProject.setVisibility(View.GONE);
//        } else {
//            lnlNumberNoticeProject.setVisibility(View.VISIBLE);
//            tvNumberNoticeProject.setText(String.valueOf(getNumberUnreadNotifications(MainActivity.profileNotifications)));
//        }
//        //set thong bao moi
//        if (getNumberUnreadNotifications(MainActivity.profileNotifications) == 0) {
//            lnlNumberNoticeProject.setVisibility(View.GONE);
//        } else {
//            lnlNumberNoticeProject.setVisibility(View.VISIBLE);
//            tvNumberNoticeProject.setText(String.valueOf(getNumberUnreadNotifications(MainActivity.profileNotifications)));
//        }
    }

    @Override
    public void onClick(View v) {
        tvTabTitleProject.setTextColor(this.getResources().getColor(R.color.colorTextTabNotActive));
        imvProject.setColorFilter(ContextCompat.getColor(this, R.color.colorTextTabNotActive));
        tvTabTitleMeeting.setTextColor(this.getResources().getColor(R.color.colorTextTabNotActive));
        imvMeeting.setColorFilter(ContextCompat.getColor(this, R.color.colorTextTabNotActive));
        tvTabTitleTask.setTextColor(this.getResources().getColor(R.color.colorTextTabNotActive));
        imvTask.setColorFilter(ContextCompat.getColor(this, R.color.colorTextTabNotActive));
        tvTabTitleContact.setTextColor(this.getResources().getColor(R.color.colorTextTabNotActive));
        imvContact.setColorFilter(ContextCompat.getColor(this, R.color.colorTextTabNotActive));
        tvTabTitleAccount.setTextColor(this.getResources().getColor(R.color.colorTextTabNotActive));
        imvAccount.setColorFilter(ContextCompat.getColor(this, R.color.colorTextTabNotActive));
        switch (v.getId()) {
            case R.id.lnlProject:
                tvTabTitleProject.setTextColor(this.getResources().getColor(R.color.colorAllWhite));
                imvProject.setColorFilter(ContextCompat.getColor(this, R.color.colorAllWhite));
                if (positionCurrentTab != 1) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
                            fragmentTransaction.replace(R.id.frameLayoutMain, new ProjectsFragment(), "project_fragment");
                            positionCurrentTab = 1;
                            fragmentTransaction.commit();
                        }
                    }, 500);
                }
                break;
            case R.id.lnlMeeting:
                tvTabTitleMeeting.setTextColor(this.getResources().getColor(R.color.colorAllWhite));
                imvMeeting.setColorFilter(ContextCompat.getColor(this, R.color.colorAllWhite));
                if (positionCurrentTab != 2) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            fragmentTransaction = fragmentManager.beginTransaction();
                            if (positionCurrentTab == 1) {
                                fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                            } else {
                                fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
                            }
                            fragmentTransaction.replace(R.id.frameLayoutMain, new MeetingsFragment(), "meeting_fragment");
                            positionCurrentTab = 2;
                            fragmentTransaction.commit();
                        }
                    }, 500);
                }
                break;
            case R.id.lnlTask:
                tvTabTitleTask.setTextColor(this.getResources().getColor(R.color.colorAllWhite));
                imvTask.setColorFilter(ContextCompat.getColor(this, R.color.colorAllWhite));
                if (positionCurrentTab != 3) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            fragmentTransaction = fragmentManager.beginTransaction();
                            if (positionCurrentTab == 1 || positionCurrentTab == 2) {
                                fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                            } else {
                                fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
                            }
                            fragmentTransaction.replace(R.id.frameLayoutMain, new TasksFragment(), "task_fragment");
                            positionCurrentTab = 3;
                            fragmentTransaction.commit();
                        }
                    }, 500);

                }
                break;
            case R.id.lnlContact:
                tvTabTitleContact.setTextColor(this.getResources().getColor(R.color.colorAllWhite));
                imvContact.setColorFilter(ContextCompat.getColor(this, R.color.colorAllWhite));
                if (positionCurrentTab != 4) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            fragmentTransaction = fragmentManager.beginTransaction();
                            if (positionCurrentTab == 5) {
                                fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
                            } else {
                                fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                            }
                            fragmentTransaction.replace(R.id.frameLayoutMain, new ContactsFragment(), "contact_fragment");
                            positionCurrentTab = 4;
                            fragmentTransaction.commit();
                        }
                    }, 500);
                }
                break;
            case R.id.lnlAccount:
                tvTabTitleAccount.setTextColor(this.getResources().getColor(R.color.colorAllWhite));
                imvAccount.setColorFilter(ContextCompat.getColor(this, R.color.colorAllWhite));
                if (positionCurrentTab != 5) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                            fragmentTransaction.replace(R.id.frameLayoutMain, new AccountFragment(), "account_fragment");
                            positionCurrentTab = 5;
                            fragmentTransaction.commit();
                        }
                    }, 500);
                }
                break;
        }

    }

    // get number of notifications unread
    public int getNumberUnreadNotifications(List<NotificationModel> unreadNotifications) {
        int numberUnreadNotice = 0;
        if (unreadNotifications != null && unreadNotifications.size() > 0) {
            for (NotificationModel notificationModel : unreadNotifications) {
                if (notificationModel.getFlagRead() == 0) {
                    numberUnreadNotice++;
                }
            }
        }
        return numberUnreadNotice;
    }

}
