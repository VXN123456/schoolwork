package com.example.vien_pt.schoolwork.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.EditMeetingActivity;
import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.activities.MainActivity;
import com.example.vien_pt.schoolwork.asyncTasks.DeleteMeetingAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.UpdateProfileMeetingAsyncTask;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.DataProcessingManager;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.MeetingModel;
import com.example.vien_pt.schoolwork.models.ProfileMeetingModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Vien-PT on 12-Nov-17.
 */

public class MeetingAdapter extends ArrayAdapter<MeetingModel> {
     Context context;
    List<MeetingModel> meetingModels;

    public MeetingAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<MeetingModel> objects) {
        super(context, resource, objects);
        this.context = context;
        this.meetingModels = objects;
    }

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public View getView(final int position, @Nullable final View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        final boolean[] isShownFullMeetingContent = new boolean[meetingModels.size()];
        final MeetingModel meetingModel = getItem(position);
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_meeting, null);
        }
        TextView tvCreator = (TextView) view.findViewById(R.id.tvCreator);
        final TextView tvProjectName = (TextView) view.findViewById(R.id.tvProjectName);
        TextView tvMeetingName = (TextView) view.findViewById(R.id.tvMeetingName);
        final TextView tvMeetingContent = (TextView) view.findViewById(R.id.tvMeetingContent);
        TextView tvLocation = (TextView) view.findViewById(R.id.tvLocation);
        TextView tvMeetingTime = (TextView) view.findViewById(R.id.tvMeetingTime);
        TextView tvMeetingDate = (TextView) view.findViewById(R.id.tvMeetingDate);
        ImageView imvEditMeeting = (ImageView) view.findViewById(R.id.imvEditMeeting);
        ImageView imvDeleteMeeting = (ImageView) view.findViewById(R.id.imvDeleteMeeting);
        final Button btnFlagJoinMeeting = (Button) view.findViewById(R.id.btnFlagJoinMeeting);
        final TextView tvNumberJoined = (TextView) view.findViewById(R.id.tvNumberJoined);
        final TextView tvNumberUnJoined = (TextView) view.findViewById(R.id.tvNumberUnJoined);
        LinearLayout lnlMeetingItem = (LinearLayout) view.findViewById(R.id.lnlMeetingItem);

        final Button btnShowHideMeetingContent = (Button) view.findViewById(R.id.btnShowHideMeetingContent);

//        setView(meetingModel);
        if (meetingModel != null) {
            if (!meetingModel.getName().equals(Constants.NULL_STRING)) {
                tvMeetingName.setText(meetingModel.getName());
            }

            if (!meetingModel.getContent().equals(Constants.NULL_STRING)) {
                tvMeetingContent.setText(meetingModel.getContent());
//                tvMeetingContent.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
//                    @Override
//                    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
//                        tvMeetingContent.removeOnLayoutChangeListener(this);
//                        int linesCountMeetingContent = tvMeetingContent.getLayout().getLineCount();
//                        if (linesCountMeetingContent > 2) {
//                            tvMeetingContent.setEllipsize(TextUtils.TruncateAt.END);
//                            tvMeetingContent.setLines(2);
//                            btnShowHideMeetingContent.setVisibility(View.VISIBLE);
//                            btnShowHideMeetingContent.setText("See more...");
//                            isShownFullMeetingContent[position] = false;
//                        } else {
//                            btnShowHideMeetingContent.setVisibility(View.GONE);
//                        }
//                    }
//                });
            }

            tvCreator.setText(meetingModel.getCreator());
            tvLocation.setText(meetingModel.getLocation());
            tvProjectName.setText(meetingModel.getProjectName());
            tvMeetingTime.setText(meetingModel.getTime());
            tvMeetingDate.setText(meetingModel.getDate());


            //set name for button join meeting
            if (DataProcessingManager.getProfileProfileMeetingsByMeetingId(LoginActivity.profileProfileMeetings, meetingModel.getId()).getFlagJoin() == 0) {
                btnFlagJoinMeeting.setText("Join");
            } else {
                btnFlagJoinMeeting.setText("UnJoin");
            }
            //kiểm tra để nếu cuộc hẹn đã qua sẽ disable button join
            if (Utils.isPassCurrentTime(Utils.convertTimeStampToLongMillisecond(Utils.getCurrentTimeStamp())
                    , Utils.convertStringDateToLongMillisecond(meetingModel.getDate())
                            + Utils.convertStringTimeToLongMillisecond(meetingModel.getTime() + ":00"))) {
                btnFlagJoinMeeting.setEnabled(false);
                btnFlagJoinMeeting.setBackground(context.getResources().getDrawable(R.drawable.bg_main_border_radius_2));
                lnlMeetingItem.setBackgroundResource(R.drawable.bg_read);
            } else {
                btnFlagJoinMeeting.setEnabled(true);
                btnFlagJoinMeeting.setBackground(context.getResources().getDrawable(R.drawable.bg_blue_border_radius_2));
                lnlMeetingItem.setBackgroundResource(R.drawable.bg_unread);
            }
            tvNumberJoined.setText(String.valueOf(meetingModel.getParticipants()));
            tvNumberUnJoined.setText(String.valueOf(meetingModel.getMembers() - meetingModel.getParticipants()));
        }

        // click edit meeting
        imvEditMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (meetingModel != null && (meetingModel.getCreatedBy() == MainActivity.currentAccount.getId()
                        || MainActivity.currentAccount.getRole().equals("Teacher"))) {
                    Intent intentEditMeeting = new Intent(context, EditMeetingActivity.class);
                    intentEditMeeting.putExtra("meeting", meetingModel);
                    context.startActivity(intentEditMeeting);
                } else {
                    Toast.makeText(context, "Permission Denied", Toast.LENGTH_SHORT).show();
                }
            }
        });
        //click delete meeting
        imvDeleteMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new android.support.v7.app.AlertDialog.Builder(context, R.style.DialogThemMain)
                        .setMessage("Do you really want to delete this meeting?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (meetingModel != null && (meetingModel.getCreatedBy() == MainActivity.currentAccount.getId()
                                        || MainActivity.currentAccount.getRole().equals("Teacher"))) {
                                    DeleteMeetingAsyncTask deleteMeetingAsyncTask = new DeleteMeetingAsyncTask(context);
                                    deleteMeetingAsyncTask.execute(Constants.URL_DELETE_MEETING_API + String.valueOf(meetingModel.getId()));
                                    remove(meetingModel);
                                    notifyDataSetChanged();

                                } else {
                                    Toast.makeText(context, "Permission Denied", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        btnFlagJoinMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isConnectingToInternet(context)) {
                    ProfileMeetingModel profileMeetingModel = null;
                    if (meetingModel != null) {
                        profileMeetingModel = DataProcessingManager.getProfileProfileMeetingsByMeetingId(LoginActivity.profileProfileMeetings, meetingModel.getId());
                    }

                    JSONObject objectProfileMeeting = new JSONObject();
                    if (DataProcessingManager.getProfileProfileMeetingsByMeetingId(
                            LoginActivity.profileProfileMeetings, meetingModel.getId()).getFlagJoin() == 0) {
                        //trường hợp chưa join sẽ tăng số người tham gia nếu ấn join
                        meetingModel.setParticipants(meetingModel.getParticipants() + 1);
                        tvNumberJoined.setText(String.valueOf(meetingModel.getParticipants()));
                        tvNumberUnJoined.setText(String.valueOf(meetingModel.getMembers() - meetingModel.getParticipants()));
                        btnFlagJoinMeeting.setText("UnJoin");

                        try {
                            objectProfileMeeting.put("id", profileMeetingModel.getId());
                            objectProfileMeeting.put("profileId", profileMeetingModel.getProfileId());
                            objectProfileMeeting.put("meetingId", profileMeetingModel.getMeetingId());
                            objectProfileMeeting.put("flagRead", profileMeetingModel.getFlagRead());
                            objectProfileMeeting.put("flagJoin", 1);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        //trường hợp tham gia rồi sẽ trừ số người tham gia nếu ấn un join
                        meetingModel.setParticipants(meetingModel.getParticipants() - 1);
                        tvNumberJoined.setText(String.valueOf(meetingModel.getParticipants()));
                        tvNumberUnJoined.setText(String.valueOf(meetingModel.getMembers() - meetingModel.getParticipants()));
                        btnFlagJoinMeeting.setText("Join");

                        try {
                            objectProfileMeeting.put("id", profileMeetingModel.getId());
                            objectProfileMeeting.put("profileId", profileMeetingModel.getProfileId());
                            objectProfileMeeting.put("meetingId", profileMeetingModel.getMeetingId());
                            objectProfileMeeting.put("flagRead", profileMeetingModel.getFlagRead());
                            objectProfileMeeting.put("flagJoin", 0);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    //thực hiện update trạng thái join
                    new UpdateProfileMeetingAsyncTask(context, objectProfileMeeting.toString()).execute(Constants.URL_UDPATE_PROFILEMEETING_API);
                } else {
                    Toast.makeText(context, "You are currently offline!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        //show hide meeting content
//        btnShowHideMeetingContent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!isShownFullMeetingContent[position]) {
//                    tvMeetingContent.setEllipsize(null);
//                    tvMeetingContent.setMaxLines(Integer.MAX_VALUE);
//                    btnShowHideMeetingContent.setText("Collapse");
//                    isShownFullMeetingContent[position] = true;
//                } else {
//                    tvMeetingContent.setEllipsize(TextUtils.TruncateAt.END);
//                    tvMeetingContent.setLines(2);
//                    btnShowHideMeetingContent.setText("See more...");
//                    isShownFullMeetingContent[position] = false;
//                }
//            }
//        });


        return view;
    }

//    private void setView(MeetingModel meetingModel) {
//
//        if (meetingModel != null) {
//            if (!meetingModel.getName().equals(Constants.NULL_STRING)) {
//                tvMeetingName.setText(meetingModel.getName());
//            }
//
//            if (!meetingModel.getContent().equals(Constants.NULL_STRING)) {
//                tvMeetingContent.setText(meetingModel.getContent());
//                tvMeetingContent.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
//                    @Override
//                    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
//                        tvMeetingContent.removeOnLayoutChangeListener(this);
//                        int linesCountMeetingContent = tvMeetingContent.getLayout().getLineCount();
//                        if (linesCountMeetingContent > 2) {
//                            tvMeetingContent.setEllipsize(TextUtils.TruncateAt.END);
//                            tvMeetingContent.setLines(2);
//                            btnShowHideMeetingContent.setVisibility(View.VISIBLE);
//                            btnShowHideMeetingContent.setText("See more...");
//                            isShownFullMeetingContent = false;
//                        } else {
//                            btnShowHideMeetingContent.setVisibility(View.GONE);
//                        }
//                    }
//                });
//            }
//
//            tvCreator.setText(meetingModel.getCreator());
//            tvLocation.setText(meetingModel.getLocation());
//            tvProjectName.setText(meetingModel.getProjectName());
//            tvMeetingTime.setText(meetingModel.getTime());
//            tvMeetingDate.setText(meetingModel.getDate());
//
//
//            //set name for button join meeting
//            if (DataProcessingManager.getProfileProfileMeetingsByMeetingId(LoginActivity.profileProfileMeetings, meetingModel.getId()).getFlagJoin() == 0) {
//                btnFlagJoinMeeting.setText("Join");
//            } else {
//                btnFlagJoinMeeting.setText("UnJoin");
//            }
//            //kiểm tra để nếu cuộc hẹn đã qua sẽ disable button join
//            if (Utils.isPassCurrentTime(Utils.convertTimeStampToLongMillisecond(Utils.getCurrentTimeStamp())
//                    , Utils.convertStringDateToLongMillisecond(meetingModel.getDate())
//                            + Utils.convertStringTimeToLongMillisecond(meetingModel.getTime() + ":00"))) {
//                btnFlagJoinMeeting.setEnabled(false);
//                btnFlagJoinMeeting.setBackground(context.getResources().getDrawable(R.drawable.bg_main_border_radius_2));
//                lnlMeetingItem.setBackgroundResource(R.drawable.bg_read);
//            } else {
//                btnFlagJoinMeeting.setEnabled(true);
//                btnFlagJoinMeeting.setBackground(context.getResources().getDrawable(R.drawable.bg_blue_border_radius_2));
//                lnlMeetingItem.setBackgroundResource(R.drawable.bg_unread);
//            }
//            tvNumberJoined.setText(String.valueOf(meetingModel.getParticipants()));
//            tvNumberUnJoined.setText(String.valueOf(meetingModel.getMembers() - meetingModel.getParticipants()));
//        }
//    }
}
