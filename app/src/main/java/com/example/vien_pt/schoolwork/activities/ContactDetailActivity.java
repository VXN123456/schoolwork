package com.example.vien_pt.schoolwork.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.base.BaseActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.models.ProfileModel;

public class ContactDetailActivity extends BaseActivity {
    TextView tvProfileName;
    TextView tvEmail;
    TextView tvPhoneNumber;
    TextView tvCodeId;
    ProfileModel profileModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);

        setColorStatusBar(this);
        setToolBar("Contact detail");
        //initView
        initView();
        setView();
    }

    private void setView() {
        Intent intent = getIntent();
        profileModel = (ProfileModel) intent.getSerializableExtra("profile");
        if (profileModel != null) {
            if (!profileModel.getFullName().equals(Constants.NULL_STRING)) {
                tvProfileName.setText(profileModel.getFullName());
            }
            if (!profileModel.getPhoneNumber().equals(Constants.NULL_STRING)) {
                tvPhoneNumber.setText(profileModel.getPhoneNumber());
            }
            if (!profileModel.getEmail().equals(Constants.NULL_STRING)) {
                tvEmail.setText(profileModel.getEmail());
            }
            if (!profileModel.getCodeId().equals(Constants.NULL_STRING)) {
                tvCodeId.setText(profileModel.getCodeId());
            }
        }
    }

    private void initView() {
        tvProfileName = (TextView) findViewById(R.id.tvProfileName);
        tvEmail = (TextView) findViewById(R.id.tvEmail);
        tvPhoneNumber = (TextView) findViewById(R.id.tvPhoneNumber);
        tvCodeId = (TextView) findViewById(R.id.tvCodeId);
    }
}
