package com.example.vien_pt.schoolwork.asyncTasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.activities.MainActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.helpers.DBHelper;
import com.example.vien_pt.schoolwork.models.ProfileModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vien-PT on 28-Oct-17.
 */

public class LoginAsyncTask extends AsyncTask<String, String, List<ProfileModel>> {

    private String email;
    private String password;
    private boolean isError;
    private String rememberMe;

    private Activity loginActivity;
    private ProgressDialog progressDialog;

    public LoginAsyncTask(Activity context) {
        loginActivity = context;
        progressDialog = new ProgressDialog(context, R.style.DialogThemMain);
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setMessage("Loading, please wait...");
        progressDialog.show();
    }

    @Override
    protected List<ProfileModel> doInBackground(String... params) {
        this.email = params[1];
        this.password = params[2];
        this.rememberMe = params[3];

        String jsonFinal = null;
        try {
            jsonFinal = Utils.makeRequest(Constants.GET_METHOD, params[0], null, Constants.CONTENT_TYPE_JSON, Constants.EMPTY);
        } catch (IOException e) {
            e.printStackTrace();
            return  null;
        }
        JSONArray jsonArrayParent = null;
        try {
            jsonArrayParent = new JSONArray(jsonFinal);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        List<ProfileModel> profileModelList = new ArrayList<>();
        for (int i = 0; i < jsonArrayParent.length(); i++) {
            JSONObject jsonObject = null;
            try {
                jsonObject = jsonArrayParent.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                ProfileModel profileModel = new ProfileModel();
                profileModel.setId(jsonObject.getInt("id"));
                profileModel.setEmail(jsonObject.getString("email"));
                profileModel.setPassword(jsonObject.getString("password"));
                profileModel.setFullName(jsonObject.getString("fullName"));
                profileModel.setCodeId(jsonObject.getString("codeId"));
                profileModel.setPhoneNumber(jsonObject.getString("phoneNumber"));
                profileModel.setStudyStart(jsonObject.getString("studyStart"));
                profileModel.setStudyEnd(jsonObject.getString("studyEnd"));
                profileModel.setPicture(jsonObject.getString("picture"));
                profileModel.setRole(jsonObject.getString("role"));
                profileModel.setCurrentSemester(jsonObject.getInt("currentSemester"));

                profileModelList.add(profileModel);
            } catch (JSONException ignored) {
            }
        }
        return profileModelList;
    }

    @Override
    protected void onPostExecute(List<ProfileModel> profileModels) {
        super.onPostExecute(profileModels);
        boolean isValidUser = false;
        if (profileModels!=null && profileModels.size()>0){
            LoginActivity.profiles = profileModels;
        }
        if (profileModels == null){
            Toast.makeText(loginActivity, "Error request to server!", Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();
        }else if (profileModels.size() > 0) {
            for (ProfileModel profileModel : profileModels) {
                if (email.equals(profileModel.getEmail()) && password.equals(profileModel.getPassword())) {
                    MainActivity.currentAccount = profileModel;
                    //Save login state
                    if (rememberMe.equals("true")) {
                        DBHelper db = new DBHelper(loginActivity.getApplicationContext());
                        db.saveState(profileModel.getEmail(), profileModel.getPassword(), profileModel.getRole());
                    }
                    int id = profileModel.getId();
                    LoginActivity.accountId = id;
                    //get profile projects
                    new ProjectControlAsyncTask(loginActivity).execute(Constants.URL_GET_PROFILE_PROJECTS_API + String.valueOf(LoginActivity.accountId));
                    //get projects
                    new ProjectsAsyncTask(loginActivity).execute(Constants.URL_GET_PROJECTS_API);
                    //get contacts
                    new ContactAsyncTask(loginActivity).execute(Constants.URL_GET_CONTACTS_API + String.valueOf(LoginActivity.accountId));
                    //get profile tasks
                    new TaskProfileAsyncTask(loginActivity).execute(Constants.URL_PROFILE_TASKS_API + String.valueOf(LoginActivity.accountId));
                    //get profile meetings
                    new MeetingsProfileAsyncTask(loginActivity).execute(Constants.URL_PROFILE_MEETINGS_API + String.valueOf(LoginActivity.accountId));
                    //get criteria
                    new CriteriaAsyncTask().execute(Constants.URL_GET_CRITERIA_API);
                    //reload projectMeeting for user
                    new ProfileMeetingsAsyncTask(loginActivity).execute(Constants.URL_PROFILEMEETINGS_BY_PROFILE_API + LoginActivity.accountId);
                    if (LoginActivity.isLoadedTasks
                            && LoginActivity.isLoadedCriteria
                            && LoginActivity.isLoadedMeetings
                            && LoginActivity.isLoadedProjects
                            && LoginActivity.isLoadedContacts
                            && LoginActivity.isLoadedProfileProjects){
                        progressDialog.dismiss();
//                        Intent intentMenu = new Intent(loginActivity, MenuActivity.class);
                        Intent intentMenu = new Intent(loginActivity, MainActivity.class);
                        loginActivity.startActivity(intentMenu);
                    }else{
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
//                        Intent intentMenu = new Intent(loginActivity, MenuActivity.class);
                        Intent intentMenu = new Intent(loginActivity, MainActivity.class);
                        loginActivity.startActivity(intentMenu);
                    }

                    isValidUser = true;
                    break;

                }
            }
            if (!isValidUser) {
                progressDialog.dismiss();
                Toast.makeText(loginActivity, "Email or Password is incorrect !", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(loginActivity, "Can not connect to server !", Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();
        }


    }
}

