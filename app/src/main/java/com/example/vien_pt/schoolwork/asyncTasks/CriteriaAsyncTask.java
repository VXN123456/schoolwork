package com.example.vien_pt.schoolwork.asyncTasks;

import android.os.AsyncTask;

import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.CriterionModel;
import com.example.vien_pt.schoolwork.models.CriterionModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vien-PT on 24-Nov-17.
 */

public class CriteriaAsyncTask extends AsyncTask<String, Void, List<CriterionModel>> {
    String result;
    
    
    @Override
    protected List<CriterionModel> doInBackground(String... params) {
        String jsonFinal = null;
        try {
            jsonFinal = Utils.makeRequest(Constants.GET_METHOD, params[0], null, Constants.CONTENT_TYPE_JSON, "");
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONArray jsonArrayParent = null;
        try {
            jsonArrayParent = new JSONArray(jsonFinal);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        List<CriterionModel> criterionModelList = new ArrayList<>();
        if (jsonArrayParent != null) {
            for (int i = 0; i < jsonArrayParent.length(); i++) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = jsonArrayParent.getJSONObject(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    CriterionModel criterionModel = new CriterionModel();
                    criterionModel.setId(jsonObject.getInt("id"));
                    criterionModel.setProfileId(jsonObject.getInt("profileId"));
                    criterionModel.setProjectId(jsonObject.getInt("projectId"));
                    criterionModel.setName(jsonObject.getString("name"));
                    criterionModel.setWeight(jsonObject.getInt("weight"));
                    criterionModel.setScore(jsonObject.getDouble("score"));
                    criterionModel.setNote(jsonObject.getString("note"));

                    criterionModelList.add(criterionModel);
                } catch (JSONException ignored) {
                }
            }
        }

        return criterionModelList;
    }

    @Override
    protected void onPostExecute(List<CriterionModel> criterionModels) {
        LoginActivity.criteria = criterionModels;
        LoginActivity.isLoadedCriteria =true;
    }
}
