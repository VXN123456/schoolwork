package com.example.vien_pt.schoolwork.asyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;

import java.io.IOException;

/**
 * Created by Vien-PT on 03-Dec-17.
 */

public class UpdateNotificationsAsyncTask extends AsyncTask<String, Void, String> {
    String jsonEntityRequest = null;
    Context context;
    String result = null;
    ProgressDialog progressDialog;

    public UpdateNotificationsAsyncTask(Context context, String jsonEntityRequest) {
        this.jsonEntityRequest = jsonEntityRequest;
        this.context = context;
        this.progressDialog = new ProgressDialog(context, R.style.DialogThemMain);
    }


    @Override
    protected String doInBackground(String... params) {
        try {
            result = Utils.makeRequest(Constants.POST_METHOD, params[0], null, Constants.CONTENT_TYPE_JSON, this.jsonEntityRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        progressDialog.dismiss();
    }
}
