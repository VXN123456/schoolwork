package com.example.vien_pt.schoolwork.models;

import java.io.Serializable;

/**
 * Created by Vien-PT on 02-Dec-17.
 */

public class ClassificationModel implements Serializable{

    private int id;
    private int profileId;
    private String name;
    private double fromScore;
    private double toScore;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getFromScore() {
        return fromScore;
    }

    public void setFromScore(double fromScore) {
        this.fromScore = fromScore;
    }

    public double getToScore() {
        return toScore;
    }

    public void setToScore(double toScore) {
        this.toScore = toScore;
    }
}
