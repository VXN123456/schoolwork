package com.example.vien_pt.schoolwork.asyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.activities.TasksOfProjectActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;

import java.io.IOException;

/**
 * Created by Vien-PT on 08-Dec-17.
 */

public class UpdateTaskStatusAsyncTask extends AsyncTask<String, Void, String> {
    String result = null;
    ProgressDialog progressDialog;
    Context context;

    public UpdateTaskStatusAsyncTask(Context context) {
        this.context = context;
        this.progressDialog = new ProgressDialog(context, R.style.DialogThemMain);
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setMessage("Processing...");
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            result = Utils.makeRequest(Constants.POST_METHOD,params[0],null,Constants.CONTENT_TYPE_JSON, Constants.EMPTY);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result.equals("Updated")){
            Toast.makeText(context, "Update successful", Toast.LENGTH_SHORT).show();
            new TaskProfileAsyncTask(context).execute(Constants.URL_PROFILE_TASKS_API + String.valueOf(LoginActivity.accountId));
        }else{
            Toast.makeText(context, "Update failed!", Toast.LENGTH_SHORT).show();
        }
        progressDialog.dismiss();
    }
}
