package com.example.vien_pt.schoolwork.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.adapters.ContactAdapter;
import com.example.vien_pt.schoolwork.asyncTasks.DeleteProjectAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.TaskProjectAsyncTask;
import com.example.vien_pt.schoolwork.base.BaseActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.ProfileModel;
import com.example.vien_pt.schoolwork.models.ProjectModel;
import com.example.vien_pt.schoolwork.models.TaskModel;

import java.util.ArrayList;
import java.util.List;

public class ProjectDetailActivity extends BaseActivity {

    public static List<Integer> membersEditToProject;
    public static List<ProfileModel> members;
    public static List<TaskModel> projectTasks;

    private ProjectModel projectModel;
    private List<TaskModel> taskModels;
    private ContactAdapter contactAdapter;
    private ListView lvMembers;
    private SwipeRefreshLayout swipeRefreshTaskList;
    private TextView tvProjectName;
    private TextView tvProjectCode;
    private TextView tvMainGuide;
    private TextView tvDescription;
    private TextView tvRequirement;
    private TextView tvKeyword;
    private TextView tvReference;
    private TextView tvStartDate;
    private TextView tvEndDate;
    Button btnShowTasks;
    Button btnShowHideProjectName;
    Button btnShowHideDescription;
    Button btnShowHideRequirement;
    Button btnShowHideKeyword;
    Button btnShowHideReference;

    boolean isShownFullProjectName;
    boolean isShownFullDescription;
    boolean isShownFullRequirement;
    boolean isShownFullKeyword;
    boolean isShownFullReference;
//    LinearLayout lnlMembers;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_detail);

        getProjectModelFromProjectList();

        //init view
        initView();
        //get project model
        getProjectModelFromProjectList();

        membersEditToProject = new ArrayList<>();

        processingMembers();
        //set view project
        setViewProject();

        //flag show/hide project name and description
        isShownFullProjectName = false;
        isShownFullDescription = false;
        isShownFullRequirement = false;
        isShownFullKeyword = false;
        isShownFullReference = false;

        //show/hide project name
        btnShowHideProjectName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isShownFullProjectName) {
                    tvProjectName.setEllipsize(null);
                    tvProjectName.setMaxLines(Integer.MAX_VALUE);
                    btnShowHideProjectName.setText("Collapse");
                    isShownFullProjectName = true;
                } else {
                    tvProjectName.setEllipsize(TextUtils.TruncateAt.END);
                    tvProjectName.setLines(2);
                    btnShowHideProjectName.setText("Show more...");
                    isShownFullProjectName = false;
                }
            }
        });
        //show/hide project description
        btnShowHideDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isShownFullDescription) {
                    tvDescription.setEllipsize(null);
                    tvDescription.setMaxLines(Integer.MAX_VALUE);
                    btnShowHideDescription.setText("Collapse");
                    isShownFullDescription = true;
                } else {
                    tvDescription.setEllipsize(TextUtils.TruncateAt.END);
                    tvDescription.setLines(2);
                    btnShowHideDescription.setText("Show more...");
                    isShownFullDescription = false;
                }
            }
        });
        //show/hide project requirements
        btnShowHideRequirement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isShownFullRequirement) {
                    tvRequirement.setEllipsize(null);
                    tvRequirement.setMaxLines(Integer.MAX_VALUE);
                    btnShowHideRequirement.setText("Collapse");
                    isShownFullRequirement = true;
                } else {
                    tvRequirement.setEllipsize(TextUtils.TruncateAt.END);
                    tvRequirement.setLines(2);
                    btnShowHideRequirement.setText("Show more...");
                    isShownFullRequirement = false;
                }
            }
        });
        //show/hide project keywords
        btnShowHideKeyword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isShownFullKeyword) {
                    tvKeyword.setEllipsize(null);
                    tvKeyword.setMaxLines(Integer.MAX_VALUE);
                    btnShowHideKeyword.setText("Collapse");
                    isShownFullKeyword = true;
                } else {
                    tvKeyword.setEllipsize(TextUtils.TruncateAt.END);
                    tvKeyword.setLines(2);
                    btnShowHideKeyword.setText("Show more...");
                    isShownFullKeyword = false;
                }
            }
        });
        //show/hide project references
        btnShowHideReference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isShownFullReference) {
                    tvReference.setEllipsize(null);
                    tvReference.setMaxLines(Integer.MAX_VALUE);
                    btnShowHideReference.setText("Collapse");
                    isShownFullReference = true;
                } else {
                    tvReference.setEllipsize(TextUtils.TruncateAt.END);
                    tvReference.setLines(2);
                    btnShowHideReference.setText("Show more...");
                    isShownFullReference = false;
                }
            }
        });

        //show tasks list
        btnShowTasks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TaskProjectAsyncTask(ProjectDetailActivity.this, projectModel,true).execute(Constants.URL_PROJECT_TASKS_API + String.valueOf(projectModel.getId()));
            }
        });
    }

    private void processingMembers() {
        // get members joined project
        Integer[] memberIds = projectModel.getMembers();
        membersEditToProject.clear();
        for (int i = 0; i < memberIds.length; i++) {
            membersEditToProject.add(memberIds[i]);
        }
        // get members joined project
        members = new ArrayList<>();
        for (int id = 0; id < memberIds.length; id++) {
            if (memberIds[id] == LoginActivity.accountId) {
                members.add(MainActivity.currentAccount);//add me
            } else {
                for (ProfileModel profileModel : MainActivity.contactList) {
                    if (profileModel.getId() == memberIds[id]) {
                        members.add(profileModel);
                        break;
                    }
                }
            }
        }
    }

    private void setViewProject() {
        MainActivity.isContactToAdd = false;
        if (projectModel != null) {
            //set tên project
            if (!projectModel.getName().equals(Constants.NULL_STRING) && !projectModel.getName().equals(Constants.EMPTY)) {
                tvProjectName.setText(projectModel.getName());
                tvProjectName.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        int linesCountProjectName = tvProjectName.getLayout().getLineCount();
                        if (linesCountProjectName > 2) {
                            tvProjectName.setEllipsize(TextUtils.TruncateAt.END);
                            tvProjectName.setLines(2);
                            btnShowHideProjectName.setVisibility(View.VISIBLE);
                            btnShowHideProjectName.setText("Show more...");
                            isShownFullProjectName = false;
                        } else {
                            btnShowHideProjectName.setVisibility(View.GONE);
                        }
                        tvProjectName.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
            }
            //set mô tả project
            if (!projectModel.getDescription().equals(Constants.NULL_STRING)&& !projectModel.getDescription().equals(Constants.EMPTY)) {
                tvDescription.setText(projectModel.getDescription());
                tvDescription.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        int linesCountDescription = tvDescription.getLayout().getLineCount();
                        if (linesCountDescription > 2) {
                            tvDescription.setEllipsize(TextUtils.TruncateAt.END);
                            tvDescription.setLines(2);
                            btnShowHideDescription.setVisibility(View.VISIBLE);
                            btnShowHideDescription.setText("Show more...");
                            isShownFullDescription = false;
                        } else {
                            btnShowHideDescription.setVisibility(View.GONE);
                        }
                        tvDescription.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
            }
            // set giáo viên hướng dẫn chính


            //set yêu cầu ban đầu
            if (!projectModel.getRequirement().equals(Constants.NULL_STRING)&& !projectModel.getRequirement().equals(Constants.EMPTY)) {
                tvRequirement.setText(projectModel.getRequirement());
                tvRequirement.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        int linesCountRequirement = tvRequirement.getLayout().getLineCount();
                        if (linesCountRequirement > 2) {
                            tvRequirement.setEllipsize(TextUtils.TruncateAt.END);
                            tvRequirement.setLines(2);
                            btnShowHideRequirement.setVisibility(View.VISIBLE);
                            btnShowHideRequirement.setText("Show more...");
                            isShownFullRequirement = false;
                        } else {
                            btnShowHideRequirement.setVisibility(View.GONE);
                        }
                        tvRequirement.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
            }
            //set từ khóa
            if (!projectModel.getKeyword().equals(Constants.NULL_STRING)&& !projectModel.getKeyword().equals(Constants.EMPTY)) {
                tvKeyword.setText(projectModel.getKeyword());
                tvKeyword.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        int linesCountKeyword = tvKeyword.getLayout().getLineCount();
                        if (linesCountKeyword > 2) {
                            tvKeyword.setEllipsize(TextUtils.TruncateAt.END);
                            tvKeyword.setLines(2);
                            btnShowHideKeyword.setVisibility(View.VISIBLE);
                            btnShowHideKeyword.setText("Show more...");
                            isShownFullKeyword = false;
                        } else {
                            btnShowHideKeyword.setVisibility(View.GONE);
                        }
                        tvKeyword.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
            }

            //set tài liệu tham khảo
            if (!projectModel.getReference().equals(Constants.NULL_STRING)&& !projectModel.getReference().equals(Constants.EMPTY)) {
                tvReference.setText(projectModel.getReference());
                tvReference.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        int linesCountProjectName = tvReference.getLayout().getLineCount();
                        if (linesCountProjectName > 2) {
                            tvReference.setEllipsize(TextUtils.TruncateAt.END);
                            tvReference.setLines(2);
                            btnShowHideReference.setVisibility(View.VISIBLE);
                            btnShowHideReference.setText("Show more...");
                            isShownFullReference = false;
                        } else {
                            btnShowHideReference.setVisibility(View.GONE);
                        }
                        tvReference.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
            }

            if (!projectModel.getCode().equals(Constants.NULL_STRING)&& !projectModel.getCode().equals(Constants.EMPTY)) {
                tvProjectCode.setText(projectModel.getCode());
            }
            if (!projectModel.getTimeStart().equals(Constants.NULL_STRING)&& !projectModel.getTimeStart().equals(Constants.EMPTY)) {
                tvStartDate.setText(projectModel.getTimeStart());
            }
            if (!projectModel.getTimeEnd().equals(Constants.NULL_STRING)&& !projectModel.getTimeEnd().equals(Constants.EMPTY)) {
                tvEndDate.setText(projectModel.getTimeEnd());
            }
            if (projectModel.getMainGuide() != 0) {
                for (ProfileModel profileModel : members) {
                    if (profileModel.getId() == projectModel.getMainGuide()) {
                        tvMainGuide.setText(profileModel.getFullName());
                    }
                }
            }

        }
        if (members != null && members.size() > 0) {
            ContactAdapter contactAdapter = new ContactAdapter(ProjectDetailActivity.this, R.layout.item_contact_mini, members, false);
            lvMembers.setAdapter(contactAdapter);
        }
        //row short click
        lvMembers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ProfileModel profileModel = (ProfileModel) lvMembers.getItemAtPosition(position);
                Intent intentContactDetail = new Intent(ProjectDetailActivity.this, ContactDetailActivity.class);
                intentContactDetail.putExtra("profile", profileModel);
                startActivity(intentContactDetail);
            }
        });
//        Utils.setListViewHeightBasedOnItems(lvMembers, R.layout.item_contact_mini, ProjectDetailActivity.this);
    }

    //init view
    public void initView() {
        setToolBar("Project Detail");
        setColorStatusBar(this);
        tvProjectName = (TextView) findViewById(R.id.tvProjectName);
        tvProjectCode = (TextView) findViewById(R.id.tvProjectCode);
        tvMainGuide = (TextView) findViewById(R.id.tvMainGuide);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
        tvRequirement = (TextView) findViewById(R.id.tvRequirement);
        tvKeyword = (TextView) findViewById(R.id.tvKeyword);
        tvReference = (TextView) findViewById(R.id.tvReference);
        tvStartDate = (TextView) findViewById(R.id.tvStartDate);
        tvEndDate = (TextView) findViewById(R.id.tvEndDate);
        lvMembers = (ListView) findViewById(R.id.lvMembers);
        btnShowTasks = (Button) findViewById(R.id.btnShowTasks);
        btnShowHideProjectName = (Button) findViewById(R.id.btnShowHideProjectName);
        btnShowHideDescription = (Button) findViewById(R.id.btnShowHideDescription);
        btnShowHideRequirement = (Button) findViewById(R.id.btnShowHideRequirement);
        btnShowHideKeyword = (Button) findViewById(R.id.btnShowHideKeyword);
        btnShowHideReference = (Button) findViewById(R.id.btnShowHideReference);
//        lnlMembers = (LinearLayout)findViewById(R.id.lnlMembers);

    }

    //get project model data
    public void getProjectModelFromProjectList() {
        Intent intent = getIntent();
        projectModel = (ProjectModel) intent.getSerializableExtra("project");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.project_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.delete:
                new AlertDialog.Builder(this, R.style.DialogThemMain)
                        .setMessage("Do you want to delete this project ?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (!Utils.isConnectingToInternet(getApplicationContext())) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(ProjectDetailActivity.this, R.style.DialogThemMain);
                                    builder.setMessage("No internet connection, please try again!")
                                            .setPositiveButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.cancel();
                                                }
                                            }).show();
                                } else if (MainActivity.currentAccount.getRole().equals("Teacher") && MainActivity.currentAccount.getId()==projectModel.getMainGuide()) {
                                    DeleteProjectAsyncTask deleteProjectAsyncTask = new DeleteProjectAsyncTask(ProjectDetailActivity.this, projectModel.getId());
                                    deleteProjectAsyncTask.execute(Constants.URL_DELETE_PROJECT_API + String.valueOf(projectModel.getId()));

                                } else {
                                    Toast.makeText(ProjectDetailActivity.this, "Permission Denied", Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                }
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
                return true;
            case R.id.member:
                Intent intentMember = new Intent(ProjectDetailActivity.this, MemberProjectActivity.class);
                intentMember.putExtra("project", projectModel);
                startActivity(intentMember);
                return true;
            case R.id.edit:
                if (LoginActivity.accountId == projectModel.getMainGuide()) {
                    Intent intentEditProject = new Intent(ProjectDetailActivity.this, EditProjectActivity.class);
                    intentEditProject.putExtra("project", projectModel);
                    startActivityForResult(intentEditProject, 1);
                } else {
                    Toast.makeText(this, "This function for main guide Teacher!", Toast.LENGTH_SHORT).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(this.getClass().getSimpleName(), Constants.ON_START_ACTIVITY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(this.getClass().getSimpleName(), "onActivityResult");
        if (requestCode == 1 && resultCode == RESULT_OK) {
            projectModel = (ProjectModel) data.getSerializableExtra("project");
            onResume();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        processingMembers();
//        setViewProject();
        Log.d(this.getClass().getSimpleName(), Constants.ON_RESUME_ACTIVITY);

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(this.getClass().getSimpleName(), Constants.ON_PAUSE_ACTIVITY);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(this.getClass().getSimpleName(), Constants.ON_STOP_ACTIVITY);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(this.getClass().getSimpleName(), Constants.ON_RESTART_ACTIVITY);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(this.getClass().getSimpleName(), Constants.ON_DESTROY_ACTIVITY);
    }

}
