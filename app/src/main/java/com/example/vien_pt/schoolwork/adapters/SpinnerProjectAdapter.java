package com.example.vien_pt.schoolwork.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.CreateTaskActivity;
import com.example.vien_pt.schoolwork.models.ProjectModel;

import java.util.List;

/**
 * Created by Vien-PT on 11-Nov-17.
 */

public class SpinnerProjectAdapter extends ArrayAdapter<ProjectModel> implements SpinnerAdapter {
    Context context;
    List<ProjectModel> projectModels;
    public SpinnerProjectAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<ProjectModel> objects) {
        super(context, resource, objects);
        this.context = context;
        this.projectModels = objects;
    }
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txt = new TextView(context);
        txt.setPadding(16, 16, 16, 16);
        txt.setTextSize(18);
        txt.setGravity(Gravity.CENTER_VERTICAL);
        txt.setText(projectModels.get(position).getName());
        txt.setTextColor(Color.parseColor("#000000"));
        return  txt;
    }

    public View getView(int i, View view, ViewGroup viewgroup) {
        TextView txt = new TextView(context);
        txt.setGravity(Gravity.START);
        txt.setPadding(16, 16, 16, 16);
        txt.setTextSize(16);
        txt.setText(projectModels.get(i).getName());
        txt.setTextColor(Color.parseColor("#000000"));
        return  txt;
    }
}
