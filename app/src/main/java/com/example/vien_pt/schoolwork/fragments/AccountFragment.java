package com.example.vien_pt.schoolwork.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.activities.MainActivity;
import com.example.vien_pt.schoolwork.activities.UpdateAccountActivity;
import com.example.vien_pt.schoolwork.asyncTasks.SignUpAsyncTask;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.helpers.DBHelper;
import com.example.vien_pt.schoolwork.models.ProfileModel;

import org.json.JSONException;
import org.json.JSONObject;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Vien-PT on 08-Oct-17.
 */

public class AccountFragment extends Fragment {
    ImageView imvCoverProfile;
    ImageView imvProfilePicture;
    ImageButton imbEditCoverProfile;
    ImageButton imbEditProfilePicture;
    TextView tvProfileName;
    TextView tvEmail;
    TextView tvPhoneNumber;
    TextView tvCodeId;
    TextView tvStudyStart;
    TextView tvStudyEnd;
    TextView tvCurrentSemester;

    public AccountFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_account, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Set a toolbar to replace the action bar.
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Account");
        initAccountView();
        if (MainActivity.currentAccount != null) {
            parseAccountInfo();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.account_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logOut:
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),R.style.DialogThemMain);
                builder.setMessage("Do you want to log out!")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                DBHelper db = new DBHelper((getActivity()));
                                db.deleteState();
                                Intent intent_Login = new Intent(getActivity(), LoginActivity.class);
                                (getActivity()).finish();
                                startActivity(intent_Login);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).show();
                return true;
            case R.id.edit:
                Intent intentUpdateAccount = new Intent(getActivity(), UpdateAccountActivity.class);
                intentUpdateAccount.putExtra("profile",MainActivity.currentAccount);
                startActivityForResult(intentUpdateAccount,2);
                return true;
            case R.id.changePassword:
                Dialog dialogChangePassword = new Dialog(getActivity());
                dialogChangePassword.setContentView(R.layout.dialog_change_password);
                final AutoCompleteTextView edtCurrentPassword = (AutoCompleteTextView)dialogChangePassword.findViewById(R.id.edtCurrentPassword);
                final AutoCompleteTextView edtNewPassword = (AutoCompleteTextView)dialogChangePassword.findViewById(R.id.edtNewPassword);
                final AutoCompleteTextView edtConfirmNewPassword = (AutoCompleteTextView)dialogChangePassword.findViewById(R.id.edtConfirmNewPassword);
                final TextView tvWarning  = (TextView)dialogChangePassword.findViewById(R.id.tvWarning);
                Button btnUpdatePassword = (Button)dialogChangePassword.findViewById(R.id.btnUpdatePassword);
                btnUpdatePassword.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!edtNewPassword.getText().toString().equals(edtConfirmNewPassword.getText().toString())){
                            tvWarning.setVisibility(View.VISIBLE);
                            tvWarning.setText("Password confirm not match!");
                        }else if(!edtCurrentPassword.getText().toString().equals(MainActivity.currentAccount.getPassword())){
                            tvWarning.setVisibility(View.VISIBLE);
                            tvWarning.setText("Incorrect current password!");
                        }else{
                            JSONObject profileObject = new JSONObject();
                            try {
                                profileObject.put("id", LoginActivity.accountId);
                                profileObject.put("email", MainActivity.currentAccount.getEmail());
                                profileObject.put("password", edtNewPassword.getText().toString());
                                profileObject.put("codeId",MainActivity.currentAccount.getCodeId());
                                profileObject.put("phoneNumber", MainActivity.currentAccount.getPhoneNumber());
                                profileObject.put("fullName", MainActivity.currentAccount.getFullName());
                                profileObject.put("studyStart", MainActivity.currentAccount.getStudyStart());
                                profileObject.put("studyEnd", MainActivity.currentAccount.getStudyStart());
                                profileObject.put("role", MainActivity.currentAccount.getRole());
                                profileObject.put("picture", "");
                                profileObject.put("currentSemester", Integer.valueOf(MainActivity.currentAccount.getCurrentSemester()));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            new SignUpAsyncTask(getActivity(), profileObject.toString(),null).execute(Constants.URL_ADD_OR_UPDATE_PROFILE_API);
                        }
                    }
                });
                dialogChangePassword.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(this.getClass().getSimpleName(), "onActivityResult");
        if (requestCode==2 && resultCode == RESULT_OK){
            MainActivity.currentAccount = (ProfileModel) data.getSerializableExtra("profile");
            onResume();
        }
    }

    //init account view info
    public void initAccountView() {
        imvCoverProfile = (ImageView) getView().findViewById(R.id.imvCoverProfile);
        imvProfilePicture = (ImageView) getView().findViewById(R.id.imvProfilePicture);
        imbEditCoverProfile = (ImageButton) getView().findViewById(R.id.imbEditCoverProfile);
        imbEditProfilePicture = (ImageButton) getView().findViewById(R.id.imbEditProfilePicture);
        tvProfileName = (TextView) getView().findViewById(R.id.tvProfileName);
        tvEmail = (TextView) getView().findViewById(R.id.tvEmail);
        tvPhoneNumber = (TextView) getView().findViewById(R.id.tvPhoneNumber);
        tvCodeId = (TextView) getView().findViewById(R.id.tvCodeId);
        tvStudyStart = (TextView) getView().findViewById(R.id.tvStudyStart);
        tvStudyEnd = (TextView) getView().findViewById(R.id.tvStudyEnd);
        tvCurrentSemester = (TextView) getView().findViewById(R.id.tvCurrentSemester);
    }

    @Override
    public void onResume() {
        super.onResume();
        parseAccountInfo();
    }

    //parse date profile to view
    public void parseAccountInfo() {
        if (!MainActivity.currentAccount.getFullName().equals(Constants.NULL_STRING)) {
            tvProfileName.setText(MainActivity.currentAccount.getFullName());
        }
        if (!MainActivity.currentAccount.getEmail().equals(Constants.NULL_STRING)) {
            tvEmail.setText(MainActivity.currentAccount.getEmail());
        }
        if (!MainActivity.currentAccount.getPhoneNumber().equals(Constants.NULL_STRING)) {
            tvPhoneNumber.setText(MainActivity.currentAccount.getPhoneNumber());
        }
        if (!MainActivity.currentAccount.getCodeId().equals(Constants.NULL_STRING)) {
            tvCodeId.setText(MainActivity.currentAccount.getCodeId());
        }
        if (!MainActivity.currentAccount.getStudyStart().equals(Constants.NULL_STRING)) {
            tvStudyStart.setText(MainActivity.currentAccount.getStudyStart());
        }
        if (!MainActivity.currentAccount.getStudyEnd().equals(Constants.NULL_STRING)) {
            tvStudyEnd.setText(MainActivity.currentAccount.getStudyEnd());
        }
        if (MainActivity.currentAccount.getCurrentSemester() != 0)
            tvCurrentSemester.setText(String.valueOf(MainActivity.currentAccount.getCurrentSemester()));
    }
}
