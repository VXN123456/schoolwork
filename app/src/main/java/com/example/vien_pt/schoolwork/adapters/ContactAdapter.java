package com.example.vien_pt.schoolwork.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.CreateProjectActivity;
import com.example.vien_pt.schoolwork.activities.MainActivity;
import com.example.vien_pt.schoolwork.activities.ProjectDetailActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.models.ProfileModel;

import java.util.List;

/**
 * Created by Vien-PT on 01-Nov-17.
 */

public class ContactAdapter extends ArrayAdapter<ProfileModel> {
    Context context;
    private List<ProfileModel> profileModels;
    boolean isCreateProject;
    // View lookup cache
    private static class ViewHolder {
        ImageView imvProfilePicture;
        TextView tvProfileName;
        ImageButton imbAddMember;
        TextView tvRole;
    }
    private LayoutInflater inflater;
    int resource;
    public ContactAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<ProfileModel> objects , boolean isCreateProject) {
        super(context, resource, objects);
        this.context = context;
        this.profileModels = objects;
        this.resource = resource;
        this.isCreateProject = isCreateProject;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ProfileModel profileModel = getItem(position);
        final ViewHolder viewHolder = new ContactAdapter.ViewHolder();
        if (convertView == null) {
            convertView = inflater.inflate(resource, parent, false);
            viewHolder.imvProfilePicture = (ImageView) convertView.findViewById(R.id.imvProfilePicture);
            viewHolder.tvProfileName = (TextView) convertView.findViewById(R.id.tvProfileName);
            viewHolder.imbAddMember = (ImageButton) convertView.findViewById(R.id.imbAddMember);
            viewHolder.tvRole = (TextView)convertView.findViewById(R.id.tvRole);
            convertView.setTag(viewHolder);
        } else {
            viewHolder.imvProfilePicture = (ImageView) convertView.findViewById(R.id.imvProfilePicture);
            viewHolder.tvProfileName = (TextView) convertView.findViewById(R.id.tvProfileName);
            viewHolder.imbAddMember = (ImageButton) convertView.findViewById(R.id.imbAddMember);
            viewHolder.tvRole = (TextView)convertView.findViewById(R.id.tvRole);
        }
        if (profileModel!=null && !profileModel.getFullName().equals(Constants.NULL_STRING)) {
            viewHolder.tvProfileName.setText(profileModel.getFullName());
            viewHolder.tvRole.setText(profileModel.getRole());
        }
        //danh sách thành viên trong trường hợp đang chỉnh sửa project
        if (!isCreateProject){
            if(ProjectDetailActivity.membersEditToProject != null && ProjectDetailActivity.membersEditToProject.size()>0){
                if (ProjectDetailActivity.membersEditToProject.contains(profileModel.getId())){
                    viewHolder.imbAddMember.setBackgroundResource(R.drawable.icon_checked);
                }else {
                    viewHolder.imbAddMember.setBackgroundResource(R.drawable.icon_plus_black);
                }
            }
        }

        if (MainActivity.isContactToAdd){
           viewHolder.imbAddMember.setVisibility(View.VISIBLE);

            viewHolder.imbAddMember.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //trường hợp tạo project
                    if (isCreateProject){
                        if (!CreateProjectActivity.membersAddToProject.contains(profileModel.getId())){
                            CreateProjectActivity.membersAddToProject.add(profileModel.getId());
                            viewHolder.imbAddMember.setBackgroundResource(R.drawable.icon_checked);
                        }else {
                            CreateProjectActivity.membersAddToProject.remove(Integer.valueOf(profileModel.getId()));
                            viewHolder.imbAddMember.setBackgroundResource(R.drawable.icon_plus_black);
                        }
                    }else{//trường hợp edit project
                        if (!ProjectDetailActivity.membersEditToProject.contains(profileModel.getId())){
                            ProjectDetailActivity.membersEditToProject.add(profileModel.getId());
                            viewHolder.imbAddMember.setBackgroundResource(R.drawable.icon_checked);
                        }else {
                            ProjectDetailActivity.membersEditToProject.remove(Integer.valueOf(profileModel.getId()));
                            viewHolder.imbAddMember.setBackgroundResource(R.drawable.icon_plus_black);
                        }
                    }
                }
            });
        }else {
            viewHolder.imbAddMember.setVisibility(View.GONE);
        }

        viewHolder.imvProfilePicture.setImageDrawable(convertView.getResources().getDrawable(R.drawable.icon_profile));

        return convertView;
    }
}
