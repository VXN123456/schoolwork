package com.example.vien_pt.schoolwork.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.asyncTasks.SignUpAsyncTask;
import com.example.vien_pt.schoolwork.base.BaseActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.models.ProfileModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class UpdateAccountActivity extends BaseActivity {
    AwesomeValidation awesomeValidation;
    EditText edtFullName;
    EditText edtEmail;
    EditText edtPhoneNumber;
    EditText edtCodeId;
    //    EditText edtStudyStart;
//    EditText edtStudyEnd;
    EditText edtCurrentSemester;
    ProfileModel profileModel;
    DatePickerDialog datePickerDialog;
    TextView tvStudyStart;
    TextView tvStudyEnd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_account);

        // Set a toolbar to replace the action bar.
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle("Update Account Info");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        //set color status bar
//        setColorStatusBar(this);


        Intent intent = getIntent();
        profileModel = (ProfileModel) intent.getSerializableExtra("profile");
        initView();

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        //adding validation to edit texts
        awesomeValidation.addValidation(this, R.id.edtEmail, Patterns.EMAIL_ADDRESS, R.string.email_error);

        tvStudyStart.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR); // current year
                int month = c.get(Calendar.MONTH); // current month
                int day = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(UpdateAccountActivity.this, android.R.style.Theme_DeviceDefault_Light_Dialog,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
//                                tvStudyStart.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                tvStudyStart.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);

                            }
                        }, year, month, day);
                datePickerDialog.show();
            }
        });
        tvStudyEnd.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR); // current year
                int month = c.get(Calendar.MONTH); // current month
                int day = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(UpdateAccountActivity.this, android.R.style.Theme_DeviceDefault_Light_Dialog,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
//                                tvStudyEnd.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                tvStudyEnd.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);

                            }
                        }, year, month, day);
                datePickerDialog.show();
            }
        });


        Button btnUpdateAccount = (Button) findViewById(R.id.btnUpdateAccount);
        btnUpdateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject profileObject = new JSONObject();
                try {
                    profileObject.put("id", LoginActivity.accountId);
                    profileObject.put("email", edtEmail.getText().toString());
                    profileModel.setEmail(edtEmail.getText().toString());
                    profileObject.put("password", MainActivity.currentAccount.getPassword());
                    profileObject.put("codeId", edtCodeId.getText().toString());
                    profileModel.setCodeId(edtCodeId.getText().toString());
                    profileObject.put("phoneNumber", edtPhoneNumber.getText().toString());
                    profileModel.setPhoneNumber(edtPhoneNumber.getText().toString());
                    profileObject.put("fullName", edtFullName.getText().toString());
                    profileModel.setFullName(edtFullName.getText().toString());
                    profileObject.put("studyStart", tvStudyStart.getText().toString());
                    profileModel.setStudyStart(tvStudyStart.getText().toString());
                    profileObject.put("studyEnd", tvStudyEnd.getText().toString());
                    profileModel.setStudyEnd(tvStudyEnd.getText().toString());
                    profileObject.put("role", MainActivity.currentAccount.getRole());
                    profileObject.put("picture", "");
                    if (!edtCurrentSemester.getText().toString().equals("")) {
                        profileObject.put("currentSemester", Integer.valueOf(edtCurrentSemester.getText().toString()));
                        profileModel.setCurrentSemester(Integer.valueOf(edtCurrentSemester.getText().toString()));
                    } else {
                        profileObject.put("currentSemester", 0);
                        profileModel.setCurrentSemester(0);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                new SignUpAsyncTask(UpdateAccountActivity.this, profileObject.toString(), profileModel).execute(Constants.URL_ADD_OR_UPDATE_PROFILE_API);
            }
        });


    }

    public void initView() {
        setToolBar("Update Account Info");
        setColorStatusBar(this);
        edtFullName = (EditText) findViewById(R.id.edtFullName);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPhoneNumber = (EditText) findViewById(R.id.edtPhoneNumber);
        edtCodeId = (EditText) findViewById(R.id.edtCodeId);
//        edtStudyStart = (EditText) findViewById(R.id.edtStudyStart);
//        edtStudyEnd = (EditText) findViewById(R.id.edtStudyEnd);
        tvStudyStart = (TextView) findViewById(R.id.tvStudyStart);
        tvStudyEnd = (TextView) findViewById(R.id.tvStudyEnd);
        edtCurrentSemester = (EditText) findViewById(R.id.edtCurrentSemester);

        if (!profileModel.getFullName().equals(Constants.NULL_STRING) && !profileModel.getFullName().equals(Constants.EMPTY))
            edtFullName.setText(profileModel.getFullName());
        if (!profileModel.getEmail().equals(Constants.NULL_STRING) && !profileModel.getEmail().equals(Constants.EMPTY))
            edtEmail.setText(profileModel.getEmail());
        if (!profileModel.getPhoneNumber().equals(Constants.NULL_STRING) && !profileModel.getPhoneNumber().equals(Constants.EMPTY))
            edtPhoneNumber.setText(profileModel.getPhoneNumber());
        if (!profileModel.getCodeId().equals(Constants.NULL_STRING) && !profileModel.getCodeId().equals(Constants.EMPTY))
            edtCodeId.setText(profileModel.getCodeId());
        if (!profileModel.getStudyStart().equals(Constants.NULL_STRING) && !profileModel.getStudyStart().equals(Constants.EMPTY))
            tvStudyStart.setText(profileModel.getStudyStart());
        if (!profileModel.getStudyEnd().equals(Constants.NULL_STRING) && !profileModel.getStudyEnd().equals(Constants.EMPTY))
            tvStudyEnd.setText(profileModel.getStudyEnd());
        edtCurrentSemester.setText(String.valueOf(profileModel.getCurrentSemester()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
