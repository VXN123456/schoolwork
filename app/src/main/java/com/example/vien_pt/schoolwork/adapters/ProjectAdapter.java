package com.example.vien_pt.schoolwork.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.activities.ProjectEvaluationActivity;
import com.example.vien_pt.schoolwork.asyncTasks.CriteriaAsyncTask;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.models.ProjectModel;

import java.util.List;

/**
 * Created by Vien-PT on 31-Oct-17.
 */

public class ProjectAdapter extends ArrayAdapter<ProjectModel> {
    private List<ProjectModel> projectModels;
    Context context;

    // View lookup cache
    private static class ViewHolder {
        TextView tvProjectName;
        TextView tvDescription;
        ImageView imvProjectImage;
        ImageView evaluate;
    }

    public ProjectAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<ProjectModel> objects) {
        super(context, resource, objects);
        this.projectModels = objects;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable final View convertView, @NonNull ViewGroup parent) {

        View view = convertView;
        final ProjectModel projectModel = getItem(position);
        ViewHolder viewHolder;
        viewHolder = new ViewHolder();

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_project, null);
        }
        viewHolder.tvProjectName = (TextView) view.findViewById(R.id.tvProjectName);
        viewHolder.tvDescription = (TextView) view.findViewById(R.id.tvDescription);
        viewHolder.imvProjectImage = (ImageView) view.findViewById(R.id.imvProject);
        viewHolder.evaluate = (ImageView) view.findViewById(R.id.evaluate);

        if (projectModel != null) {
            viewHolder.tvProjectName.setText(projectModel.getName());
            if (!projectModel.getDescription().equals(Constants.NULL_STRING))
                viewHolder.tvDescription.setText(projectModel.getDescription());
        }
        viewHolder.evaluate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get criteria
                new CriteriaAsyncTask().execute(Constants.URL_GET_CRITERIA_API);
                if (LoginActivity.isLoadedCriteria){
                    Intent intentEvaluate = new Intent(context, ProjectEvaluationActivity.class);
                    intentEvaluate.putExtra("project",projectModel);
                    context.startActivity(intentEvaluate);
                }

////                Toast.makeText(context, "evaluating", Toast.LENGTH_SHORT).show();
//                final Dialog dialogEvaluate = new Dialog(context);
//                dialogEvaluate.setContentView(R.layout.dialog_evaluate_project);
//                final EditText edtEvaluate = (EditText) dialogEvaluate.findViewById(R.id.edtEvaluate);
//                final EditText edtScore = (EditText) dialogEvaluate.findViewById(R.id.edtScore);
//
//                    if (projectModel!= null && !projectModel.getRating().equals(Constants.NULL_STRING)){
//                        edtEvaluate.setText(projectModel.getRating());
//                    }
//                    if (projectModel != null){
//                        edtScore.setText(String.valueOf(projectModel.getScore()));
//                    }
//
//                Button btnSaveEvaluate = (Button) dialogEvaluate.findViewById(R.id.btnSaveEvaluate);
//                btnSaveEvaluate.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (MenuActivity.currentAccount.getRole().equals("Teacher")) {
//                            JSONObject projectObject = new JSONObject();
//                            try {
//                                if (projectModel != null) {
//                                    double score;
//                                    if (edtScore.getText().toString().equals("")|| edtScore.getText().toString().length()==0){
//                                        score = 0;
//                                    }else {
//                                        score = Double.valueOf(edtScore.getText().toString());
//                                    }
//                                    projectObject.put("id", projectModel.getId());
//                                    projectObject.put("name", projectModel.getName());
//                                    projectObject.put("rating", edtEvaluate.getText().toString());
//                                    projectObject.put("score", score);
//                                    projectObject.put("code", projectModel.getCode());
//                                    projectObject.put("createdBy", projectModel.getCreatedBy());
//                                    projectObject.put("description", projectModel.getDescription());
//                                    Date currentTime = Calendar.getInstance().getTime();
//                                    projectObject.put("createdAt", projectModel.getCreatedAt());
//                                    projectObject.put("updatedAt", currentTime.toString());
//                                    projectObject.put("timeStart", projectModel.getTimeStart());
//                                    projectObject.put("timeEnd", projectModel.getTimeEnd());
//                                    projectObject.put("members", new JSONArray(projectModel.getMembers()));
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            CreateOrUpdateProjectAsyncTask createOrUpdateProjectAsyncTask = new CreateOrUpdateProjectAsyncTask(context, projectObject.toString(),true);
//                            createOrUpdateProjectAsyncTask.execute(Constants.URL_ADD_OR_UPDATE_PROJECT_API);
//                            dialogEvaluate.dismiss();
//                        } else {
//                            Toast.makeText(context, "permission denied", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                });
//
//                dialogEvaluate.show();
            }
        });
        viewHolder.imvProjectImage.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_project));

        return view;
    }
}
