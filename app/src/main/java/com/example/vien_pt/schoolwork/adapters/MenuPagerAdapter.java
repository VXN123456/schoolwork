package com.example.vien_pt.schoolwork.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.vien_pt.schoolwork.activities.MainActivity;
import com.example.vien_pt.schoolwork.fragments.AccountFragment;
import com.example.vien_pt.schoolwork.fragments.ContactsFragment;
import com.example.vien_pt.schoolwork.fragments.MeetingsFragment;
import com.example.vien_pt.schoolwork.fragments.ProjectsFragment;
import com.example.vien_pt.schoolwork.fragments.TasksFragment;

/**
 * Created by Vien-PT on 08-Oct-17.
 */

public class MenuPagerAdapter extends FragmentStatePagerAdapter {
    int numberOfTab;

    public MenuPagerAdapter(FragmentManager fm , int numberOfTab) {
        super(fm);
        this.numberOfTab = numberOfTab;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                ProjectsFragment projectFragment = new ProjectsFragment();
                return projectFragment;
             case 1:
                MeetingsFragment notificationsFragment = new MeetingsFragment();
                return notificationsFragment;
            case 2:
                TasksFragment tasksFragment = new TasksFragment();
                return tasksFragment;
             case 3:
                 MainActivity.isContactToAdd = false;
                ContactsFragment contactsFragment = new ContactsFragment();
                return contactsFragment;
             case 4:
                AccountFragment accountFragment = new AccountFragment();
                return accountFragment;
             default:
                 return null;
        }
    }

    @Override
    public int getCount() {
        return numberOfTab;
    }


//    @Override
//    public CharSequence getPageTitle(int position) {
//        return super.getPageTitle(position);
//    }
}
