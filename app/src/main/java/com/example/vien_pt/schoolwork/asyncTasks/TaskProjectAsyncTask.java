package com.example.vien_pt.schoolwork.asyncTasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.ProjectDetailActivity;
import com.example.vien_pt.schoolwork.activities.TasksOfProjectActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.ProjectModel;
import com.example.vien_pt.schoolwork.models.TaskModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Vien-PT on 02-Nov-17.
 */

public class TaskProjectAsyncTask extends AsyncTask<String, Void, List<TaskModel>> {
    private ProgressDialog progressDialog;
    Context context;
    ProjectModel projectModel;
    boolean isFromProjectDetail;

    public TaskProjectAsyncTask(Context context, ProjectModel projectModel, boolean isFromProjectDetail) {
        progressDialog = new ProgressDialog(context, R.style.DialogThemMain);
        this.context = context;
        this.projectModel = projectModel;
        this.isFromProjectDetail = isFromProjectDetail;
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setMessage("Loading, please wait...");
        progressDialog.show();
    }

    @Override
    protected List<TaskModel> doInBackground(String... params) {
        String jsonFinal = null;
        try {
            jsonFinal = Utils.makeRequest(Constants.GET_METHOD, params[0], null, Constants.CONTENT_TYPE_JSON, "");
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONArray jsonArrayParent = null;
        try {
            jsonArrayParent = new JSONArray(jsonFinal);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        List<TaskModel> taskList = new ArrayList<>();
        if (jsonArrayParent != null) {
            for (int i = 0; i < jsonArrayParent.length(); i++) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = jsonArrayParent.getJSONObject(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    TaskModel taskModel = new TaskModel();
                    taskModel.setId(jsonObject.getInt("id"));
                    taskModel.setName(jsonObject.getString("name"));
                    taskModel.setStatus(jsonObject.getString("status"));
                    taskModel.setDescription(jsonObject.getString("description"));
                    taskModel.setCreatedBy(jsonObject.getInt("createdBy"));
                    taskModel.setMeetingId(jsonObject.getInt("meetingId"));
                    taskModel.setAssignee(jsonObject.getInt("assignee"));
                    taskModel.setCreatedAt(jsonObject.getString("createdAt"));
                    taskModel.setUpdatedAt(jsonObject.getString("updatedAt"));
                    taskModel.setDeadline(jsonObject.getString("deadline"));
                    taskModel.setProjectId(jsonObject.getInt("projectId"));
                    taskModel.setNote(jsonObject.getString("note"));

                    taskList.add(taskModel);
                } catch (JSONException ignored) {
                }
            }
        }
        return taskList;
    }

    @Override
    protected void onPostExecute(List<TaskModel> taskModels) {
        super.onPostExecute(taskModels);
        Collections.reverse(taskModels);
        ProjectDetailActivity.projectTasks = taskModels;
        progressDialog.dismiss();
        if (isFromProjectDetail){
            Intent intentTasksOfProject = new Intent(context, TasksOfProjectActivity.class);
            intentTasksOfProject.putExtra("project",projectModel);
            intentTasksOfProject.putExtra("tasks", (Serializable) taskModels);
            context.startActivity(intentTasksOfProject);
        }
    }
}
