package com.example.vien_pt.schoolwork.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.activities.MainActivity;
import com.example.vien_pt.schoolwork.adapters.MeetingAdapter;
import com.example.vien_pt.schoolwork.asyncTasks.MeetingsProfileAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.ProfileMeetingsAsyncTask;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.DataProcessingManager;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.MeetingModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vien-PT on 01-Dec-17.
 */

public class MeetingsHistoryFragment extends Fragment {
    MeetingAdapter meetingAdapter;
    ListView lvMeetingsHistory;
    SwipeRefreshLayout swipeRefreshMeetingList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_meetings_history, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeRefreshMeetingList = (SwipeRefreshLayout) getView().findViewById(R.id.swipeRefreshMeetingList);
        lvMeetingsHistory = (ListView) getView().findViewById(R.id.lvMeetingsHistory);

        DataProcessingManager.setRelationFieldsForMeeting(getMeetingHistory(LoginActivity.profileMeetings), LoginActivity.projects, LoginActivity.profiles);
        meetingAdapter = new MeetingAdapter(getActivity(), R.layout.item_meeting, getMeetingHistory(LoginActivity.profileMeetings));
        lvMeetingsHistory.setAdapter(meetingAdapter);

        swipeRefreshMeetingList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //reload projectMeeting for user
                new ProfileMeetingsAsyncTask(getActivity()).execute(Constants.URL_PROFILEMEETINGS_BY_PROFILE_API + LoginActivity.accountId);
                //get projects
                new MeetingsProfileAsyncTask(getActivity()).execute(Constants.URL_PROFILE_MEETINGS_API + String.valueOf(LoginActivity.accountId));
                swipeRefreshMeetingList.setRefreshing(false);
                onResume();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        DataProcessingManager.setRelationFieldsForMeeting(getMeetingHistory(LoginActivity.profileMeetings), LoginActivity.projects, LoginActivity.profiles);
        meetingAdapter = new MeetingAdapter(getActivity(), R.layout.item_meeting, getMeetingHistory(LoginActivity.profileMeetings));
        lvMeetingsHistory.setAdapter(meetingAdapter);
        meetingAdapter.notifyDataSetChanged();

        ((MainActivity)getActivity()).setNumberNotice();
    }

    public List<MeetingModel> getMeetingHistory(List<MeetingModel> profileMeetings){
        List<MeetingModel> meetingsHistory = new ArrayList<>();
        if (profileMeetings!=null && profileMeetings.size()>0){
            long currentTime = Utils.convertTimeStampToLongMillisecond(Utils.getCurrentTimeStamp());
            for(MeetingModel meetingModel : profileMeetings){
                long meetingTime = Utils.convertStringDateToLongMillisecond(meetingModel.getDate())
                        +Utils.convertStringTimeToLongMillisecond(meetingModel.getTime()+":00");
                if (Utils.isPassCurrentTime(currentTime,meetingTime)){
                    meetingsHistory.add(meetingModel);
                }
            }
        }
        return meetingsHistory;
    }
}
