package com.example.vien_pt.schoolwork.asyncTasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.RecoverySystem;
import android.support.v4.app.Fragment;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.ProjectModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Vien-PT on 31-Oct-17.
 */

public class ProjectControlAsyncTask extends AsyncTask<String, Void, List<ProjectModel>> {
    private ProgressDialog progressDialog;
    Context context;

    public ProjectControlAsyncTask(Context context) {
        this.progressDialog = new ProgressDialog(context, R.style.DialogThemMain);
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setMessage("Loading, please wait...");
        progressDialog.show();
    }

    @Override
    protected List<ProjectModel> doInBackground(String... params) {
        String jsonFinal = null;
        try {
            jsonFinal = Utils.makeRequest(Constants.GET_METHOD, params[0], null, Constants.CONTENT_TYPE_JSON, "");
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONArray jsonArrayParent = null;
        try {
            jsonArrayParent = new JSONArray(jsonFinal);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        List<ProjectModel> projectModelList = new ArrayList<>();
        if (jsonArrayParent != null) {
            for (int i = 0; i < jsonArrayParent.length(); i++) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = jsonArrayParent.getJSONObject(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    ProjectModel projectModel = new ProjectModel();
                    projectModel.setId(jsonObject.getInt("id"));
                    projectModel.setRating(jsonObject.getString("rating"));
                    projectModel.setScore(jsonObject.getDouble("score"));
                    projectModel.setName(jsonObject.getString("name"));
                    projectModel.setCode(jsonObject.getString("code"));
                    projectModel.setCreatedBy(jsonObject.getInt("createdBy"));
                    projectModel.setDescription(jsonObject.getString("description"));
                    projectModel.setRequirement(jsonObject.getString("requirement"));
                    projectModel.setKeyword(jsonObject.getString("keyword"));
                    projectModel.setReference(jsonObject.getString("reference"));
                    projectModel.setCreatedAt(jsonObject.getString("createdAt"));
                    projectModel.setUpdatedAt(jsonObject.getString("updatedAt"));
                    projectModel.setTimeStart(jsonObject.getString("timeStart"));
                    projectModel.setTimeEnd(jsonObject.getString("timeEnd"));
                    projectModel.setMainGuide(jsonObject.getInt("mainGuide"));
                    JSONArray arrayMember = jsonObject.getJSONArray("members");
                    Integer[] idMembers = new Integer[arrayMember.length()];
                    List<Integer> temp = new ArrayList<>();
                    for (int j = 0; j< arrayMember.length();j++){
                        temp.add(arrayMember.getInt(j));
                    }
                    temp.toArray(idMembers);
                    projectModel.setMembers(idMembers);
                    projectModelList.add(projectModel);
                } catch (JSONException ignored) {
                }
            }
        }

        return projectModelList;
    }

    @Override
    protected void onPostExecute(List<ProjectModel> projectModels) {
        super.onPostExecute(projectModels);
        Collections.reverse(projectModels);
        if (projectModels.size()>0){
            LoginActivity.profileProjects = projectModels;
        }
        LoginActivity.isLoadedProfileProjects = true;
        progressDialog.dismiss();
    }
}
