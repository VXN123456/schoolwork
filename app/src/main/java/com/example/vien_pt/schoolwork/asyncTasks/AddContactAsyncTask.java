package com.example.vien_pt.schoolwork.asyncTasks;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.activities.CreateProjectActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;

import java.io.IOException;

/**
 * Created by Vien-PT on 10-Nov-17.
 */

public class AddContactAsyncTask extends AsyncTask<String, Void, String> {
    String result;
    Context context;
    public AddContactAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            result = Utils.makeRequest(Constants.POST_METHOD,params[0],null,Constants.CONTENT_TYPE_JSON,"");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (result.equals("Add yourself")){
            Toast.makeText(context,"Add yourself!",Toast.LENGTH_SHORT).show();
        }else if (result.equals("Existed in contact")){
            Toast.makeText(context,"Existed in contact",Toast.LENGTH_SHORT).show();
        }else if(result.equals("Not exist")){
            Toast.makeText(context,"Account does not exist",Toast.LENGTH_SHORT).show();
        }else if(result.equals("Added too")){
            Toast.makeText(context,"Contact has been added",Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(context,"Cannot connect to server!",Toast.LENGTH_SHORT).show();
        }
    }
}
