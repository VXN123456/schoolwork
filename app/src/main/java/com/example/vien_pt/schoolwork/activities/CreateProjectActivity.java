package com.example.vien_pt.schoolwork.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.adapters.ContactAdapter;
import com.example.vien_pt.schoolwork.asyncTasks.CreateOrUpdateProjectAsyncTask;
import com.example.vien_pt.schoolwork.base.BaseActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.ProjectModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CreateProjectActivity extends BaseActivity {
    public static List<Integer> membersAddToProject;

    //    AutoCompleteTextView edtProjectName;
//    AutoCompleteTextView edtDescription;
//    AutoCompleteTextView edtRequirement;
//    AutoCompleteTextView edtKeyword;
//    AutoCompleteTextView edtReference;
    AutoCompleteTextView edtProjectName;
    TextView tvDescription;
    TextView tvRequirement;
    TextView tvKeyword;
    TextView tvReference;


    ListView lvContact;
    Button btnCreateProject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(this.getClass().getSimpleName(), Constants.ON_CREATE_ACTIVITY);

        setContentView(R.layout.activity_create_project);
        //

        final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        membersAddToProject = new ArrayList<>();
        initView();
        MainActivity.isContactToAdd = true;
        if (MainActivity.contactList!=null && MainActivity.contactList.size()>0){
            ContactAdapter contactAdapter = new ContactAdapter(CreateProjectActivity.this, R.layout.item_contact, MainActivity.contactList, true);
            lvContact.setAdapter(contactAdapter);
        }

        tvDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialogEdit = new Dialog(CreateProjectActivity.this);
                dialogEdit.setContentView(R.layout.dialog_edit_common);
                TextView tvTitle = (TextView) dialogEdit.findViewById(R.id.tvTitle);
                final EditText edtCommon = (EditText) dialogEdit.findViewById(R.id.edtCommon);
                Button btnSave = (Button)dialogEdit.findViewById(R.id.btnSave) ;
                tvTitle.setText("Descriptions");
                if (!tvDescription.getText().toString().equals(Constants.NULL_STRING)) {
                    edtCommon.setText(tvDescription.getText().toString());
                    edtCommon.setSelection(edtCommon.getText().toString().length());
                }
                edtCommon.requestFocus();
                inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);
                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!edtCommon.getText().toString().trim().equals(Constants.NULL_STRING) && !edtCommon.getText().toString().trim().equals(Constants.EMPTY) ){
                            tvDescription.setText(edtCommon.getText().toString().trim());
                        }
                        tvDescription.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                int lines = tvDescription.getLayout().getLineCount();
                                if (lines>1){
                                    tvDescription.setLines(1);
                                    tvDescription.setEllipsize(TextUtils.TruncateAt.END);
                                }
                                tvDescription.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            }
                        });
                        dialogEdit.dismiss();
                    }
                });
                dialogEdit.show();
            }
        });
        //Enter requirement
        tvRequirement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialogEdit = new Dialog(CreateProjectActivity.this);
                dialogEdit.setContentView(R.layout.dialog_edit_common);
                TextView tvTitle = (TextView) dialogEdit.findViewById(R.id.tvTitle);
                final EditText edtCommon = (EditText) dialogEdit.findViewById(R.id.edtCommon);
                Button btnSave = (Button)dialogEdit.findViewById(R.id.btnSave) ;
                tvTitle.setText("Requirements");
                if (!tvRequirement.getText().toString().equals(Constants.NULL_STRING)) {
                    edtCommon.setText(tvRequirement.getText().toString());
                    edtCommon.setSelection(edtCommon.getText().toString().length());
                }
                edtCommon.requestFocus();
                inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);
                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!edtCommon.getText().toString().trim().equals(Constants.NULL_STRING) && !edtCommon.getText().toString().trim().equals(Constants.EMPTY) ){
                            tvRequirement.setText(edtCommon.getText().toString().trim());
                        }
                        tvRequirement.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                int lines = tvRequirement.getLayout().getLineCount();
                                if (lines>1){
                                    tvRequirement.setLines(1);
                                    tvRequirement.setEllipsize(TextUtils.TruncateAt.END);
                                }
                                tvRequirement.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            }
                        });
                        dialogEdit.dismiss();
                    }
                });
                dialogEdit.show();
            }
        });
        //Enter keywords
        tvKeyword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialogEdit = new Dialog(CreateProjectActivity.this);
                dialogEdit.setContentView(R.layout.dialog_edit_common);
                TextView tvTitle = (TextView) dialogEdit.findViewById(R.id.tvTitle);
                final EditText edtCommon = (EditText) dialogEdit.findViewById(R.id.edtCommon);
                Button btnSave = (Button)dialogEdit.findViewById(R.id.btnSave) ;
                tvTitle.setText("Keywords");
                if (!tvKeyword.getText().toString().equals(Constants.NULL_STRING)) {
                    edtCommon.setText(tvKeyword.getText().toString());
                    edtCommon.setSelection(edtCommon.getText().toString().length());
                }
                edtCommon.requestFocus();
                inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);
                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!edtCommon.getText().toString().trim().equals(Constants.NULL_STRING) && !edtCommon.getText().toString().trim().equals(Constants.EMPTY) ){
                            tvKeyword.setText(edtCommon.getText().toString().trim());
                        }
                        tvKeyword.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                int lines = tvKeyword.getLayout().getLineCount();
                                if (lines>1){
                                    tvKeyword.setLines(1);
                                    tvKeyword.setEllipsize(TextUtils.TruncateAt.END);
                                }
                                tvKeyword.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            }
                        });
                        dialogEdit.dismiss();
                    }
                });
                dialogEdit.show();
            }
        });
        //Enter references
        tvReference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialogEdit = new Dialog(CreateProjectActivity.this);
                dialogEdit.setContentView(R.layout.dialog_edit_common);
                TextView tvTitle = (TextView) dialogEdit.findViewById(R.id.tvTitle);
                final EditText edtCommon = (EditText) dialogEdit.findViewById(R.id.edtCommon);
                Button btnSave = (Button)dialogEdit.findViewById(R.id.btnSave) ;
                tvTitle.setText("References");
                if (!tvReference.getText().toString().equals(Constants.NULL_STRING)) {
                    edtCommon.setText(tvReference.getText().toString());
                    edtCommon.setSelection(edtCommon.getText().toString().length());
                }
                edtCommon.requestFocus();
                inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);
                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!edtCommon.getText().toString().trim().equals(Constants.NULL_STRING) && !edtCommon.getText().toString().trim().equals(Constants.EMPTY) ){
                            tvReference.setText(edtCommon.getText().toString().trim());
                        }
                        tvReference.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                int lines = tvReference.getLayout().getLineCount();
                                if (lines>1){
                                    tvReference.setLines(1);
                                    tvReference.setEllipsize(TextUtils.TruncateAt.END);
                                }
                                tvReference.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            }
                        });
                        dialogEdit.dismiss();
                    }
                });
                dialogEdit.show();
            }
        });

        //click button create project
        btnCreateProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isConnectingToInternet(getApplicationContext())) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(CreateProjectActivity.this, R.style.DialogThemMain);
                    builder.setMessage("You are currently offline!")
                            .setPositiveButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            }).show();
                } else if (edtProjectName.getText().toString().equals(Constants.EMPTY)) {
                    Toast.makeText(getApplicationContext(), "Please enter project name!", Toast.LENGTH_SHORT).show();
                } else {
                    JSONObject projectObject = new JSONObject();
                    CreateProjectActivity.membersAddToProject.add(LoginActivity.accountId);
                    JSONArray arrayMembers = new JSONArray(CreateProjectActivity.membersAddToProject);

                    try {
                        projectObject.put("id", 0);
                        projectObject.put("name", edtProjectName.getText().toString());
                        projectObject.put("code", "");
                        projectObject.put("createdBy", MainActivity.currentAccount.getId());

                        projectObject.put("description", tvDescription.getText().toString());
                        projectObject.put("requirement", tvRequirement.getText().toString());
                        projectObject.put("keyword", tvKeyword.getText().toString());
                        projectObject.put("reference", tvReference.getText().toString());
                        projectObject.put("createdAt", Utils.getCurrentTimeStamp());
                        projectObject.put("updatedAt", Utils.getCurrentTimeStamp());
                        projectObject.put("timeStart", "");
                        projectObject.put("timeEnd", "");
                        projectObject.put("mainGuide", MainActivity.currentAccount.getId());
                        projectObject.put("members", arrayMembers);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    CreateOrUpdateProjectAsyncTask createOrUpdateProjectAsyncTask = new CreateOrUpdateProjectAsyncTask(CreateProjectActivity.this, projectObject.toString(), null);
                    createOrUpdateProjectAsyncTask.execute(Constants.URL_ADD_OR_UPDATE_PROJECT_API);
                }
            }
        });

    }

    public void initView() {
        // Set a toolbar to replace the action bar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Create Project");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //set color status bar
        setColorStatusBar(this);
        CreateProjectActivity.membersAddToProject.clear();
        edtProjectName = (AutoCompleteTextView) findViewById(R.id.edtProjectName);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
        tvRequirement = (TextView) findViewById(R.id.tvRequirement);
        tvKeyword = (TextView) findViewById(R.id.tvKeyword);
        tvReference = (TextView) findViewById(R.id.tvReference);
        lvContact = (ListView) findViewById(R.id.lvContact);
        btnCreateProject = (Button) findViewById(R.id.btnCreateProject);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(this.getClass().getSimpleName(), Constants.ON_START_ACTIVITY);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(this.getClass().getSimpleName(), Constants.ON_RESUME_ACTIVITY);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(this.getClass().getSimpleName(), Constants.ON_PAUSE_ACTIVITY);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(this.getClass().getSimpleName(), Constants.ON_STOP_ACTIVITY);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(this.getClass().getSimpleName(), Constants.ON_RESTART_ACTIVITY);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(this.getClass().getSimpleName(), Constants.ON_DESTROY_ACTIVITY);
    }
}
