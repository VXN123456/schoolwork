package com.example.vien_pt.schoolwork.asyncTasks;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.activities.ProjectDetailActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.ProjectModel;

import java.io.IOException;
import java.util.Iterator;

/**
 * Created by Vien-PT on 06-Nov-17.
 */

public class DeleteProjectAsyncTask extends AsyncTask<String, Void, String>  {
    private String result;
    Context context;
    private int projectId;

    public DeleteProjectAsyncTask(Context context, int projectId) {
        this.context = context;
        this.projectId = projectId;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            result = Utils.makeRequest(Constants.POST_METHOD,params[0],null,"","");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result.equals("OK")){
            for (Iterator<ProjectModel> projectModelIterator = LoginActivity.profileProjects.iterator(); projectModelIterator.hasNext(); ) {
                if (projectModelIterator.next().getId() == projectId) {
                    projectModelIterator.remove();
                }
            }
            ((ProjectDetailActivity)context).finish();
        }else{
            Toast.makeText(context, "Delete project on server failed", Toast.LENGTH_SHORT).show();
        }
    }
}
