package com.example.vien_pt.schoolwork.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.adapters.CriterionAdapter;
import com.example.vien_pt.schoolwork.asyncTasks.CreateOrUpdateCriterionAsyncTask;
import com.example.vien_pt.schoolwork.base.BaseActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.DataProcessingManager;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.CriterionModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SettingCriteriaActivity extends BaseActivity {
    private boolean isChangedCriteriaSetting = false;

    public boolean isChangedCriteriaSetting() {
        return isChangedCriteriaSetting;
    }

    public void setChangedCriteriaSetting(boolean changedCriteriaSetting) {
        isChangedCriteriaSetting = changedCriteriaSetting;
    }


    ListView lvCriteria;
    Button btnAddNewCriterion;
    CriterionAdapter criterionAdapter;
    List<CriterionModel> profileCriteriaCommon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_criteria);
        //init View
        initView();

        profileCriteriaCommon = new ArrayList<>();
        //lay danh sach cac tieu chi chung cho cac project cua giao vien
        profileCriteriaCommon = DataProcessingManager.getProfileCriteriaCommon(LoginActivity.criteria, MainActivity.currentAccount.getId());
        // set criteria list to list view criteria
        criterionAdapter = new CriterionAdapter(SettingCriteriaActivity.this, R.layout.item_criterion, profileCriteriaCommon, true);
        lvCriteria.setAdapter(criterionAdapter);

        //add new criterion
        btnAddNewCriterion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialogAddCriterion = new Dialog(SettingCriteriaActivity.this);
                dialogAddCriterion.setContentView(R.layout.dialog_add_criterion);
                final EditText edtCriterionName = (EditText) dialogAddCriterion.findViewById(R.id.edtCriterionName);
                final EditText edtCriterionWeight = (EditText) dialogAddCriterion.findViewById(R.id.edtCriterionWeight);
                final EditText edtCriterionNote = (EditText) dialogAddCriterion.findViewById(R.id.edtCriterionNote);
                Button btnAddCriterion = (Button) dialogAddCriterion.findViewById(R.id.btnAddCriterion);
                btnAddCriterion.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (MainActivity.currentAccount.getRole().equals("Teacher")) {
                            CriterionModel criterionModel = new CriterionModel();
                            if (edtCriterionName.getText().toString().trim().equals("")) {
                                Toast.makeText(SettingCriteriaActivity.this, "Please enter criterion name", Toast.LENGTH_SHORT).show();
                            } else {
                                try {
                                    int value = Integer.parseInt(edtCriterionWeight.getText().toString());
                                    criterionModel.setId(0);
                                    criterionModel.setName(edtCriterionName.getText().toString().trim());
                                    criterionModel.setWeight(value);
                                    criterionModel.setNote(edtCriterionNote.getText().toString().trim());
                                    criterionModel.setScore(0);
                                    criterionModel.setProfileId(MainActivity.currentAccount.getId());
                                    criterionModel.setProjectId(0);
                                    profileCriteriaCommon.add(criterionModel);
                                    criterionAdapter.notifyDataSetChanged();
                                    setChangedCriteriaSetting(true);
                                    dialogAddCriterion.dismiss();
                                } catch (Exception e) {
                                    Toast.makeText(SettingCriteriaActivity.this, "Weight is an integer value!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else {
                            Toast.makeText(SettingCriteriaActivity.this, "This function for Teacher role!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                dialogAddCriterion.show();
            }
        });
    }

    private void initView() {
        setToolBar("Setting Criteria");
        setColorStatusBar(this);
        lvCriteria = (ListView) findViewById(R.id.lvCriteria);
        btnAddNewCriterion = (Button) findViewById(R.id.btnAddNewCriterion);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.criteria_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.saveCriteria:
                if (Utils.isConnectingToInternet(getApplicationContext())) {
                    if (profileCriteriaCommon != null && profileCriteriaCommon.size() > 0) {
                        JSONArray arrayCriteria = new JSONArray();
                        for (CriterionModel criterionModel : profileCriteriaCommon) {
                            JSONObject objectCriterion = new JSONObject();
                            try {
                                objectCriterion.put("id", criterionModel.getId());
                                objectCriterion.put("profileId", criterionModel.getProfileId());
                                objectCriterion.put("projectId", criterionModel.getProjectId());
                                objectCriterion.put("name", criterionModel.getName());
                                objectCriterion.put("weight", criterionModel.getWeight());
                                objectCriterion.put("score", criterionModel.getScore());
                                arrayCriteria.put(objectCriterion);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        new CreateOrUpdateCriterionAsyncTask(SettingCriteriaActivity.this
                                , arrayCriteria.toString()).execute(Constants.URL_ADD_OR_UPDATE_CRITERION_API);
                    }
                } else {
                    Toast.makeText(this, "You are currently offline!", Toast.LENGTH_SHORT).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (isChangedCriteriaSetting) {
            new AlertDialog.Builder(this, R.style.DialogThemMain)
                    .setMessage("Your changes will not be saved. Continue?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        }else{
            finish();
        }
    }
}
