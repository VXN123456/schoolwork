package com.example.vien_pt.schoolwork.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.CreateTaskActivity;
import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.adapters.TaskAdapter;
import com.example.vien_pt.schoolwork.asyncTasks.TaskProfileAsyncTask;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.DataProcessingManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vien-PT on 08-Oct-17.
 */

public class TasksFragment extends Fragment {
    ListView lvTasks;
    TaskAdapter taskAdapter;
    SwipeRefreshLayout swipeRefreshTaskList;
    ViewPager viewPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_tasks, container, false);

        // Setting ViewPager for each Tabs
        viewPager = (ViewPager) view.findViewById(R.id.vpTask);
        setupViewPager(viewPager);
        // Set Tabs inside Toolbar
        TabLayout tabsTask = (TabLayout) view.findViewById(R.id.tabsTask);
        tabsTask.setTabGravity(TabLayout.GRAVITY_FILL);

        tabsTask.setupWithViewPager(viewPager);

        return view;
    }

    private void setupViewPager(final ViewPager viewPager) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                TaskTabAdapter taskTabAdapter = new TaskTabAdapter(getChildFragmentManager());
                taskTabAdapter.addFragment(new OpenTasksFragment(), "Open Tasks");
                taskTabAdapter.addFragment(new CompletedTasksFragment(), "Completed Tasks");
                taskTabAdapter.addFragment(new RequestedTasksFragment(), "Requested Tasks");
                viewPager.setAdapter(taskTabAdapter);
            }
        }, 500);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageButton imbAddTask = (ImageButton) getView().findViewById(R.id.imbAddTask);
        imbAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), CreateTaskActivity.class));
            }
        });

//        lvTasks = (ListView) getView().findViewById(R.id.lvTasks);
//        taskAdapter = new TaskAdapter(getActivity(), R.layout.item_task, LoginActivity.profileTasks, null, false);
//        lvTasks.setAdapter(taskAdapter);
//
//
//        swipeRefreshTaskList = (SwipeRefreshLayout) getView().findViewById(R.id.swipeRefreshTaskList);
//        swipeRefreshTaskList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                //get projects
//                new TaskProfileAsyncTask(getActivity()).execute(Constants.URL_PROFILE_TASKS_API + String.valueOf(LoginActivity.accountId));
//                swipeRefreshTaskList.setRefreshing(false);
//                onResume();
//            }
//        });
    }

    @Override
    public void onResume() {
        super.onResume();
//        //set project name for task
//        DataProcessingManager.setRelationFieldsForTask(LoginActivity.profileTasks, LoginActivity.profileProjects, LoginActivity.profiles);
//        taskAdapter = new TaskAdapter(getActivity(), R.layout.item_task, LoginActivity.profileTasks, null,false);
//        lvTasks.setAdapter(taskAdapter);
//        taskAdapter.notifyDataSetChanged();
    }

    static class TaskTabAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public TaskTabAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
