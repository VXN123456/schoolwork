package com.example.vien_pt.schoolwork.asyncTasks;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;

import java.io.IOException;

/**
 * Created by Vien-PT on 17-Nov-17.
 */

public class DeleteTaskAsyncTask extends AsyncTask<String, Void, String> {
    String result;
    Context context;

    public DeleteTaskAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            result = Utils.makeRequest(Constants.POST_METHOD, params[0], null, "", "");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result.equals("Removed")) {
            Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Delete failed", Toast.LENGTH_SHORT).show();
        }
    }
}
