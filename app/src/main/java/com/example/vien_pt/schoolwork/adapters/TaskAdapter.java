package com.example.vien_pt.schoolwork.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.system.Os;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.EditTaskActivity;
import com.example.vien_pt.schoolwork.activities.MainActivity;
import com.example.vien_pt.schoolwork.asyncTasks.CreateOrUpdateTaskAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.DeleteTaskAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.UpdateTaskStatusAsyncTask;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.ProjectModel;
import com.example.vien_pt.schoolwork.models.TaskModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Vien-PT on 06-Nov-17.
 */

public class TaskAdapter extends ArrayAdapter<TaskModel> {
    private Context context;
    private ProjectModel projectModel;
    private boolean isInProject;
//    private TextView tvCreator;
//    private TextView tvProjectName;
//    private TextView tvTaskName;
//    private TextView tvTaskDes;
//    private TextView tvAssignee;
//    private Button btnChangeStatus;
//    private TextView tvDueDate;
//    private ImageView imvProject;
//    private ImageView imvDeleteTask;
//    private ImageView imvEditTask;
//    private LinearLayout lnlProject;

    public TaskAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<TaskModel> objects, ProjectModel model, boolean isInProject) {
        super(context, resource, objects);
        this.context = context;
        this.projectModel = model;
        this.isInProject = isInProject;
    }

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        final TaskModel taskModel = getItem(position);
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_task, null);
        }
//        ImageView imvCreator = (ImageView)view.findViewById(R.id.imvCreator) ;
        TextView tvCreator = (TextView) view.findViewById(R.id.tvCreator);
        TextView tvProjectName = (TextView) view.findViewById(R.id.tvProjectName);
//        TextView tvTaskId = (TextView)view.findViewById(R.id.tvTaskId);
        TextView tvTaskName = (TextView) view.findViewById(R.id.tvTaskName);
        TextView tvTaskDes = (TextView) view.findViewById(R.id.tvTaskDes);
        TextView tvAssignee = (TextView) view.findViewById(R.id.tvAssignee);
        final Button btnChangeStatus = (Button) view.findViewById(R.id.btnChangeStatus);
        TextView tvDueDate = (TextView) view.findViewById(R.id.tvDueDate);
        ImageView imvProject = (ImageView) view.findViewById(R.id.imvProject);
        ImageView imvDeleteTask = (ImageView) view.findViewById(R.id.imvDeleteTask);
        ImageView imvEditTask = (ImageView) view.findViewById(R.id.imvEditTask);
        LinearLayout lnlProject = (LinearLayout) view.findViewById(R.id.lnlProject);
        if (taskModel != null) {
            tvCreator.setText(taskModel.getCreator());
            if (projectModel != null) {
                tvProjectName.setText(projectModel.getName());
            } else {
                tvProjectName.setText(taskModel.getProjectName());
            }
            tvTaskName.setText(taskModel.getName());
            tvTaskDes.setText(taskModel.getDescription());
            tvAssignee.setText(taskModel.getAssigneeName());
            if (taskModel.getStatus().equals("Open")) {
                btnChangeStatus.setText("Done");
                btnChangeStatus.setBackgroundResource(R.drawable.bg_blue_border_radius_2);
                btnChangeStatus.setTextColor(context.getResources().getColor(R.color.colorAllWhite));
            } else {
                btnChangeStatus.setText("Open");
                btnChangeStatus.setBackgroundResource(R.drawable.bg_main_border_radius_2);
                btnChangeStatus.setTextColor(context.getResources().getColor(R.color.colorAllBlack));
            }
            tvDueDate.setText(taskModel.getDeadline());
        }
        //
        btnChangeStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isConnectingToInternet(context)) {
                    String status = null;
                    if (taskModel.getStatus().equals("Open")) {
                        status = "Completed";
                        btnChangeStatus.setText("Open");
                        btnChangeStatus.setBackgroundResource(R.drawable.bg_white_border_radius_2);
                        btnChangeStatus.setTextColor(context.getResources().getColor(R.color.colorAllBlack));
                    }
                    if (taskModel.getStatus().equals("Completed")) {
                        status = "Open";
                        btnChangeStatus.setText("Done");
                        btnChangeStatus.setBackgroundResource(R.drawable.bg_blue_border_radius_2);
                        btnChangeStatus.setTextColor(context.getResources().getColor(R.color.colorAllWhite));
                    }

                    UpdateTaskStatusAsyncTask updateTaskStatusAsyncTask = new UpdateTaskStatusAsyncTask(context);
                    updateTaskStatusAsyncTask.execute(Constants.URL_UPDATE_TASK_STATUS_API + String.valueOf(taskModel.getId()) + "/" + status);
                } else {
                    Toast.makeText(context, "No internet connection!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        //hide project name
        if (isInProject) {
            lnlProject.setVisibility(View.GONE);
        }
        // edit task
        imvEditTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentEditTask = new Intent(context, EditTaskActivity.class);
                intentEditTask.putExtra("task", taskModel);
                context.startActivity(intentEditTask);
            }
        });
        //delete task
        imvDeleteTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new android.support.v7.app.AlertDialog.Builder(context, R.style.DialogThemMain)
                        .setMessage("Do you really want to delete this task?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (taskModel != null && taskModel.getCreatedBy() == MainActivity.currentAccount.getId()) {
                                    DeleteTaskAsyncTask deleteTaskAsyncTask = new DeleteTaskAsyncTask(context);
                                    deleteTaskAsyncTask.execute(Constants.URL_DELETE_TASK_API + String.valueOf(taskModel.getId()));
                                    remove(taskModel);
                                    notifyDataSetChanged();

                                } else {
                                    Toast.makeText(context, "Permission Denied", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        return view;
    }
}
