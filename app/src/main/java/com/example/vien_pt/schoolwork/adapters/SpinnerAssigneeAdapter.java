package com.example.vien_pt.schoolwork.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.example.vien_pt.schoolwork.models.ProfileModel;
import com.example.vien_pt.schoolwork.models.ProjectModel;

import java.util.List;

/**
 * Created by Vien-PT on 11-Nov-17.
 */

public class SpinnerAssigneeAdapter extends ArrayAdapter<ProfileModel> implements SpinnerAdapter {
    Context context;
    List<ProfileModel> profileModels;

    public SpinnerAssigneeAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<ProfileModel> objects) {
        super(context, resource, objects);
        this.context = context;
        this.profileModels = objects;
    }
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txt = new TextView(context);
        txt.setPadding(16, 16, 16, 16);
        txt.setTextSize(18);
        txt.setGravity(Gravity.CENTER_VERTICAL);
        txt.setText(profileModels.get(position).getFullName());
        txt.setTextColor(Color.parseColor("#000000"));
        return  txt;
    }

    public View getView(int i, View view, ViewGroup viewgroup) {
        TextView txt = new TextView(context);
        txt.setGravity(Gravity.START);
        txt.setPadding(16, 16, 16, 16);
        txt.setTextSize(16);
        txt.setText(profileModels.get(i).getFullName());
        txt.setTextColor(Color.parseColor("#000000"));
        return  txt;
    }
}
