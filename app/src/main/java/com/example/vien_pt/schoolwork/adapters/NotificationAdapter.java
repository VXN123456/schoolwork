package com.example.vien_pt.schoolwork.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.common.DataProcessingManager;
import com.example.vien_pt.schoolwork.models.NotificationModel;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by Vien-PT on 03-Dec-17.
 */

public class NotificationAdapter extends ArrayAdapter<NotificationModel> {
    Context context;

    public NotificationAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<NotificationModel> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        NotificationModel notificationModel = getItem(position);
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_notification, null);
        }
        TextView tvNotificationContent = (TextView) view.findViewById(R.id.tvNotificationContent);
        TextView tvNotificationTime = (TextView) view.findViewById(R.id.tvNotificationTime);
        RelativeLayout rlNotificationItem = (RelativeLayout)view.findViewById(R.id.rlNotificationItem);

        if (notificationModel != null) {
            tvNotificationContent.setText(
                    DataProcessingManager.getProfileNameByProfileId(LoginActivity.profiles, notificationModel.getNoticeFrom())
                            + " " + notificationModel.getContent()
            );
            long noticeTime = notificationModel.getCreatedAt()/1000*1000;

            tvNotificationTime.setText(String.valueOf(new Timestamp(noticeTime)));

            if (notificationModel.getFlagRead()==1){
                rlNotificationItem.setBackgroundResource(R.drawable.bg_read);
            }else{
                rlNotificationItem.setBackgroundResource(R.drawable.bg_unread);
            }
        }
        return view;
    }
}
