package com.example.vien_pt.schoolwork.models;

import java.io.Serializable;

/**
 * Created by Vien-PT on 31-Oct-17.
 */

public class ProjectModel implements Serializable {

    private int id;
    private String name;
    private String rating;
    private double score;
    private String code;
    private int createdBy;
    private String description;
    private String requirement;
    private String keyword;
    private String reference;
    private String createdAt;
    private String updatedAt;
    private String timeStart;
    private String timeEnd;

    private int mainGuide;


    public Integer[] getMembers() {
        return members;
    }

    public void setMembers(Integer[] members) {
        this.members = members;
    }

    private Integer[] members;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int profileId) {
        this.createdBy = profileId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }


    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }


    public int getMainGuide() {
        return mainGuide;
    }

    public void setMainGuide(int mainGuide) {
        this.mainGuide = mainGuide;
    }

}
