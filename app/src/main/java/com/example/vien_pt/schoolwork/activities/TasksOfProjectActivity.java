package com.example.vien_pt.schoolwork.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ListView;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.adapters.TaskAdapter;
import com.example.vien_pt.schoolwork.asyncTasks.TaskProfileAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.TaskProjectAsyncTask;
import com.example.vien_pt.schoolwork.base.BaseActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.DataProcessingManager;
import com.example.vien_pt.schoolwork.models.ProjectModel;
import com.example.vien_pt.schoolwork.models.TaskModel;

import java.util.List;

public class TasksOfProjectActivity extends BaseActivity {
    private ProjectModel projectModel;
    private List<TaskModel> taskModels;
    ListView lvTasks;
    FloatingActionButton btnCreateTask;
    TaskAdapter taskAdapter;
    SwipeRefreshLayout swipeRefreshTaskList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks_of_project);

        //init view
        initView();
        //get data project model
        getDataProjectModel();
        setToolBar(projectModel.getName());
        // set color status bar
        setColorStatusBar(this);

        taskAdapter = new TaskAdapter(TasksOfProjectActivity.this, R.layout.item_task, getTaskModels(), projectModel, true);
        lvTasks.setAdapter(taskAdapter);
        btnCreateTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TasksOfProjectActivity.this, CreateTaskActivity.class).putExtra("project", projectModel));
            }
        });
        swipeRefreshTaskList = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshTaskList);
        swipeRefreshTaskList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new TaskProjectAsyncTask(TasksOfProjectActivity.this, projectModel, false).execute(Constants.URL_PROFILE_TASKS_API + String.valueOf(projectModel.getId()));
                swipeRefreshTaskList.setRefreshing(false);
                onResume();
            }
        });
    }

    private void initView() {
        lvTasks = (ListView) findViewById(R.id.lvTasks);
        btnCreateTask = (FloatingActionButton) findViewById(R.id.btnCreateTask);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDataProjectModel();
        taskAdapter = new TaskAdapter(TasksOfProjectActivity.this, R.layout.item_task, getTaskModels(), projectModel, true);
        lvTasks.setAdapter(taskAdapter);
        taskAdapter.notifyDataSetChanged();
    }

    private void getDataProjectModel() {
        Intent intent = getIntent();
        projectModel = (ProjectModel) intent.getSerializableExtra("project");
//        taskModels = (List<TaskModel>) intent.getSerializableExtra("tasks");
        taskModels = ProjectDetailActivity.projectTasks;
        DataProcessingManager.setRelationFieldsForTask(taskModels, LoginActivity.profileProjects, LoginActivity.profiles);
    }

    public List<TaskModel> getTaskModels() {
        return taskModels;
    }

    public void setTaskModels(List<TaskModel> taskModels) {
        this.taskModels = taskModels;
    }
}
