package com.example.vien_pt.schoolwork.asyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.MainActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.ClassificationModel;
import com.example.vien_pt.schoolwork.models.ClassificationModel;
import com.example.vien_pt.schoolwork.models.ProfileModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vien-PT on 02-Dec-17.
 */

public class ClassificationAsyncTask extends AsyncTask<String, Void, List<ClassificationModel>> {
    Context context;
    ProgressDialog progressDialog;
    public ClassificationAsyncTask(Context context) {
        this.context = context;
        this.progressDialog = new ProgressDialog(context, R.style.DialogThemMain);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog.setMessage("Loading, please wait...");
        progressDialog.show();
    }

    @Override
    protected List<ClassificationModel> doInBackground(String... params) {
        String jsonFinal = null;
        try {
            jsonFinal = Utils.makeRequest(Constants.GET_METHOD, params[0], null, Constants.CONTENT_TYPE_JSON, "");
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONArray jsonArrayParent = null;
        try {
            jsonArrayParent = new JSONArray(jsonFinal);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        List<ClassificationModel> classificationModelList = new ArrayList<>();
        if (jsonArrayParent != null) {
            for (int i = 0; i < jsonArrayParent.length(); i++) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = jsonArrayParent.getJSONObject(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    ClassificationModel classificationModel = new ClassificationModel();
                    classificationModel.setId(jsonObject.getInt("id"));
                    classificationModel.setProfileId(jsonObject.getInt("profileId"));
                    classificationModel.setName(jsonObject.getString("name"));
                    classificationModel.setFromScore(jsonObject.getDouble("fromScore"));
                    classificationModel.setToScore(jsonObject.getDouble("toScore"));

                    classificationModelList.add(classificationModel);
                } catch (JSONException ignored) {
                }
            }
        }
        return classificationModelList;
    }

    @Override
    protected void onPostExecute(List<ClassificationModel> classificationModels) {
        super.onPostExecute(classificationModels);
        MainActivity.classifications = classificationModels;
        progressDialog.dismiss();
    }
}
