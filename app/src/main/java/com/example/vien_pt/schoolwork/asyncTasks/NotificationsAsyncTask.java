package com.example.vien_pt.schoolwork.asyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.MainActivity;
import com.example.vien_pt.schoolwork.adapters.ContactAdapter;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.NotificationModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Vien-PT on 03-Dec-17.
 */

public class NotificationsAsyncTask extends AsyncTask<String, Void, List<NotificationModel>> {
    Context context;
    ProgressDialog progressDialog;
    public NotificationsAsyncTask(Context context) {
        this.context = context;
        this.progressDialog = new ProgressDialog(context, R.style.DialogThemMain);
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setMessage("Processing...");
        progressDialog.show();
    }

    @Override
    protected List<NotificationModel> doInBackground(String... params) {
        String jsonFinal = null;
        try {
            jsonFinal = Utils.makeRequest(Constants.GET_METHOD, params[0], null, Constants.CONTENT_TYPE_JSON, "");
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONArray jsonArrayParent = null;
        try {
            jsonArrayParent = new JSONArray(jsonFinal);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        List<NotificationModel> notificationModelList = new ArrayList<>();
        if (jsonArrayParent != null) {
            for (int i = 0; i < jsonArrayParent.length(); i++) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = jsonArrayParent.getJSONObject(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {NotificationModel notification = new NotificationModel();
                    notification.setId(jsonObject.getInt("id"));
                    notification.setNoticeFrom(jsonObject.getInt("noticeFrom"));
                    notification.setNoticeTo(jsonObject.getInt("noticeTo"));
                    notification.setContent(jsonObject.getString("content"));
                    notification.setProjectId(jsonObject.getInt("projectId"));
                    notification.setMeetingId(jsonObject.getInt("meetingId"));
                    notification.setTaskId(jsonObject.getInt("taskId"));
                    notification.setCreatedAt(jsonObject.getLong("createdAt"));
                    notification.setFlagRead(jsonObject.getInt("flagRead"));
                    notificationModelList.add(notification);
                } catch (JSONException ignored) {
                }
            }
        }

        return notificationModelList;
    }

    @Override
    protected void onPostExecute(List<NotificationModel> notificationModels) {
        super.onPostExecute(notificationModels);
        Collections.reverse(notificationModels);
        MainActivity.profileNotifications = notificationModels;
        progressDialog.dismiss();
    }
}
