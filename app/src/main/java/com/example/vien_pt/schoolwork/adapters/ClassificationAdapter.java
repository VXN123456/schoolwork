package com.example.vien_pt.schoolwork.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.activities.MainActivity;
import com.example.vien_pt.schoolwork.activities.SettingClassificationsActivity;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.ClassificationModel;

import java.util.List;

/**
 * Created by Vien-PT on 02-Dec-17.
 */

public class ClassificationAdapter extends ArrayAdapter<ClassificationModel> {
    Context context;

    public ClassificationAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<ClassificationModel> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        final ClassificationModel classificationModel = getItem(position);
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_classification, null);
        }
        TextView tvClassificationName = (TextView) view.findViewById(R.id.tvClassificationName);
        TextView tvSettingFromScore = (TextView) view.findViewById(R.id.tvSettingFromScore);
        TextView tvSettingToScore = (TextView) view.findViewById(R.id.tvSettingToScore);
        ImageView imvEditClassification = (ImageView) view.findViewById(R.id.imvEditClassification);
        ImageView imvDeleteClassification = (ImageView) view.findViewById(R.id.imvDeleteClassification);

        if (classificationModel != null) {
            tvClassificationName.setText(classificationModel.getName());
            tvSettingFromScore.setText(String.valueOf(classificationModel.getFromScore()));
            tvSettingToScore.setText(String.valueOf(classificationModel.getToScore()));
        }

        //on click edit
        imvEditClassification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LoginActivity.accountId == classificationModel.getProfileId()) {
                    final Dialog dialogEditClassification = new Dialog(context);
                    dialogEditClassification.setContentView(R.layout.dialog_edit_classification);
                    TextView tvTitle = (TextView) dialogEditClassification.findViewById(R.id.tvTitle);
                    final EditText edtClassificationName = (EditText) dialogEditClassification.findViewById(R.id.edtClassificationName);
                    final EditText edtFromScore = (EditText) dialogEditClassification.findViewById(R.id.edtFromScore);
                    final EditText edtToScore = (EditText) dialogEditClassification.findViewById(R.id.edtToScore);
                    Button btnUpdate = (Button) dialogEditClassification.findViewById(R.id.btnSave);

                    tvTitle.setText("Edit Classification");
                    btnUpdate.setText("Update");
                    if (classificationModel != null) {
                        edtClassificationName.setText(classificationModel.getName());
                        edtFromScore.setText(String.valueOf(classificationModel.getFromScore()));
                        edtToScore.setText(String.valueOf(classificationModel.getToScore()));
                    }
                    btnUpdate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isConnectingToInternet(context)) {
                                if (edtClassificationName.getText().toString().trim().equals("")) {
                                    Toast.makeText(context, "Please enter classification name", Toast.LENGTH_SHORT).show();
                                } else {
                                    try {
                                        double fromScore = Double.parseDouble(edtFromScore.getText().toString());
                                        double toScore = Double.parseDouble(edtToScore.getText().toString());
                                        if (fromScore>=toScore){
                                            Toast.makeText(context, "FROM must less than TO", Toast.LENGTH_SHORT).show();
                                        }else{
                                            classificationModel.setName(edtClassificationName.getText().toString());
                                            classificationModel.setFromScore(fromScore);
                                            classificationModel.setToScore(toScore);
                                            ((SettingClassificationsActivity) context).setChangedClassificationSetting(true);
                                            dialogEditClassification.dismiss();
                                        }
                                    } catch (Exception e) {
                                        Toast.makeText(context, "Edit classification thrown the exception!", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            } else {
                                Toast.makeText(context, "Can not connect to internet!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    dialogEditClassification.show();
                } else {
                    Toast.makeText(context, "Permission Denied", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //on click delete
        imvDeleteClassification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new android.support.v7.app.AlertDialog.Builder(context, R.style.DialogThemMain)
                        .setMessage("Do you really want to delete this classification?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (MainActivity.currentAccount.getRole().equals("Teacher") && MainActivity.currentAccount.getId() == classificationModel.getProfileId()) {
                                    remove(classificationModel);
                                    notifyDataSetChanged();
                                    ((SettingClassificationsActivity) context).setChangedClassificationSetting(true);
                                } else {
                                    Toast.makeText(context, "Permission Denied", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        return view;
    }
}
