package com.example.vien_pt.schoolwork.asyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.activities.MainActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;
import com.example.vien_pt.schoolwork.models.ProfileModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vien-PT on 01-Nov-17.
 */

public class ContactAsyncTask extends AsyncTask<String, Void, List<ProfileModel>> {
    Context context;
    private ProgressDialog progressDialog;

    public ContactAsyncTask(Context context) {
        this.context = context;
        progressDialog = new ProgressDialog(context, R.style.DialogThemMain);
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setMessage("Loading, please wait...");
        progressDialog.show();
    }

    @Override
    protected List<ProfileModel> doInBackground(String... params) {
        String jsonFinal = null;
        try {
            jsonFinal = Utils.makeRequest(Constants.GET_METHOD, params[0], null, Constants.CONTENT_TYPE_JSON, Constants.EMPTY);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONArray jsonArrayParent = null;
        try {
            jsonArrayParent = new JSONArray(jsonFinal);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        List<ProfileModel> profileModelList = new ArrayList<>();
        if (jsonArrayParent != null) {
            for (int i = 0; i < jsonArrayParent.length(); i++) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = jsonArrayParent.getJSONObject(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    ProfileModel profileModel = new ProfileModel();
                    profileModel.setId(jsonObject.getInt("id"));
                    profileModel.setEmail(jsonObject.getString("email"));
                    profileModel.setPassword(jsonObject.getString("password"));
                    profileModel.setFullName(jsonObject.getString("fullName"));
                    profileModel.setCodeId(jsonObject.getString("codeId"));
                    profileModel.setPhoneNumber(jsonObject.getString("phoneNumber"));
                    profileModel.setStudyStart(jsonObject.getString("studyStart"));
                    profileModel.setStudyEnd(jsonObject.getString("studyEnd"));
                    profileModel.setPicture(jsonObject.getString("picture"));
                    profileModel.setRole(jsonObject.getString("role"));
                    profileModel.setCurrentSemester(jsonObject.getInt("currentSemester"));

                    profileModelList.add(profileModel);

                } catch (JSONException ignored) {
                }
            }
        }
        return profileModelList;

    }

    @Override
    protected void onPostExecute(List<ProfileModel> profileModels) {
        super.onPostExecute(profileModels);
        MainActivity.contactList = profileModels;
        LoginActivity.isLoadedContacts = true;
        progressDialog.dismiss();
    }
}
