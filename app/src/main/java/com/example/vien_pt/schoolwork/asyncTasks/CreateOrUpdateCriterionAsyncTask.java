package com.example.vien_pt.schoolwork.asyncTasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.CreateTaskActivity;
import com.example.vien_pt.schoolwork.activities.EditTaskActivity;
import com.example.vien_pt.schoolwork.activities.ProjectEvaluationActivity;
import com.example.vien_pt.schoolwork.activities.SettingCriteriaActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;

import java.io.IOException;

/**
 * Created by Vien-PT on 03-Nov-17.
 */

public class CreateOrUpdateCriterionAsyncTask extends AsyncTask<String, Void, String> {
    private Context context;
    private String jsonRequestEntity;
    private String result;
    ProgressDialog progressDialog;

    public CreateOrUpdateCriterionAsyncTask(Context context, String jsonString) {
        this.context = context;
        this.jsonRequestEntity = jsonString;
        this.progressDialog = new ProgressDialog(context, R.style.DialogThemMain);
    }

    @Override
    protected void onPreExecute() {
        progressDialog.setMessage("Processing...");
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            result = Utils.makeRequest(Constants.POST_METHOD, params[0], null, Constants.CONTENT_TYPE_JSON, this.jsonRequestEntity);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (result.equals("Removed it in others")
                || result.equals("Created setting")
                || result.equals("Updated setting")) {
            Toast.makeText(context, "Updated setting criteria!", Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();
            ((Activity) context).finish();
        } else if (result.equals("Updated")) {
            Toast.makeText(context, "Updated evaluate!", Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();
            ((Activity) context).finish();
        } else if (result.equals("Created First Time")) {
            Toast.makeText(context, "Evaluated First Time!", Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();
            ((Activity) context).finish();
        } else if (result.equals("")) {
            Toast.makeText(context, "Can not update to database!", Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();
        } else {
            Toast.makeText(context, "Cannot connect to server!", Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();
        }
    }
}
