package com.example.vien_pt.schoolwork.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.method.LinkMovementMethod;
import android.util.Patterns;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.asyncTasks.SignUpAsyncTask;
import com.example.vien_pt.schoolwork.base.BaseActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.common.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SignUpActivity extends BaseActivity {

    AutoCompleteTextView inputFullName;
    AutoCompleteTextView inputEmail;
    AutoCompleteTextView inputPassword;
    AutoCompleteTextView inputPasswordConfirm;
    RadioGroup rgRole;
    Button btnSignUp;
    String role;
    TextView goLogin;

    //defining AwesomeValidation object
    private AwesomeValidation awesomeValidation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        //set color status bar
        setColorStatusBar(this);

        initView();


        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        //adding validation to edit texts
        awesomeValidation.addValidation(this, R.id.inputEmail, Patterns.EMAIL_ADDRESS, R.string.email_error);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.isConnectingToInternet(getApplicationContext())){
                    if (awesomeValidation.validate()) {
                        if (!inputPassword.getText().toString().equals(inputPasswordConfirm.getText().toString())) {
                            Toast.makeText(SignUpActivity.this, "Password not match!", Toast.LENGTH_SHORT).show();
                        } else if (inputPassword.getText().length() < 6) {
                            Toast.makeText(SignUpActivity.this, "Password at least 6 character!", Toast.LENGTH_SHORT).show();
                        } else {
                            JSONObject profileObject = new JSONObject();
                            try {
                                profileObject.put("id", 0);
                                profileObject.put("email", inputEmail.getText().toString());
                                profileObject.put("password", inputPassword.getText().toString());
                                profileObject.put("codeId", "");
                                profileObject.put("phoneNumber", "");
                                profileObject.put("fullName", inputFullName.getText().toString());
                                String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                                profileObject.put("studyStart", date);
                                profileObject.put("studyEnd", date);
                                profileObject.put("picture","" );

                                final int idChecked = rgRole.getCheckedRadioButtonId();
                                if (rgRole.getCheckedRadioButtonId() != -1) {
                                    RadioButton rbRole = (RadioButton) findViewById(idChecked);
                                    role = rbRole.getText().toString();
                                }
                                profileObject.put("role", role);
                                profileObject.put("currentSemester", 0);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            new SignUpAsyncTask(SignUpActivity.this, profileObject.toString(),null).execute(Constants.URL_ADD_OR_UPDATE_PROFILE_API);

                        }
                    }
                }else{
                    Toast.makeText(SignUpActivity.this, "You are currently offline!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //init view
    public void initView() {
        inputFullName = (AutoCompleteTextView) findViewById(R.id.inputFullName);
        inputEmail = (AutoCompleteTextView) findViewById(R.id.inputEmail);
        inputPassword = (AutoCompleteTextView) findViewById(R.id.inputPassword);
        inputPasswordConfirm = (AutoCompleteTextView) findViewById(R.id.inputPasswordConfirm);
        rgRole = (RadioGroup) findViewById(R.id.rgRole);
        btnSignUp = (Button) findViewById(R.id.btnSignUp);
        goLogin = (TextView)findViewById(R.id.tvGoLogin) ;

        goLogin.setText(R.string.already_have_an_account_sign_in);
    }

    public void signIn(View view) {
        Intent intentLogin = new Intent(SignUpActivity.this, LoginActivity.class);
        startActivity(intentLogin);
        this.finish();
    }
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this,R.style.DialogThemMain)
                .setMessage("Do you want to close app?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        moveTaskToBack(true);
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }
}
