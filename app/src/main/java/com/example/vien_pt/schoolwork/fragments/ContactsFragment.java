package com.example.vien_pt.schoolwork.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.ContactDetailActivity;
import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.activities.MainActivity;
import com.example.vien_pt.schoolwork.adapters.ContactAdapter;
import com.example.vien_pt.schoolwork.asyncTasks.AddContactAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.ContactAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.DeleteContactAsyncTask;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.models.ProfileModel;

/**
 * Created by Vien-PT on 08-Oct-17.
 */

public class ContactsFragment extends Fragment {
    ListView lvContact;
    ImageButton imbAddContact;
    SwipeRefreshLayout swipeRefreshContactList;
    ContactAdapter contactAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contacts, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        imbAddContact = (ImageButton) getView().findViewById(R.id.imbAddContact);
        imbAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialogAddContact = new Dialog(getActivity());
                dialogAddContact.setContentView(R.layout.dialog_add_contact);
                final AutoCompleteTextView inputEmailFriend = (AutoCompleteTextView) dialogAddContact.findViewById(R.id.inputEmailFriend);

                Button btnAddContact = (Button) dialogAddContact.findViewById(R.id.btnAddContact);
                btnAddContact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (inputEmailFriend.getText().toString().equals(Constants.EMPTY)) {
                            Toast.makeText(getActivity(), "Please enter email!", Toast.LENGTH_SHORT).show();
                        } else {
                            AddContactAsyncTask addContactAsyncTask = new AddContactAsyncTask(getActivity());
                            addContactAsyncTask.execute(Constants.URL_CREATE_CONTACTS_API + inputEmailFriend.getText().toString() + "/" + String.valueOf(LoginActivity.accountId));
                            dialogAddContact.dismiss();
                        }
                    }
                });
                dialogAddContact.show();
            }
        });
        MainActivity.isContactToAdd = false;
        //list project
        lvContact = (ListView) getView().findViewById(R.id.lvContacts);
        contactAdapter = new ContactAdapter(getActivity(), R.layout.item_contact, MainActivity.contactList, false);
        lvContact.setAdapter(contactAdapter);
        //refresh listview project by pull down
        swipeRefreshContactList = (SwipeRefreshLayout) getView().findViewById(R.id.swipeRefreshContactList);
        swipeRefreshContactList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //get contacts
                new ContactAsyncTask(getActivity()).execute(Constants.URL_GET_CONTACTS_API + String.valueOf(LoginActivity.accountId));
                swipeRefreshContactList.setRefreshing(false);
                onResume();
            }
        });
        //row short click
        lvContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ProfileModel profileModel = (ProfileModel) lvContact.getItemAtPosition(position);
                Intent intentContactDetail = new Intent(getActivity(), ContactDetailActivity.class);
                intentContactDetail.putExtra("profile", profileModel);
                startActivity(intentContactDetail);
            }
        });
        //long click to edit or remove contact
        lvContact.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> parent, View view, final int position, long id) {
                final ProfileModel profileModel = (ProfileModel) lvContact.getItemAtPosition(position);
                final Dialog dialogContact = new Dialog(getActivity());
                dialogContact.setContentView(R.layout.dialog_delete_contact);
                Button btnDeleteContact = (Button) dialogContact.findViewById(R.id.btnDeleteContact);
                btnDeleteContact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.DialogThemMain);
                        builder.setMessage("Do you really want to delete " + profileModel.getFullName() + " ?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        new DeleteContactAsyncTask(getActivity()).execute(Constants.URL_DELETE_CONTACT_API + profileModel.getEmail() + "/" + String.valueOf(LoginActivity.accountId));
                                        MainActivity.contactList.remove(position);
                                        contactAdapter.notifyDataSetChanged();
                                        dialogContact.dismiss();
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        dialogContact.dismiss();
                                    }
                                }).show();

                    }
                });
                dialogContact.show();
                return true;
            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.isContactToAdd = false;
        contactAdapter = new ContactAdapter(getActivity(), R.layout.item_contact, MainActivity.contactList,false);
        lvContact.setAdapter(contactAdapter);
        contactAdapter.notifyDataSetChanged();
    }

}
