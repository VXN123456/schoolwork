package com.example.vien_pt.schoolwork.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.adapters.ContactAdapter;
import com.example.vien_pt.schoolwork.base.BaseActivity;
import com.example.vien_pt.schoolwork.models.ProfileModel;
import com.example.vien_pt.schoolwork.models.ProjectModel;

import java.util.ArrayList;
import java.util.List;

public class MemberProjectActivity extends BaseActivity {
    ProjectModel projectModel;
    ListView lvMember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_project);

        // Set a toolbar to replace the action bar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Members");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //set color status bar
        setColorStatusBar(this);

        MainActivity.isContactToAdd = false;

        lvMember = (ListView) findViewById(R.id.lvMember);
        Intent intent = getIntent();
        projectModel = (ProjectModel) intent.getSerializableExtra("project");
        // get members joined project
        Integer[] memberIds = projectModel.getMembers();
        List<ProfileModel> members = new ArrayList<>();
        for (int id = 0; id < memberIds.length; id++) {
            if (memberIds[id]==LoginActivity.accountId){
                members.add(MainActivity.currentAccount);//add me
            }else{
                for (ProfileModel profileModel : MainActivity.contactList) {
                    if (profileModel.getId() == memberIds[id]) {
                        members.add(profileModel);
                        break;
                    }
                }
            }
        }
        MainActivity.isContactToAdd = false;
        if (members!=null && members.size()>0){
            ContactAdapter contactAdapter = new ContactAdapter(MemberProjectActivity.this, R.layout.item_contact, members, false);
            lvMember.setAdapter(contactAdapter);
        }

        lvMember.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ProfileModel profileModel = (ProfileModel) lvMember.getItemAtPosition(position);
                Intent intentContactDetail = new Intent(MemberProjectActivity.this, ContactDetailActivity.class);
                intentContactDetail.putExtra("profile", profileModel);
                startActivity(intentContactDetail);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
