package com.example.vien_pt.schoolwork.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.activities.CreateProjectActivity;
import com.example.vien_pt.schoolwork.activities.LoginActivity;
import com.example.vien_pt.schoolwork.activities.MainActivity;
import com.example.vien_pt.schoolwork.activities.ProjectDetailActivity;
import com.example.vien_pt.schoolwork.activities.ProjectNotificationActivity;
import com.example.vien_pt.schoolwork.adapters.ProjectAdapter;
import com.example.vien_pt.schoolwork.asyncTasks.ClassificationAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.NotificationsAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.ProjectControlAsyncTask;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.models.ProjectModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vien-PT on 31-Oct-17.
 */

public class ProjectsFragment extends Fragment {
    //    private Spinner spnSemester;
    private ListView lvProject;
    private ImageButton imbAddProject;
    private ImageButton imbProjectNotification;
    private SwipeRefreshLayout swipeRefreshProjectList;
    private ProjectAdapter projectAdapter;
    private List<ProjectModel> projectModels;
    private TextView tvUnreadNotifications;
    private LinearLayout lnlUnreadNotifications;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("project", Constants.ON_CREATE_VIEW_FRAGMENT);
        return inflater.inflate(R.layout.fragment_project, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("project", Constants.ON_VIEW_CREATED_FRAGMENT);
//        spnSemester = (Spinner) getView().findViewById(R.id.spnSemester);
        //init view
        initView();

//        List<String> list = new ArrayList<>();
//        list.add("All");
//        list.add("HK1(2015-2016)");
//        list.add("HK2(2015-2016)");
//        list.add("HK1(2016-2017)");
//        list.add("HK2(2016-2017)");
//        list.add("HK1(2017-2018)");
//        list.add("HK2(2017-2018)");
//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, list);
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spnSemester.setAdapter(dataAdapter);

        projectModels = new ArrayList<>();
        projectModels = LoginActivity.profileProjects;

        //click create project
        imbAddProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check role to create project
                if (MainActivity.currentAccount.getRole().equals("Teacher")) {
                    Intent intentCreateProject = new Intent(getActivity(), CreateProjectActivity.class);
                    startActivity(intentCreateProject);
                } else {
                    Toast.makeText(getActivity(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
            }
        });
        //show project notification
        imbProjectNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentNotification = new Intent(getActivity(), ProjectNotificationActivity.class);
                startActivity(intentNotification);
            }
        });

        //list project
        lvProject = (ListView) getView().findViewById(R.id.lvProject);
        if (LoginActivity.profileProjects != null && LoginActivity.profileProjects.size() > 0) {
            projectAdapter = new ProjectAdapter(getActivity(), R.layout.item_project, LoginActivity.profileProjects);
            lvProject.setAdapter(projectAdapter);
        }

        //click on item project
        lvProject.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ProjectModel projectModel = (ProjectModel) lvProject.getItemAtPosition(position);
                Intent intentProjectDetail = new Intent(getActivity(), ProjectDetailActivity.class);
                intentProjectDetail.putExtra("project", projectModel);
                startActivity(intentProjectDetail);
            }
        });

        //refresh listview project by pull down
        swipeRefreshProjectList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                swipeRefreshProjectList.setRefreshing(false);
                onResume();
            }
        });
    }

    private void initView() {
        imbAddProject = (ImageButton) getView().findViewById(R.id.imbAddProject);
        imbProjectNotification = (ImageButton) getView().findViewById(R.id.imbProjectNotification);
        swipeRefreshProjectList = (SwipeRefreshLayout) getView().findViewById(R.id.swipeRefreshProjectList);
        lnlUnreadNotifications = (LinearLayout)getView().findViewById(R.id.lnlUnreadNotifications);
        tvUnreadNotifications = (TextView)getView().findViewById(R.id.tvUnreadNotifications);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("project", Constants.ON_ATTACH_FRAGMENT);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("project", Constants.ON_CREATE_FRAGMENT);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("project", Constants.ON_ACTIVITY_CREATED_FRAGMENT);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("project", Constants.ON_START_FRAGMENT);
    }

    @Override
    public void onResume() {
        super.onResume();
        //get projects
        new ProjectControlAsyncTask(getActivity()).execute(Constants.URL_GET_PROFILE_PROJECTS_API + String.valueOf(LoginActivity.accountId));
        //load classifications
        new ClassificationAsyncTask(getActivity()).execute(Constants.URL_GET_CLASSIFICATIONS_API);
        //load notifications
        new NotificationsAsyncTask(getActivity()).execute(Constants.URL_GET_PROFILE_NOTIFICATIONS_API + String.valueOf(LoginActivity.accountId));
        //refresh listview project
        projectAdapter = new ProjectAdapter(getActivity(), R.layout.item_project, LoginActivity.profileProjects);
        lvProject.setAdapter(projectAdapter);
        projectAdapter.notifyDataSetChanged();
        Log.d("project", Constants.ON_RESUME_FRAGMENT);

        if (((MainActivity)getActivity()).getNumberUnreadNotifications(MainActivity.profileNotifications)==0){
            lnlUnreadNotifications.setVisibility(View.GONE);
        }else{
            lnlUnreadNotifications.setVisibility(View.VISIBLE);
            this.setUnreadNotifications(((MainActivity)getActivity()).getNumberUnreadNotifications(MainActivity.profileNotifications));
        }
    }


    //set unread notifications
    public void setUnreadNotifications(int unreadNotices){
        tvUnreadNotifications.setText(String.valueOf(unreadNotices));
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("project", Constants.ON_PAUSE_FRAGMENT);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("project", Constants.ON_STOP_FRAGMENT);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("project", Constants.ON_DESTROY_VIEW_FRAGMENT);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("project", Constants.ON_DESTROY_FRAGMENT);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("project", Constants.ON_DETACH_FRAGMENT);
    }

}
