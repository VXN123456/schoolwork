package com.example.vien_pt.schoolwork.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.example.vien_pt.schoolwork.R;
import com.example.vien_pt.schoolwork.adapters.NotificationAdapter;
import com.example.vien_pt.schoolwork.asyncTasks.CreateOrUpdateCriterionAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.DeleteContactAsyncTask;
import com.example.vien_pt.schoolwork.asyncTasks.UpdateNotificationsAsyncTask;
import com.example.vien_pt.schoolwork.base.BaseActivity;
import com.example.vien_pt.schoolwork.common.Constants;
import com.example.vien_pt.schoolwork.models.CriterionModel;
import com.example.vien_pt.schoolwork.models.NotificationModel;
import com.example.vien_pt.schoolwork.models.ProfileModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;
import java.util.Collections;

public class ProjectNotificationActivity extends BaseActivity {
    ListView lvNotifications;
    NotificationAdapter notificationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_notification);
        //init view
        initView();

        lvNotifications.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NotificationModel notificationModel = (NotificationModel) lvNotifications.getItemAtPosition(position);
                if (notificationModel.getFlagRead()==0){
                    notificationModel.setFlagRead(1);
                    RelativeLayout rlNotificationItem = (RelativeLayout)view.findViewById(R.id.rlNotificationItem);
                    rlNotificationItem.setBackgroundResource(R.drawable.bg_read);
                }
            }
        });
        lvNotifications.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> parent, final View view, final int position, long id) {
                final NotificationModel notificationModel = (NotificationModel) lvNotifications.getItemAtPosition(position);
                final Dialog dialogUnread = new Dialog(ProjectNotificationActivity.this);
                dialogUnread.setContentView(R.layout.dialog_unread);
                Button btnUnread = (Button) dialogUnread.findViewById(R.id.btnUnread);
                btnUnread.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (notificationModel.getFlagRead()==1) {
                            notificationModel.setFlagRead(0);
                            RelativeLayout rlNotificationItem = (RelativeLayout)view.findViewById(R.id.rlNotificationItem);
                            rlNotificationItem.setBackgroundResource(R.drawable.bg_unread);
                            dialogUnread.dismiss();
                        }else{
                            dialogUnread.dismiss();
                        }
                    }
                });
                dialogUnread.show();
                return true;
            }
        });

    }

    private void initView() {
        setToolBar("Project notifications");
        setColorStatusBar(this);
        lvNotifications = (ListView)findViewById(R.id.lvNotifications);
        notificationAdapter = new NotificationAdapter(this,R.layout.item_notification, MainActivity.profileNotifications);
        lvNotifications.setAdapter(notificationAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (MainActivity.profileNotifications != null && MainActivity.profileNotifications.size() > 0) {
            JSONArray arrayNotifications = new JSONArray();
            for (NotificationModel notificationModel : MainActivity.profileNotifications) {
                JSONObject objectNotification = new JSONObject();
                try {
                    objectNotification.put("id", notificationModel.getId());
                    objectNotification.put("noticeFrom", notificationModel.getNoticeFrom());
                    objectNotification.put("noticeTo", notificationModel.getNoticeTo());
                    objectNotification.put("projectId", notificationModel.getProjectId());
                    objectNotification.put("meetingId", notificationModel.getMeetingId());
                    objectNotification.put("taskId", notificationModel.getTaskId());
                    objectNotification.put("content", notificationModel.getContent());
                    objectNotification.put("createdAt", notificationModel.getCreatedAt());
                    objectNotification.put("flagRead", notificationModel.getFlagRead());
                    arrayNotifications.put(objectNotification);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            new UpdateNotificationsAsyncTask(ProjectNotificationActivity.this
                    , arrayNotifications.toString()).execute(Constants.URL_UPDATE_PROFILE_NOTIFICATIONS_API);
        }
    }
}
